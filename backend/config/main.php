<?php

$params = array_merge(

    require __DIR__ . '/../../common/config/params.php',

    require __DIR__ . '/../../common/config/params-local.php',

    require __DIR__ . '/params.php',

    require __DIR__ . '/params-local.php'

);



return [

    'id' => 'app-backend',

    'basePath' => dirname(__DIR__),

    'controllerNamespace' => 'backend\controllers',

    'bootstrap' => ['log'],

    'modules' => [

	    'schoolinfo' => [
            'class' => 'app\modules\schoolinfo\Module',
        ],
		'principal' => [
            'class' => 'app\modules\principal\Module',
        ],
		'teacher' => [
            'class' => 'app\modules\teacher\Module',
        ],
		
		'student' => [
            'class' => 'app\modules\student\Module',
        ],
        'subject' => [
            'class' => 'app\modules\subject\Module',
        ],
		'classwise' => [
            'class' => 'app\modules\classwise\Module',
        ],
		'examination' => [
            'class' => 'app\modules\examination\Module',
        ],
		'schoolmanagement' => [
            'class' => 'app\modules\schoolmanagement\Module',
        ],
		'promote' => [
            'class' => 'app\modules\promote\Module',
        ],
	],

    'components' => [

        'request' => [

            'csrfParam' => '_csrf-backend',

        ],
		
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
			'defaultRoles' => ['guest'],
        ],

		'utility' => [

            'class' => 'app\components\Utility',

        ],
        'user' => [

            'identityClass' => 'common\models\User',

            'enableAutoLogin' => true,

            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],

        ],

        'session' => [

            // this is the name of the session cookie used for login on the backend

            'name' => 'advanced-backend',

        ],

        'log' => [

            'traceLevel' => YII_DEBUG ? 3 : 0,

            'targets' => [

                [

                    'class' => 'yii\log\FileTarget',

                    'levels' => ['error', 'warning'],

                ],

            ],

        ],

        'errorHandler' => [

            'errorAction' => 'site/error',

        ],

       

     

        'urlManager' => [

            'enablePrettyUrl' => true,

            //'showScriptName' => false,

            'rules' => [

            ],

        ],

      

 

    ],

    'params' => $params,

];

