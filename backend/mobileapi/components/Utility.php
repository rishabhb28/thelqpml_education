<?php

namespace app\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use PDO;
use yii\web\Cookie;
use yii\web\Session;

use yii\db\Query;

class Utility extends Component {

  /**
   * Function to redirect on changepassword page when user login first time
   * @return Boolean
   */
  public function forcefullyChangePassword() {
    return FALSE;

}


    /**
     * Function to generater unique token for SSO
     * @return String $uuid
     */
    static function generateGuuid($moduleName, $tableName) {
        $time = time();
        $randValue = rand(10000000, 99999999);
        $firstmd5 = md5($time . $randValue);

        $time = time();
        $randValue = rand(10000000, 99999999);
        $secondmd5 = md5($moduleName . $tableName . $randValue);

        $guuid = strtoupper($firstmd5 . $secondmd5);
        return $guuid;
    }


}
