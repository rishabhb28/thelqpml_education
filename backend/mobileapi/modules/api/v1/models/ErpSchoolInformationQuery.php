<?php


namespace app\modules\api\v1\models;

/**
 * This is the ActiveQuery class for [[ErpSchoolInformation]].
 *
 * @see ErpSchoolInformation
 */
class ErpSchoolInformationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ErpSchoolInformation[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ErpSchoolInformation|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
