<?php

namespace app\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "erp_schol_feedback".
 *
 * @property int $school_feedback_id
 * @property string $school_feedback_uuid
 * @property string $school_feedback_school_uuid
 * @property string $school_feedback_class_uuid
 * @property string $school_feedback_section_uuid
 * @property string $school_feedback_student_uuid
 * @property string $school_feedback_date
 * @property string $school_feedback_des
 * @property string $school_feedback_status
 * @property string $school_feedback_deleted
 * @property string $school_feedback_created_date
 *
 * @property ErpSchoolInformation $schoolFeedbackSchoolUu
 * @property ErpMasterClassSection $schoolFeedbackSectionUu
 * @property ErpStudentInformation $schoolFeedbackStudentUu
 * @property ErpMasterClass $schoolFeedbackClassUu
 */
class ErpScholFeedback extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'erp_schol_feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_feedback_uuid', 'school_feedback_school_uuid', 'school_feedback_class_uuid', 'school_feedback_section_uuid', 'school_feedback_student_uuid', 'school_feedback_date', 'school_feedback_des', 'school_feedback_created_date'], 'required'],
            [['school_feedback_date', 'school_feedback_created_date'], 'safe'],
            [['school_feedback_des', 'school_feedback_status', 'school_feedback_deleted'], 'string'],
            [['school_feedback_uuid', 'school_feedback_school_uuid', 'school_feedback_class_uuid', 'school_feedback_section_uuid', 'school_feedback_student_uuid'], 'string', 'max' => 128],
            [['school_feedback_school_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpSchoolInformation::className(), 'targetAttribute' => ['school_feedback_school_uuid' => 'school_uuid']],
            [['school_feedback_section_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpMasterClassSection::className(), 'targetAttribute' => ['school_feedback_section_uuid' => 'section_uuid']],
            [['school_feedback_student_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpStudentInformation::className(), 'targetAttribute' => ['school_feedback_student_uuid' => 'student_uuid']],
            [['school_feedback_class_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpMasterClass::className(), 'targetAttribute' => ['school_feedback_class_uuid' => 'class_uuid']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'school_feedback_id' => 'School Feedback ID',
            'school_feedback_uuid' => 'School Feedback Uuid',
            'school_feedback_school_uuid' => 'School Feedback School Uuid',
            'school_feedback_class_uuid' => 'School Feedback Class Uuid',
            'school_feedback_section_uuid' => 'School Feedback Section Uuid',
            'school_feedback_student_uuid' => 'School Feedback Student Uuid',
            'school_feedback_date' => 'School Feedback Date',
            'school_feedback_des' => 'School Feedback Des',
            'school_feedback_status' => 'School Feedback Status',
            'school_feedback_deleted' => 'School Feedback Deleted',
            'school_feedback_created_date' => 'School Feedback Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolFeedbackSchoolUu()
    {
        return $this->hasOne(ErpSchoolInformation::className(), ['school_uuid' => 'school_feedback_school_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolFeedbackSectionUu()
    {
        return $this->hasOne(ErpMasterClassSection::className(), ['section_uuid' => 'school_feedback_section_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolFeedbackStudentUu()
    {
        return $this->hasOne(ErpStudentInformation::className(), ['student_uuid' => 'school_feedback_student_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolFeedbackClassUu()
    {
        return $this->hasOne(ErpMasterClass::className(), ['class_uuid' => 'school_feedback_class_uuid']);
    }

    /**
     * @inheritdoc
     * @return ErpScholFeedbackQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ErpScholFeedbackQuery(get_called_class());
    }
}
