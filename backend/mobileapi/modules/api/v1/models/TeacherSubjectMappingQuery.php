<?php

namespace app\modules\api\v1\models;

/**
 * This is the ActiveQuery class for [[TeacherSubjectMapping]].
 *
 * @see TeacherSubjectMapping
 */
class TeacherSubjectMappingQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return TeacherSubjectMapping[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TeacherSubjectMapping|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
