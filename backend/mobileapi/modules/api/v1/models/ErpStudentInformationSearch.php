<?php

namespace app\modules\api\v1\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\student\models\ErpStudentInformation;

/**
 * ErpStudentInformationSearch represents the model behind the search form of `app\modules\student\models\ErpStudentInformation`.
 */
class ErpStudentInformationSearch extends ErpStudentInformation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student_id'], 'integer'],
            [['student_uuid', 'student_school_uuid', 'student_class_uuid', 'student_first_name', 'student_last_name', 'student_dob', 'student_address1', 'student_phone', 'student_email', 'student_created_date', 'student_address2', 'student_state', 'student_city', 'student_zip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ErpStudentInformation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'student_id' => $this->student_id,
            'student_dob' => $this->student_dob,
            'student_created_date' => $this->student_created_date,
        ]);

        $query->andFilterWhere(['like', 'student_uuid', $this->student_uuid])
            ->andFilterWhere(['like', 'student_school_uuid', $this->student_school_uuid])
            ->andFilterWhere(['like', 'student_class_uuid', $this->student_class_uuid])
            ->andFilterWhere(['like', 'student_session_uuid', $this->student_session_uuid])
            ->andFilterWhere(['like', 'student_first_name', $this->student_first_name])
			->andFilterWhere(['like', 'student_last_name', $this->student_last_name])
            ->andFilterWhere(['like', 'student_gender', $this->student_gender])
            ->andFilterWhere(['like', 'student_nationality', $this->student_nationality])
            ->andFilterWhere(['like', 'student_address1', $this->student_address1])
            ->andFilterWhere(['like', 'student_phone', $this->student_phone])
            ->andFilterWhere(['like', 'student_email', $this->student_email])
            ->andFilterWhere(['like', 'student_is_status', $this->student_is_status])
            ->andFilterWhere(['like', 'student_is_delete', $this->student_is_delete]);

        return $dataProvider;
    }
}
