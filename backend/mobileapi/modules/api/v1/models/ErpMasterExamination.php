<?php

namespace app\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "erp_master_examination".
 *
 * @property int $examination_id
 * @property string $examination_uuid
 * @property string $school_uuid
 * @property string $exam_type_uuid
 * @property string $exam_class_uuid
 * @property string $exam_subject_uuid
 * @property string $exam_date
 * @property string $examination_start_time
 * @property string $examination_end_time
 * @property string $exam_status
 * @property string $exam_is_deleted
 * @property string $exam_created_date
 *
 * @property ErpMasterSubject $examSubjectUu
 * @property ErpSchoolInformation $schoolUu
 * @property ErpMasterExaminationType $examTypeUu
 * @property ErpSchoolClasswise $examClassUu
 */
class ErpMasterExamination extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'erp_master_examination';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['examination_uuid', 'school_uuid', 'exam_type_uuid', 'exam_class_uuid', 'exam_subject_uuid', 'exam_date', 'examination_start_time', 'examination_end_time', 'exam_created_date'], 'required'],
            [['exam_date', 'examination_start_time', 'examination_end_time', 'exam_created_date'], 'safe'],
            [['exam_status', 'exam_is_deleted'], 'string'],
            [['examination_uuid', 'school_uuid', 'exam_type_uuid', 'exam_class_uuid', 'exam_subject_uuid'], 'string', 'max' => 128],
            [['examination_uuid'], 'unique'],
            [['exam_subject_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpMasterSubject::className(), 'targetAttribute' => ['exam_subject_uuid' => 'subject_uuid']],
            [['school_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpSchoolInformation::className(), 'targetAttribute' => ['school_uuid' => 'school_uuid']],
            [['exam_type_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpMasterExaminationType::className(), 'targetAttribute' => ['exam_type_uuid' => 'examination_type_uuid']],
            [['exam_class_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpSchoolClasswise::className(), 'targetAttribute' => ['exam_class_uuid' => 'classwise_uuid']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'examination_id' => 'Examination ID',
            'examination_uuid' => 'Examination Uuid',
            'school_uuid' => 'School Uuid',
            'exam_type_uuid' => 'Exam Type Uuid',
            'exam_class_uuid' => 'Exam Class Uuid',
            'exam_subject_uuid' => 'Exam Subject Uuid',
            'exam_date' => 'Exam Date',
            'examination_start_time' => 'Examination Start Time',
            'examination_end_time' => 'Examination End Time',
            'exam_status' => 'Exam Status',
            'exam_is_deleted' => 'Exam Is Deleted',
            'exam_created_date' => 'Exam Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExamSubjectUu()
    {
        return $this->hasOne(ErpMasterSubject::className(), ['subject_uuid' => 'exam_subject_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolUu()
    {
        return $this->hasOne(ErpSchoolInformation::className(), ['school_uuid' => 'school_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExamTypeUu()
    {
        return $this->hasOne(ErpMasterExaminationType::className(), ['examination_type_uuid' => 'exam_type_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExamClassUu()
    {
        return $this->hasOne(ErpSchoolClasswise::className(), ['classwise_uuid' => 'exam_class_uuid']);
    }

    /**
     * @inheritdoc
     * @return ErpMasterExaminationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ErpMasterExaminationQuery(get_called_class());
    }
}
