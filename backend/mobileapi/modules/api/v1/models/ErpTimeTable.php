<?php

namespace app\modules\api\v1\models;


use Yii;

/**
 * This is the model class for table "erp_time_table".
 *
 * @property int $timetable_id
 * @property string $timetable_uuid
 * @property string $timetable_school_uuid
 * @property string $section_uuid
 * @property string $timetable_school_class_uuid
 * @property string $timetable_subject_uuid
 * @property string $timetable_title
 * @property string $timetable_start_time
 * @property string $timetable_end_date
 * @property string $timetable_is_status
 * @property string $timetable_is_deleted
 * @property string $timetable_created_date
 *
 * @property ErpSchoolInformation $timetableSchoolUu
 * @property ErpMasterSubject $timetableSubjectUu
 * @property ErpMasterClassSection $sectionUu
 * @property ErpMasterClass $timetableSchoolClassUu
 */
class ErpTimeTable extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'erp_time_table';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['timetable_uuid', 'timetable_school_uuid', 'section_uuid', 'timetable_school_class_uuid', 'timetable_subject_uuid', 'timetable_title', 'timetable_start_time', 'timetable_end_date', 'timetable_created_date'], 'required'],
            [['timetable_start_time', 'timetable_end_date', 'timetable_created_date'], 'safe'],
            [['timetable_is_status', 'timetable_is_deleted'], 'string'],
            [['timetable_uuid', 'timetable_school_uuid', 'section_uuid', 'timetable_school_class_uuid', 'timetable_subject_uuid'], 'string', 'max' => 128],
            [['timetable_title'], 'string', 'max' => 255],
            [['timetable_uuid'], 'unique'],
            [['timetable_school_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpSchoolInformation::className(), 'targetAttribute' => ['timetable_school_uuid' => 'school_uuid']],
            [['timetable_subject_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpMasterSubject::className(), 'targetAttribute' => ['timetable_subject_uuid' => 'subject_uuid']],
            [['section_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpMasterClassSection::className(), 'targetAttribute' => ['section_uuid' => 'section_uuid']],
            [['timetable_school_class_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpMasterClass::className(), 'targetAttribute' => ['timetable_school_class_uuid' => 'class_uuid']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'timetable_id' => 'Timetable ID',
            'timetable_uuid' => 'Timetable Uuid',
            'timetable_school_uuid' => 'Timetable School Uuid',
            'section_uuid' => 'Section Uuid',
            'timetable_school_class_uuid' => 'Timetable School Class Uuid',
            'timetable_subject_uuid' => 'Timetable Subject Uuid',
            'timetable_title' => 'Timetable Title',
            'timetable_start_time' => 'Timetable Start Time',
            'timetable_end_date' => 'Timetable End Date',
            'timetable_is_status' => 'Timetable Is Status',
            'timetable_is_deleted' => 'Timetable Is Deleted',
            'timetable_created_date' => 'Timetable Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimetableSchoolUu()
    {
        return $this->hasOne(ErpSchoolInformation::className(), ['school_uuid' => 'timetable_school_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimetableSubjectUu()
    {
        return $this->hasOne(ErpMasterSubject::className(), ['subject_uuid' => 'timetable_subject_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSectionUu()
    {
        return $this->hasOne(ErpMasterClassSection::className(), ['section_uuid' => 'section_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimetableSchoolClassUu()
    {
        return $this->hasOne(ErpMasterClass::className(), ['class_uuid' => 'timetable_school_class_uuid']);
    }

    /**
     * @inheritdoc
     * @return ErpTimeTableQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ErpTimeTableQuery(get_called_class());
    }
}
