<?php

namespace app\modules\api\v1\models;

/**
 * This is the ActiveQuery class for [[ErpMasterClass]].
 *
 * @see ErpMasterClass
 */
class ErpMasterClassQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ErpMasterClass[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ErpMasterClass|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
