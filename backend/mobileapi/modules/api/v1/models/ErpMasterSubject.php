<?php

namespace app\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "erp_master_subject".
 *
 * @property int $subject_id
 * @property string $subject_uuid
 * @property string $subject_school_uuid
 * @property string $subject_name
 * @property string $subject_status
 * @property string $subject_is_deleted
 * @property string $subject_created_date
 *
 * @property ErpSchoolInformation $subjectSchoolUu
 * @property ErpTimeTable[] $erpTimeTables
 */
class ErpMasterSubject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'erp_master_subject';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subject_uuid', 'subject_school_uuid', 'subject_name', 'subject_created_date'], 'required'],
            [['subject_status', 'subject_is_deleted'], 'string'],
            [['subject_created_date'], 'safe'],
            [['subject_uuid', 'subject_school_uuid'], 'string', 'max' => 128],
            [['subject_name'], 'string', 'max' => 100],
            [['subject_uuid'], 'unique'],
            [['subject_school_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpSchoolInformation::className(), 'targetAttribute' => ['subject_school_uuid' => 'school_uuid']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'subject_id' => 'Subject ID',
            'subject_uuid' => 'Subject Uuid',
            'subject_school_uuid' => 'Subject School Uuid',
            'subject_name' => 'Subject Name',
            'subject_status' => 'Subject Status',
            'subject_is_deleted' => 'Subject Is Deleted',
            'subject_created_date' => 'Subject Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjectSchoolUu()
    {
        return $this->hasOne(ErpSchoolInformation::className(), ['school_uuid' => 'subject_school_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getErpTimeTables()
    {
        return $this->hasMany(ErpTimeTable::className(), ['timetable_subject_uuid' => 'subject_uuid']);
    }

    /**
     * @inheritdoc
     * @return ErpMasterSubjectQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ErpMasterSubjectQuery(get_called_class());
    }
}
