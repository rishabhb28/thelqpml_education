<?php

namespace app\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "erp_school_information".
 *
 * @property int $school_id
 * @property string $school_uuid
 * @property string $school_main_uuid
 * @property string $school_reg_number
 * @property string $school_name
 * @property string $school_email
 * @property string $school_phone
 * @property int $school_country
 * @property int $school_state
 * @property int $school_dist
 * @property int $school_post_code
 * @property string $school_board
 * @property string $school_is_deleted
 * @property string $school_status
 * @property string $school_created_date
 *
 * @property ErpMasterTeacher[] $erpMasterTeachers
 * @property ErpSchoolClasswise[] $erpSchoolClasswises
 * @property User $schoolMainUu
 * @property ErpStudentInformation[] $erpStudentInformations
 */
class ErpSchoolInformation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'erp_school_information';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_uuid', 'school_main_uuid', 'school_reg_number', 'school_name', 'school_email', 'school_phone', 'school_state', 'school_dist', 'school_post_code', 'school_board', 'school_created_date'], 'required'],
            [['school_country', 'school_state', 'school_dist', 'school_post_code'], 'integer'],
            [['school_is_deleted', 'school_status'], 'string'],
            [['school_created_date'], 'safe'],
            [['school_uuid', 'school_main_uuid', 'school_board'], 'string', 'max' => 128],
            [['school_reg_number', 'school_email'], 'string', 'max' => 100],
            [['school_name'], 'string', 'max' => 200],
            [['school_phone'], 'string', 'max' => 20],
            [['school_uuid'], 'unique'],
            [['school_phone'], 'unique'],
            [['school_email'], 'unique'],
            [['school_reg_number'], 'unique'],
            [['school_main_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['school_main_uuid' => 'user_uuid']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'school_id' => 'School ID',
            'school_uuid' => 'School Uuid',
            'school_main_uuid' => 'School Main Uuid',
            'school_reg_number' => 'School Reg Number',
            'school_name' => 'School Name',
            'school_email' => 'School Email',
            'school_phone' => 'School Phone',
            'school_country' => 'School Country',
            'school_state' => 'School State',
            'school_dist' => 'School Dist',
            'school_post_code' => 'School Post Code',
            'school_board' => 'School Board',
            'school_is_deleted' => 'School Is Deleted',
            'school_status' => 'School Status',
            'school_created_date' => 'School Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getErpMasterTeachers()
    {
        return $this->hasMany(ErpMasterTeacher::className(), ['teacher_school_uuid' => 'school_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getErpSchoolClasswises()
    {
        return $this->hasMany(ErpSchoolClasswise::className(), ['classwise_school_uuid' => 'school_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolMainUu()
    {
        return $this->hasOne(User::className(), ['user_uuid' => 'school_main_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getErpStudentInformations()
    {
        return $this->hasMany(ErpStudentInformation::className(), ['student_school_uuid' => 'school_uuid']);
    }

    /**
     * @inheritdoc
     * @return ErpSchoolInformationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ErpSchoolInformationQuery(get_called_class());
    }
}
