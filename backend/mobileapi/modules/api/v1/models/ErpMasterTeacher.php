<?php

namespace app\modules\api\v1\models;

/**
 * This is the model class for table "erp_master_teacher".
 *
 * @property int $teacher_id
 * @property string $teacher_uuid
 * @property string $teacher_school_uuid
 * @property string $teacher_first_name
 * @property string $teacher_last_name
 * @property string $teacher_email
 * @property string $teacher_password
 * @property string $teacher_phone
 * @property string $teacher_employee_number
 * @property string $teacher_employemdnt_date
 * @property string $teacher_dob
 * @property string $teacher_marital_status
 * @property string $teacher_address1
 * @property string $teacher_address2
 * @property string $teacher_state
 * @property string $teacher_city
 * @property string $teacher_zip
 * @property string $teacher_is_deleted
 * @property string $teacher_is_status
 * @property string $teacher_created_date
 *
 * @property ErpSchoolInformation $teacherSchoolUu
 * @property ErpStudentHomeWork[] $erpStudentHomeWorks
 * @property TeacherSubjectMapping[] $teacherSubjectMappings
 */
class ErpMasterTeacher extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'erp_master_teacher';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teacher_uuid', 'teacher_school_uuid', 'teacher_first_name', 'teacher_last_name', 'teacher_email', 'teacher_phone', 'teacher_employemdnt_date', 'teacher_dob', 'teacher_address1', 'teacher_address2', 'teacher_state', 'teacher_city', 'teacher_zip', 'teacher_created_date'], 'required'],
            [['teacher_employemdnt_date', 'teacher_dob', 'teacher_created_date'], 'safe'],
            [['teacher_marital_status', 'teacher_is_deleted', 'teacher_is_status'], 'string'],
            [['teacher_uuid', 'teacher_school_uuid'], 'string', 'max' => 128],
            [['teacher_first_name', 'teacher_last_name', 'teacher_state', 'teacher_city'], 'string', 'max' => 20],
            [['teacher_email'], 'string', 'max' => 50],
            [['teacher_password'], 'string', 'max' => 34],
            [['teacher_phone', 'teacher_employee_number'], 'string', 'max' => 10],
            [['teacher_address1', 'teacher_address2'], 'string', 'max' => 255],
            [['teacher_zip'], 'string', 'max' => 6],
            [['teacher_uuid'], 'unique'],
            [['teacher_email'], 'unique'],
            [['teacher_phone'], 'unique'],
            [['teacher_school_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpSchoolInformation::className(), 'targetAttribute' => ['teacher_school_uuid' => 'school_uuid']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'teacher_id' => 'Teacher ID',
            'teacher_uuid' => 'Teacher Uuid',
            'teacher_school_uuid' => 'Teacher School Uuid',
            'teacher_first_name' => 'Teacher First Name',
            'teacher_last_name' => 'Teacher Last Name',
            'teacher_email' => 'Teacher Email',
            'teacher_password' => 'Teacher Password',
            'teacher_phone' => 'Teacher Phone',
            'teacher_employee_number' => 'Teacher Employee Number',
            'teacher_employemdnt_date' => 'Teacher Employemdnt Date',
            'teacher_dob' => 'Teacher Dob',
            'teacher_marital_status' => 'Teacher Marital Status',
            'teacher_address1' => 'Teacher Address1',
            'teacher_address2' => 'Teacher Address2',
            'teacher_state' => 'Teacher State',
            'teacher_city' => 'Teacher City',
            'teacher_zip' => 'Teacher Zip',
            'teacher_is_deleted' => 'Teacher Is Deleted',
            'teacher_is_status' => 'Teacher Is Status',
            'teacher_created_date' => 'Teacher Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacherSchoolUu()
    {
        return $this->hasOne(ErpSchoolInformation::className(), ['school_uuid' => 'teacher_school_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getErpStudentHomeWorks()
    {
        return $this->hasMany(ErpStudentHomeWork::className(), ['teacher_uuid' => 'teacher_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacherSubjectMappings()
    {
        return $this->hasMany(TeacherSubjectMapping::className(), ['teacher_uuid' => 'teacher_uuid']);
    }

    /**
     * @inheritdoc
     * @return ErpMasterTeacherQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ErpMasterTeacherQuery(get_called_class());
    }
}
