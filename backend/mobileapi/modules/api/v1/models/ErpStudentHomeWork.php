<?php

namespace app\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "erp_student_home_work".
 *
 * @property int $student_home_work_id
 * @property string $student_home_work_uuid
 * @property string $teacher_uuid
 * @property string $class_uuid
 * @property string $section_uuid
 * @property string $subject_uuid
 * @property string $student_home_work_date
 * @property string $student_home_work_des
 * @property string $student_home_work_status
 * @property string $student_home_work_is_deleted
 * @property string $student_home_work_created_date
 *
 * @property ErpMasterTeacher $teacherUu
 * @property ErpMasterClassSection $sectionUu
 * @property ErpMasterSubject $subjectUu
 * @property ErpMasterClass $classUu
 */
class ErpStudentHomeWork extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'erp_student_home_work';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student_home_work_uuid', 'teacher_uuid', 'class_uuid', 'section_uuid', 'subject_uuid', 'student_home_work_date', 'student_home_work_des', 'student_home_work_created_date'], 'required'],
            [['student_home_work_date', 'student_home_work_created_date'], 'safe'],
            [['student_home_work_des', 'student_home_work_status', 'student_home_work_is_deleted'], 'string'],
            [['student_home_work_uuid', 'teacher_uuid', 'class_uuid', 'section_uuid', 'subject_uuid'], 'string', 'max' => 128],
            [['teacher_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpMasterTeacher::className(), 'targetAttribute' => ['teacher_uuid' => 'teacher_uuid']],
            [['section_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpMasterClassSection::className(), 'targetAttribute' => ['section_uuid' => 'section_uuid']],
            [['subject_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpMasterSubject::className(), 'targetAttribute' => ['subject_uuid' => 'subject_uuid']],
            [['class_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpMasterClass::className(), 'targetAttribute' => ['class_uuid' => 'class_uuid']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'student_home_work_id' => 'Student Home Work ID',
            'student_home_work_uuid' => 'Student Home Work Uuid',
            'teacher_uuid' => 'Teacher Uuid',
            'class_uuid' => 'Class Uuid',
            'section_uuid' => 'Section Uuid',
            'subject_uuid' => 'Subject Uuid',
            'student_home_work_date' => 'Student Home Work Date',
            'student_home_work_des' => 'Student Home Work Des',
            'student_home_work_status' => 'Student Home Work Status',
            'student_home_work_is_deleted' => 'Student Home Work Is Deleted',
            'student_home_work_created_date' => 'Student Home Work Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacherUu()
    {
        return $this->hasOne(ErpMasterTeacher::className(), ['teacher_uuid' => 'teacher_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSectionUu()
    {
        return $this->hasOne(ErpMasterClassSection::className(), ['section_uuid' => 'section_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjectUu()
    {
        return $this->hasOne(ErpMasterSubject::className(), ['subject_uuid' => 'subject_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassUu()
    {
        return $this->hasOne(ErpMasterClass::className(), ['class_uuid' => 'class_uuid']);
    }

    /**
     * @inheritdoc
     * @return ErpStudentHomeWorkQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ErpStudentHomeWorkQuery(get_called_class());
    }
}
