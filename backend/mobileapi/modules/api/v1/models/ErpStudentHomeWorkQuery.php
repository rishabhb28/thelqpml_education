<?php

namespace app\modules\api\v1\models;

/**
 * This is the ActiveQuery class for [[ErpStudentHomeWork]].
 *
 * @see ErpStudentHomeWork
 */
class ErpStudentHomeWorkQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ErpStudentHomeWork[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ErpStudentHomeWork|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
