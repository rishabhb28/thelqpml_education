<?php

namespace app\modules\api\v1\models;

use Yii;


/**
 * This is the model class for table "erp_master_class_section".
 *
 * @property int $section_id
 * @property string $section_uuid
 * @property string $school_uuid
 * @property string $section_title
 * @property string $section_created_date
 * @property string $section_status
 * @property string $section_is_deleted
 *
 * @property ErpSchoolInformation $schoolUu
 * @property ErpSchoolClasswise[] $erpSchoolClasswises
 * @property ErpStudentInformation[] $erpStudentInformations
 */
class ErpMasterClassSection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'erp_master_class_section';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['section_uuid', 'school_uuid', 'section_title', 'section_created_date'], 'required'],
            [['section_title', 'section_status', 'section_is_deleted'], 'string'],
            [['section_created_date'], 'safe'],
            [['section_uuid', 'school_uuid'], 'string', 'max' => 128],
            [['school_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpSchoolInformation::className(), 'targetAttribute' => ['school_uuid' => 'school_uuid']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'section_id' => 'Section ID',
            'section_uuid' => 'Section Uuid',
            'school_uuid' => 'School Uuid',
            'section_title' => 'Section Title',
            'section_created_date' => 'Section Created Date',
            'section_status' => 'Section Status',
            'section_is_deleted' => 'Section Is Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolUu()
    {
        return $this->hasOne(ErpSchoolInformation::className(), ['school_uuid' => 'school_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getErpSchoolClasswises()
    {
        return $this->hasMany(ErpSchoolClasswise::className(), ['section_uuid' => 'section_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getErpStudentInformations()
    {
        return $this->hasMany(ErpStudentInformation::className(), ['section_uuid' => 'section_uuid']);
    }

    /**
     * @inheritdoc
     * @return ErpMasterClassSectionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ErpMasterClassSectionQuery(get_called_class());
    }
}
