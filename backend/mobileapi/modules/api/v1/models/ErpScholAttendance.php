<?php

namespace app\modules\api\v1\models;


use Yii;

/**
 * This is the model class for table "erp_schol_attendance".
 *
 * @property int $school_attendance_id
 * @property string $school_attendance_uuid
 * @property string $school_attendance_school_uuid
 * @property string $school_attendance_class_uuid
 * @property string $school_attendance_section_uuid
 * @property string $school_attendance_student_uuid
 * @property string $school_attendance_date
 * @property string $school_attendance_student_status
 * @property string $school_attendance_status
 * @property string $school_attendance_deleted
 * @property string $school_attendance_created_date
 *
 * @property ErpSchoolInformation $schoolAttendanceSchoolUu
 * @property ErpMasterClassSection $schoolAttendanceSectionUu
 * @property ErpStudentInformation $schoolAttendanceStudentUu
 * @property ErpMasterClass $schoolAttendanceClassUu
 */
class ErpScholAttendance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'erp_schol_attendance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_attendance_uuid', 'school_attendance_school_uuid', 'school_attendance_class_uuid', 'school_attendance_section_uuid', 'school_attendance_student_uuid', 'school_attendance_date', 'school_attendance_student_status', 'school_attendance_created_date'], 'required'],
            [['school_attendance_date', 'school_attendance_created_date'], 'safe'],
            [['school_attendance_student_status', 'school_attendance_status', 'school_attendance_deleted'], 'string'],
            [['school_attendance_uuid', 'school_attendance_school_uuid', 'school_attendance_class_uuid', 'school_attendance_section_uuid', 'school_attendance_student_uuid'], 'string', 'max' => 128],
            [['school_attendance_uuid'], 'unique'],
            [['school_attendance_section_uuid', 'school_attendance_student_uuid', 'school_attendance_date'], 'unique', 'targetAttribute' => ['school_attendance_section_uuid', 'school_attendance_student_uuid', 'school_attendance_date']],
            [['school_attendance_school_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpSchoolInformation::className(), 'targetAttribute' => ['school_attendance_school_uuid' => 'school_uuid']],
            [['school_attendance_section_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpMasterClassSection::className(), 'targetAttribute' => ['school_attendance_section_uuid' => 'section_uuid']],
            [['school_attendance_student_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpStudentInformation::className(), 'targetAttribute' => ['school_attendance_student_uuid' => 'student_uuid']],
            [['school_attendance_class_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpMasterClass::className(), 'targetAttribute' => ['school_attendance_class_uuid' => 'class_uuid']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'school_attendance_id' => 'School Attendance ID',
            'school_attendance_uuid' => 'School Attendance Uuid',
            'school_attendance_school_uuid' => 'School Attendance School Uuid',
            'school_attendance_class_uuid' => 'School Attendance Class Uuid',
            'school_attendance_section_uuid' => 'School Attendance Section Uuid',
            'school_attendance_student_uuid' => 'School Attendance Student Uuid',
            'school_attendance_date' => 'School Attendance Date',
            'school_attendance_student_status' => 'School Attendance Student Status',
            'school_attendance_status' => 'School Attendance Status',
            'school_attendance_deleted' => 'School Attendance Deleted',
            'school_attendance_created_date' => 'School Attendance Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolAttendanceSchoolUu()
    {
        return $this->hasOne(ErpSchoolInformation::className(), ['school_uuid' => 'school_attendance_school_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolAttendanceSectionUu()
    {
        return $this->hasOne(ErpMasterClassSection::className(), ['section_uuid' => 'school_attendance_section_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolAttendanceStudentUu()
    {
        return $this->hasOne(ErpStudentInformation::className(), ['student_uuid' => 'school_attendance_student_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolAttendanceClassUu()
    {
        return $this->hasOne(ErpMasterClass::className(), ['class_uuid' => 'school_attendance_class_uuid']);
    }

    /**
     * @inheritdoc
     * @return ErpScholAttendanceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ErpScholAttendanceQuery(get_called_class());
    }
}
