<?php

namespace app\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "erp_student_leave_status".
 *
 * @property int $student_leave_id
 * @property string $student_leave_uuid
 * @property string $student_uuid
 * @property string $student_class_uuid
 * @property string $student_section_uuid
 * @property string $student_leave_from_date
 * @property string $student_leave_to_date
 * @property string $student_leave_description
 * @property string $student_leave_status
 * @property string $student_leave_is_deleted
 * @property string $student_leave_created_date
 *
 * @property ErpStudentInformation $studentUu
 * @property ErpMasterClassSection $studentSectionUu
 * @property ErpMasterClass $studentClassUu
 */
class ErpStudentLeaveStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'erp_student_leave_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student_leave_uuid', 'student_uuid', 'student_class_uuid', 'student_section_uuid', 'student_leave_from_date', 'student_leave_to_date', 'student_leave_description', 'student_leave_created_date'], 'required'],
            [['student_leave_from_date', 'student_leave_to_date', 'student_leave_created_date'], 'safe'],
            [['student_leave_description', 'student_leave_status', 'student_leave_is_deleted'], 'string'],
            [['student_leave_uuid', 'student_uuid', 'student_class_uuid', 'student_section_uuid'], 'string', 'max' => 128],
            [['student_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpStudentInformation::className(), 'targetAttribute' => ['student_uuid' => 'student_uuid']],
            [['student_section_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpMasterClassSection::className(), 'targetAttribute' => ['student_section_uuid' => 'section_uuid']],
            [['student_class_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpMasterClass::className(), 'targetAttribute' => ['student_class_uuid' => 'class_uuid']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'student_leave_id' => 'Student Leave ID',
            'student_leave_uuid' => 'Student Leave Uuid',
            'student_uuid' => 'Student Uuid',
            'student_class_uuid' => 'Student Class Uuid',
            'student_section_uuid' => 'Student Section Uuid',
            'student_leave_from_date' => 'Student Leave From Date',
            'student_leave_to_date' => 'Student Leave To Date',
            'student_leave_description' => 'Student Leave Description',
            'student_leave_status' => 'Student Leave Status',
            'student_leave_is_deleted' => 'Student Leave Is Deleted',
            'student_leave_created_date' => 'Student Leave Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentUu()
    {
        return $this->hasOne(ErpStudentInformation::className(), ['student_uuid' => 'student_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentSectionUu()
    {
        return $this->hasOne(ErpMasterClassSection::className(), ['section_uuid' => 'student_section_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentClassUu()
    {
        return $this->hasOne(ErpMasterClass::className(), ['class_uuid' => 'student_class_uuid']);
    }

    /**
     * @inheritdoc
     * @return ErpStudentLeaveStatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ErpStudentLeaveStatusQuery(get_called_class());
    }
}
