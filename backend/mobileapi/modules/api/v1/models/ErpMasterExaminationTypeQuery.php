<?php

namespace app\modules\api\v1\models;

/**
 * This is the ActiveQuery class for [[ErpMasterExaminationType]].
 *
 * @see ErpMasterExaminationType
 */
class ErpMasterExaminationTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ErpMasterExaminationType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ErpMasterExaminationType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
