<?php

namespace app\modules\api\v1\models;

/**
 * This is the ActiveQuery class for [[ErpMasterTeacher]].
 *
 * @see ErpMasterTeacher
 */
class ErpMasterTeacherQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ErpMasterTeacher[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ErpMasterTeacher|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
