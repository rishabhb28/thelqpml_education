<?php

namespace app\modules\api\v1\models;

/**
 * This is the ActiveQuery class for [[ErpScholFeedback]].
 *
 * @see ErpScholFeedback
 */
class ErpScholFeedbackQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ErpScholFeedback[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ErpScholFeedback|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
