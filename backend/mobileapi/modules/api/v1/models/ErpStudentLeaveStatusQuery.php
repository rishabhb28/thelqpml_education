<?php

namespace app\modules\api\v1\models;

/**
 * This is the ActiveQuery class for [[ErpStudentLeaveStatus]].
 *
 * @see ErpStudentLeaveStatus
 */
class ErpStudentLeaveStatusQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ErpStudentLeaveStatus[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ErpStudentLeaveStatus|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
