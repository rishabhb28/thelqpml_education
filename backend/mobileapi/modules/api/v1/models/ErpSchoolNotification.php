<?php

namespace app\modules\api\v1\models;

use Yii;

/**
 * This is the model class for table "erp_school_notification".
 *
 * @property int $notification_id
 * @property string $notification_uuid
 * @property string $notification_school_uuid
 * @property string $notification_title
 * @property string $notification_description
 * @property string $notification_date
 * @property string $notification_is_status
 * @property string $notification_is_deleted
 * @property string $notification_created_date
 *
 * @property ErpSchoolInformation $notificationSchoolUu
 */
class ErpSchoolNotification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'erp_school_notification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notification_uuid', 'notification_school_uuid', 'notification_title', 'notification_description', 'notification_date', 'notification_created_date'], 'required'],
            [['notification_description', 'notification_is_status', 'notification_is_deleted'], 'string'],
            [['notification_date', 'notification_created_date'], 'safe'],
            [['notification_uuid', 'notification_school_uuid'], 'string', 'max' => 128],
            [['notification_title'], 'string', 'max' => 255],
            [['notification_uuid'], 'unique'],
            [['notification_school_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpSchoolInformation::className(), 'targetAttribute' => ['notification_school_uuid' => 'school_uuid']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'notification_id' => 'Notification ID',
            'notification_uuid' => 'Notification Uuid',
            'notification_school_uuid' => 'Notification School Uuid',
            'notification_title' => 'Notification Title',
            'notification_description' => 'Notification Description',
            'notification_date' => 'Notification Date',
            'notification_is_status' => 'Notification Is Status',
            'notification_is_deleted' => 'Notification Is Deleted',
            'notification_created_date' => 'Notification Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationSchoolUu()
    {
        return $this->hasOne(ErpSchoolInformation::className(), ['school_uuid' => 'notification_school_uuid']);
    }

    /**
     * @inheritdoc
     * @return ErpSchoolNotificationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ErpSchoolNotificationQuery(get_called_class());
    }
}
