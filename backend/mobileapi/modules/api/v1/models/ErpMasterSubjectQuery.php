<?php

namespace app\modules\api\v1\models;

/**
 * This is the ActiveQuery class for [[ErpMasterSubject]].
 *
 * @see ErpMasterSubject
 */
class ErpMasterSubjectQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ErpMasterSubject[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ErpMasterSubject|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
