<?php

namespace app\modules\api\v1\models;

/**
 * This is the ActiveQuery class for [[ErpSchoolNotification]].
 *
 * @see ErpSchoolNotification
 */
class ErpSchoolNotificationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ErpSchoolNotification[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ErpSchoolNotification|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
