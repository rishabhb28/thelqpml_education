<?php
namespace app\modules\api\v1\controllers;
use Yii;
use app\modules\api\v1\models\ErpMasterTeacher;
use app\modules\api\v1\models\ErpSchoolInformation;
use app\modules\api\v1\models\TeacherSubjectMapping;
use app\modules\api\v1\models\ErpTimeTable;
use app\modules\api\v1\models\ErpMasterExamination;
use app\modules\api\v1\models\ErpStudentInformation;
use app\modules\api\v1\models\ErpScholAttendance;
use app\modules\api\v1\models\ErpScholFeedback;
use app\modules\api\v1\models\ErpStudentLeaveStatus;
use app\modules\api\v1\models\ErpStudentHomeWork;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
/**

* TeacherController implements the CRUD actions for TeacherController model.

*/
class TeacherController extends \yii\web\Controller
{
    /**
    
    * @inheritdoc
    
    */
    public function beforeAction($action)
    {
        if ($action->id == 'index' || $action->id == 'login' || $action->id == 'getteacherclass' || $action->id == 'gettimetable' || $action->id == 'getexamdetail' || $action->id == 'getexamcalendar' || $action->id == 'getstudent' || $action->id == 'studentattendance' || $action->id == 'getstudentattendance' || $action->id == 'studentfeedback' || $action->id == 'getstudentfeedback' || $action->id == 'approveleave' || $action->id == 'getleavestatus' || $action->id == 'studentlisting' || $action->id == 'getclasswisesubject' || $action->id == 'studenthomework') {
			
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }
    /** 
    
    * Register new ErpMasterTeacher models.
    
    * @return mixed
    
    */
    public function actionIndex()
    {
        //$this->enableCsrfValidation = false;
        $_POST            = json_decode(file_get_contents('php://input'), TRUE);
        $teacher_phone    = $_POST['teacher_phone'];
        $teacher_password = md5($_POST['teacher_password']);
        $model = ErpMasterTeacher::find()->where(['teacher_phone' => $teacher_phone])->one();
        $count            = count($model);
        if ($count > 0 && $model->teacher_password == '') {
            $model->teacher_password = $teacher_password;
            if ($model->save()) {
                $response['status']  = 200;
                $response['message'] = 'Register Successfully';
            } else {
                $response['status']  = 201;
                $response['message'] = 'Internal Server Error';
            }
        } else {
            $response['status']  = 202;
            $response['message'] = 'Already Login!';
        }
        echo json_encode(array(
            "response" => $response
        ));
    }
    /**
    
    * Login functionality for ErpMasterTeacher
    
    * @return mixed
    
    */
    public function actionLogin()
    {
        //$this->enableCsrfValidation = false;
        $_POST            = json_decode(file_get_contents('php://input'), TRUE);
        $teacher_phone    = $_POST['teacher_phone'];
        $teacher_password = md5($_POST['teacher_password']);
        $model = ErpMasterTeacher::find()->where(['teacher_phone' => $teacher_phone, 'teacher_password' => $teacher_password])->one();
        $count            = count($model);
        if ($count > 0) {
            $output['teacher_uuid']        = $model->teacher_uuid;
			$output['teacher_name']        = $model->teacher_first_name.' '.$model->teacher_last_name;
            $output['teacher_school_uuid'] = $model->teacher_school_uuid;
            $response['status']            = 200;
            $response['message']           = 'Login Successfully';
            $response['detail']            = $output;
        } else {
            $response['statu']   = 201;
            $response['message'] = 'Please Check Creadtinals!';
        }
        echo json_encode(array(
            "response" => $response
        ));
    }
    /**
    
    * Login functionality for ErpMasterTeacher
    
    * @return mixed
    
    */
    public function actionGetteacherclass()
    {
        $_POST       = json_decode(file_get_contents('php://input'), TRUE);
        $teacherUuid = $_POST['teacherUuid'];
        $model = TeacherSubjectMapping::find()->with('classUu')->alias('ts')->joinWith('classUu.sectionUu AS cs')->joinWith('classUu.classwiseClassName AS ccn')->where(['teacher_uuid' => $teacherUuid])->asArray()->select(`DISTINCT class_uuid` )->all();
        $count       = count($model);
        if ($count > 0) {
            $output = array();
            foreach ($model as $value) {
                $data['class_uuid']           = $value['classUu']['classwiseClassName']['class_uuid'];
                $data['classwise_class_name'] = $value['classUu']['classwiseClassName']['class_title'];
                $data['section_uuid']         = $value['classUu']['section_uuid'];
                $data['section_title']        = $value['classUu']['sectionUu']['section_title'];
                $output[]                     = $data;
            }
            $response['status'] = 200;
            $response['detail'] = $output;
        } else {
            $response['statu']   = 201;
            $response['message'] = 'No data found!';
        }
        echo json_encode(array(
            "response" => $response
        ));
    }
    /**
    
    * Login functionality for ErpMasterTeacher
    
    * @return mixed
    
    */
    public function actionGettimetable()
    {
        $_POST       = json_decode(file_get_contents('php://input'), TRUE);
        $classUuid   = $_POST['classUuid'];
        $sectionUuid = $_POST['sectionUuid'];
        $timetableDay= $_POST['timetableDay'];
        
        $model = ErpTimeTable::find()->with('timetableSubjectUu')->with('timetableSchoolClassUu')
->with('timetableSchoolClassUu.erpSchoolClasswises')
->with('timetableSchoolClassUu.erpSchoolClasswises.sectionUu')->where(['timetable_school_class_uuid' => $classUuid, 'section_uuid' => $sectionUuid, 'timetable_title' => $timetableDay])->asArray()->all();


        $count       = count($model);
        if ($count > 0) {
            $output = array();
            foreach ($model as $value) {
            

            
	$getTeacher = TeacherSubjectMapping::find()->with('teacherUu')->where(['class_uuid' => $classUuid,'subject_uuid' => $value['timetable_subject_uuid']])->asArray()->one();

				
                $data['classwise_class_name'] = $value['timetableSchoolClassUu']['class_title'];
                /*$data['section_title']        = $value['timetableSchoolClassUu']['erpSchoolClasswises']['sectionUu']['section_title'];*/
                $data['subject_name']         = $value['timetableSubjectUu']['subject_name'];
		$data['teacher_name']         = $getTeacher['teacherUu']['teacher_first_name'].' '.$getTeacher['teacherUu']['teacher_last_name'];
                $data['timetable_start_time'] = date('h:i A', strtotime($value['timetable_start_time']));
                $data['timetable_end_date']   = date('h:i A', strtotime($value['timetable_end_date']));
                $output[]                     = $data;
            }
            $response['status'] = 200;
            $response['detail'] = $output;
        } else {
            $response['statu']   = 201;
            $response['message'] = 'No data found!';
        }
        echo json_encode(array(
            "response" => $response
        ));
    }
    /**
    
    * Login functionality for ErpMasterTeacher
    
    * @return mixed
    
    */
    public function actionGetexamdetail()
    {
        $_POST     = json_decode(file_get_contents('php://input'), TRUE);
        $classUuid = $_POST['classUuid'];
        $model = ErpMasterExamination::find()->with('examSubjectUu')->with('examTypeUu')->with('examClassUu')->where(['exam_class_uuid' => $classUuid])->asArray()->all();
        $count     = count($model);
        if ($count > 0) {
            $output = array();
            foreach ($model as $value) {
                $data['examination_type_name']  = $value['examTypeUu']['examination_type_name'];
                $data['class_name']             = $value['examClassUu']['classwise_class_name'];
                $data['subject_name']           = $value['examSubjectUu']['subject_name'];
                $data['exam_date']              = $value['exam_date'];
                $data['examination_start_time'] = $value['examination_start_time'];
                $data['examination_end_time']   = $value['examination_end_time'];
                $output[]                       = $data;
            }
            $response['status'] = 200;
            $response['detail'] = $output;
        } else {
            $response['statu']   = 201;
            $response['message'] = 'No data found!';
        }
        echo json_encode(array(
            "response" => $response
        ));
    }
    /**
    
    * Login functionality for ErpMasterTeacher
    
    * @return mixed
    
    */
    public function actionGetexamcalendar()
    {
        $_POST    = json_decode(file_get_contents('php://input'), TRUE);
        $typeUuid = $_POST['typeUuid'];
        $model = ErpMasterExamination::find()->with('examSubjectUu')->with('examTypeUu')->with('examClassUu')->where(['exam_type_uuid' => $typeUuid])->asArray()->all();
        $count    = count($model);
        if ($count > 0) {
            $output = array();
            foreach ($model as $value) {
                $data['class_name']             = $value['examClassUu']['classwise_class_name'];
                $data['subject_name']           = $value['examSubjectUu']['subject_name'];
                $data['exam_date']              = $value['exam_date'];
                $data['examination_start_time'] = $value['examination_start_time'];
                $data['examination_end_time']   = $value['examination_end_time'];
                $output[]                       = $data;
            }
            $response['status'] = 200;
            //$response['examination_type_name']  = $value[0]['examTypeUu']['examination_type_name'];
            $response['detail'] = $output;
        } else {
            $response['statu']   = 201;
            $response['message'] = 'No data found!';
        }
        echo json_encode(array(
            "response" => $response
        ));
    }
    /**
    
    * Login functionality for ErpMasterTeacher
    
    * @return mixed
    
    */
    public function actionGetstudent()
    {
        $_POST       = json_decode(file_get_contents('php://input'), TRUE);
        $classUuid   = $_POST['classUuid'];
        $sectionUuid = $_POST['sectionUuid'];
        $model = ErpStudentInformation::find()->with('studentClassUu')->with('sectionUu')->where(['student_class_uuid' => $classUuid,'section_uuid' => $sectionUuid])->asArray()->all();
        $count       = count($model);
        if ($count > 0) {
            $output = array();
            foreach ($model as $value) {
                $data['student_uuid']        = $value['student_uuid'];
                $data['student_first_name']  = $value['student_first_name'];
                $data['student_last_name']   = $value['student_last_name'];
                $data['student_roll_number'] = $value['student_roll_number'];
                $data['class_name']          = $value['studentClassUu']['class_title'];
                $data['section_name']        = $value['sectionUu']['section_title'];
                $output[]                    = $data;
            }
            $response['status'] = 200;
            //$response['examination_type_name']  = $value[0]['examTypeUu']['examination_type_name'];
            $response['detail'] = $output;
        } else {
            $response['statu']   = 201;
            $response['message'] = 'No data found!';
        }
        echo json_encode(array(
            "response" => $response
        ));
    }
    /**
    
    * Login functionality for ErpMasterTeacher
    
    * @return mixed
    
    */
    public function actionStudentattendance()
    {
        $_POST      = json_decode(file_get_contents('php://input'), TRUE);
		

		
        foreach ($_POST['studentdetail'] as $key => $value) {
			
			$model = ErpScholAttendance::find()->where(['school_attendance_student_uuid' => $value['studentUuid'], 'school_attendance_section_uuid' => $_POST['sectionUuid'], 'school_attendance_date' => $_POST['attendanceDate']])->one();
			
			$count = count($model);
			
			if($count == 0) {
			
			$model      = new ErpScholAttendance();
            $moduleName = "ErpScholAttendance"; //Module Name        
            $tableName  = "ErpScholAttendance"; //Table Class Nam
            $school_attendance_uuid                  = Yii::$app->utility->generateGuuid($moduleName, $tableName); //Generate UUID
            $model->school_attendance_uuid           = $school_attendance_uuid;
            $model->school_attendance_school_uuid    = $_POST['schoolUuid'];
            $model->school_attendance_class_uuid     = $_POST['classUuid'];
            $model->school_attendance_section_uuid   = $_POST['sectionUuid'];
            $model->school_attendance_date           = $_POST['attendanceDate'];
            $model->school_attendance_student_uuid   = $value['studentUuid'];
			$model->school_attendance_created_date   = date("Y-m-d h:i:s");
			}
            $model->school_attendance_student_status = $value['attendanceStatus'];
            if ($model->validate()) {
                $model->save();
            }
        }
        $response['status']  = 200;
        $response['message'] = 'Attendance Updated Successfully!';
        echo json_encode(array(
            "response" => $response
        ));
    }
    /**
    
    * Login functionality for ErpMasterTeacher
    
    * @return mixed
    
    */
    public function actionGetstudentattendance()
    {
        $_POST       = json_decode(file_get_contents('php://input'), TRUE);
        $classUuid   = $_POST['classUuid'];
        $sectionUuid = $_POST['sectionUuid'];
        $date        = $_POST['attendanceDate'];
        $model = ErpScholAttendance::find()->with('schoolAttendanceStudentUu')->where(['school_attendance_class_uuid' => $classUuid, 'school_attendance_section_uuid' => $sectionUuid, 'school_attendance_date' => $date])->all();
        $count       = count($model);
        if ($count > 0) {
            $output = array();
            foreach ($model as $value) {
                $data['student_uuid']              = $value['school_attendance_student_uuid'];
				$data['student_roll_number']       = $value['schoolAttendanceStudentUu']['student_roll_number'];
                $data['student']                   = $value['schoolAttendanceStudentUu']['student_first_name'] . ' ' . $value['schoolAttendanceStudentUu']['student_last_name'];
                $data['attendance_student_status'] = $value['school_attendance_student_status'];
                $output[]                          = $data;
            }
            $response['status'] = 200;
            //$response['examination_type_name']  = $value[0]['examTypeUu']['examination_type_name'];
            $response['detail'] = $output;
        } else {
            $response['statu']   = 201;
            $response['message'] = 'No data found!';
        }
        echo json_encode(array(
            "response" => $response
        ));
    }
    /**
    
    * Login functionality for ErpMasterTeacher
    
    * @return mixed
    
    */
    public function actionStudentfeedback()
    {
        $_POST      = json_decode(file_get_contents('php://input'), TRUE);
        foreach ($_POST['studentdetail'] as $key => $value) {
            $model      = new ErpScholFeedback();
            $moduleName = "ErpScholFeedback"; //Module Name        
            $tableName  = "ErpScholFeedback"; //Table Class Nam
            $school_feedback_uuid                = Yii::$app->utility->generateGuuid($moduleName, $tableName); //Generate UUID
            $model->school_feedback_uuid         = $school_feedback_uuid;
            $model->school_feedback_school_uuid  = $_POST['schoolUuid'];
            $model->school_feedback_class_uuid   = $_POST['classUuid'];
            $model->school_feedback_section_uuid = $_POST['sectionUuid'];
            $model->school_feedback_date         = $_POST['attendanceDate'];
            $model->school_feedback_student_uuid = $value['studentUuid'];
            $model->school_feedback_des          = $value['feedback_des'];
            $model->school_feedback_created_date = date("Y-m-d h:i:s");
            if ($model->validate()) {
                $model->save();
            }
        }
        $response['status']  = 200;
        $response['message'] = 'Feedback Updated Successfully!';
        echo json_encode(array(
            "response" => $response
        ));
    }
    /**
    
    * Login functionality for ErpMasterTeacher
    
    * @return mixed
    
    */
    public function actionGetstudentfeedback()
    {
        $_POST       = json_decode(file_get_contents('php://input'), TRUE);
        $classUuid   = $_POST['classUuid'];
        $sectionUuid = $_POST['sectionUuid'];
        $date        = $_POST['feedbackDate'];
        $model = ErpScholFeedback::find()->with('schoolFeedbackStudentUu')->where(['school_feedback_class_uuid' => $classUuid, 'school_feedback_section_uuid' => $sectionUuid, 'school_feedback_date' => $date])->all();
        $count       = count($model);
        if ($count > 0) {
            $output = array();
            foreach ($model as $value) {
                $data['student_uuid']        = $value['school_feedback_student_uuid'];
				$data['student_roll_number'] = $value['schoolFeedbackStudentUu']['student_roll_number'];
                $data['student']             = $value['schoolFeedbackStudentUu']['student_first_name'] . ' ' . $value['schoolFeedbackStudentUu']['student_last_name'];
                $data['school_feedback_des'] = $value['school_feedback_des'];
                $output[]                    = $data;
            }
            $response['status'] = 200;
            //$response['examination_type_name']  = $value[0]['examTypeUu']['examination_type_name'];
            $response['detail'] = $output;
        } else {
            $response['statu']   = 201;
            $response['message'] = 'No data found!';
        }
        echo json_encode(array(
            "response" => $response
        ));
    }
    /**
    
    * Login functionality for ErpMasterTeacher
    
    * @return mixed
    
    */
    public function actionGetleavestatus()
    {
        $_POST       = json_decode(file_get_contents('php://input'), TRUE);
        $classUuid   = $_POST['classUuid'];
        $sectionUuid = $_POST['sectionUuid'];
        $date        = $_POST['leaveDate'];
        $model = ErpStudentLeaveStatus::find()->with('studentClassUu')->with('studentUu')->with('studentSectionUu')->where(['student_class_uuid' => $classUuid, 'student_section_uuid' => $sectionUuid, 'student_leave_from_date' => $date])->all();
        $count       = count($model);
        if ($count > 0) {
            $output = array();
            foreach ($model as $value) {
				$data['studentUuid']         = $value['studentUu']['student_uuid'];
				$data['student_roll_number'] = $value['studentUu']['student_roll_number'];
				$data['studentName']         = $value['studentUu']['student_first_name'].' '.$value['studentUu']['student_last_name'];
				$data['classUuid']           = $value['studentClassUu']['classwise_uuid'];
				$data['className']           = $value['studentClassUu']['classwise_class_name'];
				$data['sectionUuid']         = $value['studentSectionUu']['section_uuid'];
				$data['sectionName']         = $value['studentSectionUu']['section_title'];
				$data['leave_uuid']          = $value['student_leave_uuid'];
				$data['leave_uuid']          = $value['student_leave_uuid'];
				$data['startDate']           = $value['student_leave_from_date'];
				$data['endDate']             = $value['student_leave_to_date'];
				$data['reason']              = $value['student_leave_description'];
                $data['status']              = $value['student_leave_status'];
                
                $output[]                    = $data;
            }
            $response['status'] = 200;
            //$response['examination_type_name']  = $value[0]['examTypeUu']['examination_type_name'];
            $response['detail'] = $output;
        } else {
            $response['statu']   = 201;
            $response['message'] = 'No data found!';
        }
        echo json_encode(array(
            "response" => $response
        ));
    }
    /**
    
    * Login functionality for ErpMasterTeacher
    
    * @return mixed
    
    */
    public function actionApproveleave()
    {
        $_POST       = json_decode(file_get_contents('php://input'), TRUE);
        $leaveUuid   = $_POST['leaveUuid'];
        $status      = $_POST['status'];
       
        $model       = ErpStudentLeaveStatus::find()->where(['student_leave_uuid' => $leaveUuid, 'student_leave_is_deleted' => 'NO'])->one();
        $count       = count($model);
        if ($count > 0) {
            $model->student_leave_status = $status;
            if ($model->save()) {
                $response['status']  = 200;
                $response['message'] = 'Leave Updated Successfully';
            } else {
                $response['status']  = 500;
                $response['message'] = 'Internal Server Error';
            }
        } else {
            $response['statu']   = 201;
            $response['message'] = 'No data found!';
        }
        echo json_encode(array(
            "response" => $response
        ));
    }
    /**
    
    * Login functionality for ErpMasterTeacher
    
    * @return mixed
    
    */
    public function actionStudentlisting()
    {
        $_POST       = json_decode(file_get_contents('php://input'), TRUE);
        $classUuid   = $_POST['classUuid'];
        $sectionUuid = $_POST['sectionUuid'];
       
        $model = ErpStudentInformation::find()->with('studentClassUu')->with('sectionUu')->where(['student_class_uuid' => $classUuid, 'section_uuid' => $sectionUuid, 'student_is_status' => 'ACTIVE', 'student_is_delete' => 'NO'])->all();
        $count       = count($model);
        if ($count > 0) {
            $output = array();
            foreach ($model as $value) {
				$data['studentUuid']         = $value['student_uuid'];
				$data['studentRoll']         = $value['student_roll_number'];
				$data['studentName']         = $value['student_first_name'].' '.$value['student_last_name'];
				$data['classUuid']           = $value['studentClassUu']['classwise_uuid'];
				$data['className']           = $value['studentClassUu']['classwise_class_name'];
				$data['sectionUuid']         = $value['sectionUu']['section_uuid'];
				$data['sectionName']         = $value['sectionUu']['section_title'];

                $output[]                    = $data;
            }
            $response['status'] = 200;
            //$response['examination_type_name']  = $value[0]['examTypeUu']['examination_type_name'];
            $response['detail'] = $output;
        } else {
            $response['statu']   = 201;
            $response['message'] = 'No data found!';
        }
        echo json_encode(array(
            "response" => $response
        ));
    }
    /**
    
    * Login functionality for ErpMasterTeacher
    
    * @return mixed
    
    */
    public function actionGetclasswisesubject()
    {
        $_POST       = json_decode(file_get_contents('php://input'), TRUE);
        $classUuid   = $_POST['classUuid'];
        $sectionUuid   = $_POST['sectionUuid'];
        $model = TeacherSubjectMapping::find()->joinWith('subjectUu AS subject')->joinWith('classUu')->joinWith('classUu.classwiseClassName AS class')->alias('mapping')->where(['class.class_uuid' => $classUuid,'mapping.section_uuid' => $sectionUuid,'subject.subject_status' => 'ACTIVE', 'subject.subject_is_deleted' => 'NO'])->all();
        $count       = count($model);
        if ($count > 0) {
            $output = array();
            foreach ($model as $value) {
				$data['subjectUuid']         = $value['subjectUu']['subject_uuid'];
				$data['subjectname']         = $value['subjectUu']['subject_name'];
                $output[]                    = $data;
            }
            $response['status'] = 200;
            $response['detail'] = $output;
        } else {
            $response['statu']   = 201;
            $response['message'] = 'No data found!';
        }
        echo json_encode(array(
            "response" => $response
        ));
    } 
    /**
    
    * Login functionality for ErpMasterTeacher
    
    * @return mixed
    
    */
    public function actionStudenthomework()
    {
        $_POST       = json_decode(file_get_contents('php://input'), TRUE);
		$teacherUuid   = $_POST['teacherUuid'];
        $classUuid   = $_POST['classUuid'];
		$sectionUuid   = $_POST['sectionUuid'];
		$subjectUuid   = $_POST['subjectUuid'];
		$homeworkDate   = $_POST['homeworkDate'];
		$homeworkDes   = $_POST['homeworkDes'];
		
		$model = new ErpStudentHomeWork();
		
		$moduleName                             = "ErpStudentHomeWork"; //Module Name        
		$tableName                              = "ErpStudentHomeWork"; //Table Class Nam
		$student_home_work_uuid        = Yii::$app->utility->generateGuuid($moduleName, $tableName); //Generate UUID  
		$model->student_home_work_uuid = $student_home_work_uuid;
		$model->teacher_uuid = $teacherUuid;
		$model->class_uuid = $classUuid;
		$model->section_uuid = $sectionUuid;
		$model->subject_uuid = $subjectUuid;
		$model->student_home_work_date = $homeworkDate;
		$model->student_home_work_des = $homeworkDes;
		$model->student_home_work_created_date = date("Y-m-d h:i:s");
		
		if ($model->validate()) {
			if ($model->save()) {
				$response['status']  = 200;
				$response['message'] = 'Home Work Updated Successfully';
				$response['homeworkUuid']  = $student_home_work_uuid;
			} else {
				$response['status']  = 500;
				$response['message'] = 'Internal Server Error';
			}
		} else {
			
			$response['status']  = 201;
			$response['message'] = 'PLease fill all required fields';
		}

        echo json_encode(array(
            "response" => $response
        ));

    }
}

