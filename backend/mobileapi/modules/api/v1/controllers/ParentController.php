<?php
namespace app\modules\api\v1\controllers;
use Yii;
use app\modules\api\v1\models\ErpStudentInformation;
use app\modules\api\v1\models\ErpStudentParentAssociation;
use app\modules\api\v1\models\ErpStudentLeaveStatus;
use app\modules\api\v1\models\ErpScholAttendance;
use app\modules\api\v1\models\ErpScholFeedback;
use app\modules\api\v1\models\ErpMasterExaminationType;
use app\modules\api\v1\models\ErpStudentHomeWork;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
/**

* ParentController implements the CRUD actions for ParentController model.

*/
class ParentController extends \yii\web\Controller
{
    /**
    
    * @inheritdoc
    
    */
    public function beforeAction($action)
    {
        if ($action->id == 'index' || $action->id == 'studentleave' || $action->id == 'register' || $action->id == 'login' || $action->id == 'getchild' || $action->id == 'studentattendance' || $action->id == 'studentfeedback' || $action->id == 'examtype' || $action->id == 'getstudenthomework') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }
    /**
    
    * Login functionality for ErpMasterTeacher
    
    * @return mixed
    
    */
    public function actionRegister()
    {
        $_POST = json_decode(file_get_contents('php://input'), TRUE);
        
        
        if(isset($_POST['parentfatherMobile'])) {
        
           $parentMobile = $_POST['parentfatherMobile'];
        
           $student = ErpStudentInformation::find()->where(['father_contact_number' => $parentMobile])->asArray()->one();
           
           $message = "Register Sucessfully as ".$student['student_first_name']." ".$student['student_last_name']." Father";

        } elseif(isset($_POST['parentmotherMobile'])) {
        
           $parentMobile = $_POST['parentmotherMobile'];

           $student = ErpStudentInformation::find()->where(['mother_contact_number' => $parentMobile])->asArray()->one();
           
           $message = "Register Sucessfully as ".$student['student_first_name']." ".$student['student_last_name']." Mother";

        }
        
        $count = count($student);
        
        if ($count > 0) {
            
            $checkvalidate = ErpStudentParentAssociation::find()->where(['parent_mobile_number' => $parentMobile])->count();
            
            if ($checkvalidate == 0) {
                
                $model = new ErpStudentParentAssociation();
                
                $moduleName                             = "ErpStudentParentAssociation"; //Module Name        
                $tableName                              = "ErpStudentParentAssociation"; //Table Class Nam
                $student_parent_association_uuid        = Yii::$app->utility->generateGuuid($moduleName, $tableName); //Generate UUID  
                $model->student_parent_association_uuid = $student_parent_association_uuid;
                $model->parent_mobile_number            = $parentMobile;
                $model->parent_password                 = md5($_POST['parentPassword']);
                $model->parent_created_date             = date("Y-m-d h:i:s");
                
                if ($model->validate()) {
                    if ($model->save()) {
                        $response['status']  = 200;
                        $response['message'] = $message;
                    } else {
                        $response['status']  = 500;
                        $response['message'] = 'Internal Server Error';
                    }
                } else {
                    
                    $response['status']  = 201;
                    $response['message'] = 'PLease fill all required fields';
                }
                
            } else {
                
                $response['status']  = 201;
                $response['message'] = 'Already register with this mobile number';
                
            }
            
        } else {
            
            $response['status']  = 201;
            $response['message'] = 'No Student found with this mobile number!';
            
        }
        
        
        echo json_encode(array(
            "response" => $response
        ));
    }
    /**
    
    * Login functionality for ErpMasterTeacher
    
    * @return mixed
    
    */
    public function actionLogin()
    {
        //$this->enableCsrfValidation = false;
        $_POST            = json_decode(file_get_contents('php://input'), TRUE);
        $parent_contact    = $_POST['parent_contact'];
        $parent_password = md5($_POST['parent_password']);
        $model = ErpStudentParentAssociation::find()->where(['parent_mobile_number' => $parent_contact, 'parent_password' => $parent_password])->one();
        $count            = count($model);
        if ($count > 0) {
            $output['parent_uuid']        = $model->student_parent_association_uuid;
            $response['status']            = 200;
            $response['message']           = 'Login Successfully';
            $response['detail']            = $output;
        } else {
            $response['statu']   = 201;
            $response['message'] = 'Please Check Creadtinals!';
        }
        echo json_encode(array(
            "response" => $response
        ));
    }
    /**
    
    * Login functionality for ErpMasterTeacher
    
    * @return mixed
    
    */
    public function actionGetchild()
    {
        $_POST       = json_decode(file_get_contents('php://input'), TRUE);
        $parentMobile   = $_POST['parent_contact'];
        $model = ErpStudentInformation::find()->with('studentClassUu')->with('sectionUu')->where(['father_contact_number' => $parentMobile])->all();
        $count       = count($model);
        if ($count > 0) {
            $output = array();
            foreach ($model as $value) {
				$data['schollUuid']          = $value['student_school_uuid'];
				$data['studentUuid']         = $value['student_uuid'];
				$data['student_name']         = $value['student_first_name'].' '.$value['student_last_name'];
				$data['student_roll_number'] = $value['student_roll_number'];
				$data['student_class_uuid'] = $value['student_class_uuid'];
			    $data['student_class_name'] = $value['studentClassUu']['class_title'];
				$data['section_uuid']        = $value['section_uuid']; 
				$data['student_section_name'] = $value['sectionUu']['section_title'];
                $output[]                    = $data;
            }
            $response['status'] = 200;
            $response['detail'] = $output;
        } else {
            $response['statu']   = 201;
            $response['message'] = 'No data found!';
        }
        echo json_encode(array(
            "response" => $response
        ));
    }
    /**
    
    * Login functionality for ErpMasterTeacher
    
    * @return mixed
    
    */
    public function actionStudentleave()
    {
        $_POST = json_decode(file_get_contents('php://input'), TRUE);
        
        $model                             = new ErpStudentLeaveStatus();
        $moduleName                        = "ErpStudentLeaveStatus"; //Module Name        
        $tableName                         = "ErpStudentLeaveStatus"; //Table Class Nam
        $student_leave_uuid                = Yii::$app->utility->generateGuuid($moduleName, $tableName); //Generate UUID
        $model->student_leave_uuid         = $student_leave_uuid;
        $model->student_uuid               = $_POST['studentUuid'];
        $model->student_class_uuid         = $_POST['classUuid'];
        $model->student_section_uuid       = $_POST['sectionUuid'];
        $model->student_leave_from_date    = $_POST['fromDate'];
        $model->student_leave_to_date      = $_POST['toDate'];
        $model->student_leave_description  = $_POST['leave_des'];
        $model->student_leave_created_date = date("Y-m-d h:i:s");
        if ($model->validate()) {
            if ($model->save()) {
                $response['status']  = 200;
                $response['message'] = 'Leave Apply Successfully';
            } else {
                $response['status']  = 500;
                $response['message'] = 'Internal Server Error';
            }
        } else {
            
            $response['status']  = 201;
            $response['message'] = 'PLease fill all required fields';
        }
        echo json_encode(array(
            "response" => $response
        ));
    }
	    /**
    
    * Login functionality for ErpMasterTeacher
    
    * @return mixed
    
    */
    public function actionStudentattendance()
    {
        $_POST       = json_decode(file_get_contents('php://input'), TRUE);
        $studentUuid   = $_POST['studentUuid'];
		$classUuid   = $_POST['classUuid'];
		$sectionUuid   = $_POST['sectionUuid'];
        $model = ErpScholAttendance::find()->where(['school_attendance_student_uuid' => $studentUuid, 'school_attendance_class_uuid' => $classUuid, 'school_attendance_section_uuid' => $sectionUuid])->all();
        $count       = count($model);
        if ($count > 0) {
            $output = array();
            foreach ($model as $value) {
				$data['school_attendance_date']           = $value['school_attendance_date'];
				$data['school_attendance_student_status'] = $value['school_attendance_student_status'];
				
                $output[]                    = $data;
            }
            $response['status'] = 200;
            $response['detail'] = $output;
        } else {
            $response['statu']   = 201;
            $response['message'] = 'No data found!';
        }
        echo json_encode(array(
            "response" => $response
        ));
    }
	    /**
    
    * Login functionality for ErpMasterTeacher
    
    * @return mixed
    
    */
    public function actionStudentfeedback()
    {
        $_POST       = json_decode(file_get_contents('php://input'), TRUE);
        $studentUuid   = $_POST['studentUuid'];
		$classUuid   = $_POST['classUuid'];
		$sectionUuid   = $_POST['sectionUuid'];
        $model = ErpScholFeedback::find()->where(['school_feedback_student_uuid' => $studentUuid, 'school_feedback_class_uuid' => $classUuid, 'school_feedback_section_uuid' => $sectionUuid, 'school_feedback_status' => 'ACTIVE', 'school_feedback_deleted' => 'NO'])->all();
        $count       = count($model);
        if ($count > 0) {
            $output = array();
            foreach ($model as $value) {
				$data['school_feedback_date']           = $value['school_feedback_date'];
				$data['school_feedback_des']             = $value['school_feedback_des'];
				
                $output[]                    = $data;
            }
            $response['status'] = 200;
            $response['detail'] = $output;
        } else {
            $response['statu']   = 201;
            $response['message'] = 'No data found!';
        }
        echo json_encode(array(
            "response" => $response
        ));
    }
    /**
    
    * Login functionality for ErpMasterTeacher
    
    * @return mixed
    
    */
    public function actionExamtype()
    {
        $_POST       = json_decode(file_get_contents('php://input'), TRUE);
		$schoolUuid   = $_POST['school_uuid'];
        $model = ErpMasterExaminationType::find()->where(['school_uuid' => $schoolUuid,'examination_type_status' => 'ACTIVE', 'examination_is_deleted' => 'NO'])->all();
        $count       = count($model);
        if ($count > 0) {
            $output = array();
            foreach ($model as $value) {
				$data['examination_type_uuid']           = $value['examination_type_uuid'];
				$data['examination_type_name']             = $value['examination_type_name'];
				$data['examination_start_date']             = $value['examination_start_date'];
				$data['examination_end_date']             = $value['examination_end_date'];
				
                $output[]                    = $data;
            }
            $response['status'] = 200;
            $response['detail'] = $output;
        } else {
            $response['statu']   = 201;
            $response['message'] = 'No data found!';
        }
        echo json_encode(array(
            "response" => $response
        ));
    }
	/**
    
    * Login functionality for ErpMasterTeacher
    
    * @return mixed
    
    */
    public function actionGetstudenthomework()
    {
        $_POST       = json_decode(file_get_contents('php://input'), TRUE);
		$classUuid   = $_POST['classUuid'];
		$sectionUuid   = $_POST['sectionUuid'];
		$homeworkDate   = $_POST['homeworkDate'];
        $model = ErpStudentHomeWork::find()->with('subjectUu')->where(['class_uuid' => $classUuid,'section_uuid' => $sectionUuid, 'student_home_work_date' => $homeworkDate, 'student_home_work_status' => 'ACTIVE', 'student_home_work_is_deleted' => 'NO'])->all();
        $count       = count($model);
        if ($count > 0) {
            $output = array();
            foreach ($model as $value) {
				$data['student_home_work_uuid']            = $value['student_home_work_uuid'];
				$data['subject']                           = $value['subjectUu']['subject_name'];
				$data['subject_uuid']                      = $value['subject_uuid'];
				$data['student_home_work_des']             = $value['student_home_work_des'];
				
                $output[]                    = $data;
            }
            $response['status'] = 200;
            $response['detail'] = $output;
        } else {
            $response['statu']   = 201;
            $response['message'] = 'No data found!';
        }
        echo json_encode(array(
            "response" => $response
        ));
    }
}
