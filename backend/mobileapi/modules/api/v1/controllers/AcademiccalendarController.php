<?php

namespace app\modules\api\v1\controllers;
use Yii;
use app\modules\api\v1\models\ErpMasterAcademicCalendar;
use app\modules\api\v1\models\ErpSchoolNotification;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
/**
 * AcademiccalendarController implements the CRUD actions for ErpMasterAcademicCalendar model.
 */
class AcademiccalendarController extends \yii\web\Controller
{
	/**
	 * @inheritdoc
	 */
	public function beforeAction($action)
	{            
		if ($action->id == 'index' || $action->id == 'getnotifications') {
			$this->enableCsrfValidation = false;
		}
	
		return parent::beforeAction($action);
	}
    /**
     * Lists all ErpMasterAcademicCalendar models.
     * @return mixed
     */
    public function actionIndex()
    {
		$_POST =json_decode(file_get_contents('php://input'), TRUE);
		
		$academic_school_uuid = $_POST['schoolUuid'];
		
		$model = ErpMasterAcademicCalendar::find()->select(['academic_title','academic_description','academic_date'])->where(['academic_school_uuid' => $academic_school_uuid, 'academic_is_status' => 'ACTIVE', 'academic_is_deleted' => 'NO'])->asArray()->all();
		
		$response['status']  = 200;
		$response['message'] = 'Academic Calendar';
        $response['detail'] = $model;


        echo json_encode(array("response" => $response));
    }
    /**
     * Lists all ErpMasterAcademicCalendar models.
     * @return mixed
     */
    public function actionGetnotifications()
    {
		$_POST =json_decode(file_get_contents('php://input'), TRUE);
		
		$notification_school_uuid = $_POST['schoolUuid'];
		
		$model = ErpSchoolNotification::find()->select(['notification_title','notification_description','notification_date'])->where(['notification_school_uuid' => $notification_school_uuid, 'notification_is_status' => 'ACTIVE', 'notification_is_deleted' => 'NO'])->asArray()->all();
		
		$response['status']  = 200;
		$response['message'] = 'School Notifications';
        $response['detail'] = $model;
		
        echo json_encode(array("response" => $response));
    }

}
