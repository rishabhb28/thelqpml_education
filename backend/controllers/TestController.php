<?php

namespace backend\controllers;

use Yii;
use app\models\DemoTable;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DemotableController implements the CRUD actions for DemoTable model.
 */
class TestController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DemoTable models.
     * @return mixed
     */
    public function actionIndex()
    {
           echo "hjhjh";
    }


}
