<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[DemoTable]].
 *
 * @see DemoTable
 */
class DemoTableQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return DemoTable[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return DemoTable|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
