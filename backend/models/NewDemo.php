<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "new_demo".
 *
 * @property int $newid
 * @property string $student_number
 * @property string $total_marks
 * @property string $obtained_marks_
 * @property int $student_id
 *
 * @property DemoTable $new
 */
class NewDemo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'new_demo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student_number', 'total_marks', 'obtained_marks_', 'student_id'], 'required'],
            [['student_id'], 'integer'],
            [['student_number', 'total_marks', 'obtained_marks_'], 'string', 'max' => 50],
            [['newid'], 'exist', 'skipOnError' => true, 'targetClass' => DemoTable::className(), 'targetAttribute' => ['newid' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'newid' => 'Newid',
            'student_number' => 'Student Number',
            'total_marks' => 'Total Marks',
            'obtained_marks_' => 'Obtained Marks',
            'student_id' => 'Student ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNew()
    {
        return $this->hasOne(DemoTable::className(), ['id' => 'newid']);
    }

    /**
     * @inheritdoc
     * @return NewDemoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new NewDemoQuery(get_called_class());
    }
}
