<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[NewDemo]].
 *
 * @see NewDemo
 */
class NewDemoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return NewDemo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return NewDemo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
