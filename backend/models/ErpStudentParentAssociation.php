<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "erp_student_parent_association".
 *
 * @property int $student_parent_association_id
 * @property string $student_parent_association_uuid
 * @property string $parent_mobile_number
 * @property string $parent_password
 * @property string $parent_status
 * @property string $parent_is_deleted
 * @property string $parent_created_date
 */
class ErpStudentParentAssociation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'erp_student_parent_association';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student_parent_association_uuid', 'parent_mobile_number', 'parent_password', 'parent_created_date'], 'required'],
            [['parent_status', 'parent_is_deleted'], 'string'],
            [['parent_created_date'], 'safe'],
            [['student_parent_association_uuid'], 'string', 'max' => 128],
            [['parent_mobile_number'], 'string', 'max' => 10],
            [['parent_password'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'student_parent_association_id' => 'Student Parent Association ID',
            'student_parent_association_uuid' => 'Student Parent Association Uuid',
            'parent_mobile_number' => 'Parent Mobile Number',
            'parent_password' => 'Parent Password',
            'parent_status' => 'Parent Status',
            'parent_is_deleted' => 'Parent Is Deleted',
            'parent_created_date' => 'Parent Created Date',
        ];
    }
}
