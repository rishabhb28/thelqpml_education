<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $user_uuid
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $phone
 * @property int $status
 * @property int $role
 * @property string $created_at
 * @property string $updated_at
 *
 * @property ErpSchoolInformation[] $erpSchoolInformations
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_uuid', 'username', 'auth_key','email', 'phone', 'role', 'created_at'], 'required'],
            [['status', 'role'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_uuid'], 'string', 'max' => 128],
            [['username', 'password_hash', 'password_reset_token'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['email'], 'string', 'max' => 100],
            [['phone'], 'string', 'max' => 10],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['user_uuid'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_uuid' => 'User Uuid',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'phone' => 'Phone',
            'status' => 'Status',
            'role' => 'Role',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getErpSchoolInformations()
    {
        return $this->hasMany(ErpSchoolInformation::className(), ['school_main_uuid' => 'user_uuid']);
    }

    /**
     * @inheritdoc
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }
}
