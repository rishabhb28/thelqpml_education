<?php

namespace backend\models;

use yii;

use yii\base\Model;

use common\models\User;



/**

 * Signup form

 */

class SignupForm extends Model

{

    public $username;

    public $email;
	
	public $role; 

    public $password;

	public $user_uuid;

	public $created_at;

	public $updated_at;







    /**

     * @inheritdoc

     */

    public function rules()

    {

        return [

		    ['user_uuid', 'trim'],

			['user_uuid', 'required'],

			['user_uuid', 'string', 'min' => 2, 'max' => 255],

			

            ['username', 'trim'],

            ['username', 'required'],

            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],

            ['username', 'string', 'min' => 2, 'max' => 255],

     

            ['email', 'trim'],

            ['email', 'required'],

            ['email', 'email'],

            ['email', 'string', 'max' => 255],

            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],



            ['password', 'required'],

            ['password', 'string', 'min' => 6],

        ];

    }



    /**

     * Signs user up.

     *

     * @return User|null the saved model or null if saving fails

     */

    public function signup()

    {

        if (!$this->validate()) {

           // return null;

			print_r($this->getErrors());

        }

        $user = new User();

		

        $moduleName = "User"; //Module Name        

		$tableName = "User"; //Table Class Nam

		$user_uuid = Yii::$app->utility->generateGuuid($moduleName, $tableName); //Generate UUID

		

		$user->user_uuid = $user_uuid;

        $user->username = $this->username;

        $user->email = $this->email;

        $user->setPassword($this->password);

        $user->generateAuthKey();
		
		$user->role = $this->role;

		$user->created_at = $this->created_at;

		$user->updated_at = $this->updated_at;

        

        return $user->save() ? $user : null;

    }

}

