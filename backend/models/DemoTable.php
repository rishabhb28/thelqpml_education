<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "demo_table".
 *
 * @property int $id
 * @property string $name
 * @property string $classs
 * @property string $room
 * @property string $section
 *
 * @property NewDemo $newDemo
 */
class DemoTable extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'demo_table';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'classs', 'room', 'section'], 'required'],
            [['name', 'classs', 'room', 'section'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name12',
            'classs' => 'Classs',
            'room' => 'Room',
            'section' => 'Section',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewDemo()
    {
        return $this->hasOne(NewDemo::className(), ['newid' => 'id']);
    }

    /**
     * @inheritdoc
     * @return DemoTableQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DemoTableQuery(get_called_class());
    }
}
