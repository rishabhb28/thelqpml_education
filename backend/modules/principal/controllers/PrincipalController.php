<?php

namespace app\modules\principal\controllers;

use Yii;
use app\modules\principal\models\ErpMasterPrincipal;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PrincipalController implements the CRUD actions for ErpMasterPrincipal model.
 */
class PrincipalController extends Controller
{
    /**
     * @inheritdoc
     */
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
	public function behaviors()
		{
			return [
				'verbs' => [
					'class' => VerbFilter::className(),
					'actions' => [
						//'delete' => ['post'],
					],
				],
				'access' => [
							'class' => \yii\filters\AccessControl::className(),
							'only' => ['index','create','update','view','delete'],
							'rules' => [
								// allow authenticated users
								[
									'allow' => true,
									'roles' => ['@'],
								],
								// everything else is denied
							],
						],            
			];
		}

    /**
     * Lists all ErpMasterPrincipal models.
     * @return mixed
     */
    public function actionIndex()
    {
		$principal_school_uuid = Yii::$app->utility->getLoginschooluuid();
		
        $dataProvider = new ActiveDataProvider([
            'query' => ErpMasterPrincipal::find(),
        ]);
		
		$dataProvider->query->where(['principal_school_uuid' => $principal_school_uuid]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ErpMasterPrincipal model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ErpMasterPrincipal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ErpMasterPrincipal();
		
		$moduleName = "ErpMasterPrincipal"; //Module Name        

		$tableName = "ErpMasterPrincipal"; //Table Class Nam

		$principal_uuid = Yii::$app->utility->generateGuuid($moduleName, $tableName); //Generate UUID

        if ($model->load(Yii::$app->request->post())) { 
		
		$model->principal_uuid = $principal_uuid;
		
		$model->principal_school_uuid = Yii::$app->utility->getLoginschooluuid();
		
		$model->principal_employemdnt_date = date("Y-m-d",strtotime($model->principal_employemdnt_date));
		
		$model->principal_dob = date("Y-m-d",strtotime($model->principal_dob));
		
		$model->principal_created_date = date("Y-m-d h:i:s");
		
		    if($model->save()) {
               return $this->redirect(['index']);
			} else {
			   print_r($model->getErrors());
			   die;	
			}
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ErpMasterPrincipal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
			if($model->save()) {
               return $this->redirect(['view', 'id' => $model->principal_id]);
			} 
		   // $model->save();
            //return $this->redirect(['view', 'id' => $model->principal_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ErpMasterPrincipal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = ErpMasterPrincipal::find()->where(['principal_id' => $id])->one();
		
		$model->principal_is_deleted = 'YES';
        
		$model->save();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ErpMasterPrincipal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ErpMasterPrincipal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ErpMasterPrincipal::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
