<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Principals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="erp-master-principal-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Principal', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'principal_id',
            //'principal_uuid',
            //'principal_school_uuid',
            'principal_first_name',
            'principal_last_name',
            'principal_email:email',
            'principal_phone',
            //'principal_employee_number',
            'principal_employemdnt_date',
            'principal_dob',
            'principal_marital_status',
            'principal_address1',
            'principal_is_deleted',
            'principal_is_status',
            //'principal_created_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
