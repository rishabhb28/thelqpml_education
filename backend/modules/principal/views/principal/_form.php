<?php



use yii\helpers\Html;

use yii\widgets\ActiveForm;



/* @var $this yii\web\View */

/* @var $model app\modules\principal\models\ErpMasterprincipal */

/* @var $form ActiveForm */

?>

<div class="principal-_form">



    <?php $form = ActiveForm::begin(array('options'=>array("class"=>'smart-form'))); ?>

    

    <fieldset>

   <div class="row">

      <section class="col col-6">

         <label class="input">

         <?= $form->field($model, 'principal_first_name')->textInput(['placeholder' => 'First Name']) ?>

         </label>

      </section>

      <section class="col col-6">

         <label class="input">

         <?= $form->field($model, 'principal_last_name')->textInput(['placeholder' => 'Last Name'])?>

         </label>

      </section>

      <section class="col col-6">

         <label class="input">Employemdnt Date

         <input type="date" id="erpmasterprincipal-principal_employemdnt_date"  class="form-control" name="ErpMasterPrincipal[principal_employemdnt_date]" placeholder="Employemdnt Date" value="<?=$model->principal_employemdnt_date?>" aria-required="true" aria-invalid="true">

         </label>

      </section>

      <section class="col col-6">

         <label class="input">Date of Birth

         <input type="date" id="erpmasterprincipal-principal_dob" value="<?=$model->principal_dob?>" class="form-control" name="ErpMasterPrincipal[principal_dob]" placeholder="Date of Birth" aria-required="true">

         </label>

      </section>

   </div>

</fieldset>

<fieldset>

   <div class="row">

      <section class="col col-6">

         <label class="input">

         <?= $form->field($model, 'principal_address1')->textInput(['placeholder' => 'Enter Address']) ?>

         </label>

      </section>

      <section class="col col-6">

         <label class="input">

         <?= $form->field($model, 'principal_address2')->textInput(['placeholder' => 'Enter Address']) ?>

         </label>

      </section>

      <section class="col col-4">

         <label class="input">

         <?= $form->field($model, 'principal_address_city')->textInput(['placeholder' => 'Enter City']) ?>

         </label>

      </section>

      <section class="col col-4">

         <label class="input">

         <?= $form->field($model, 'principal_address_state')->textInput(['placeholder' => 'Enter State']) ?>

         </label>

      </section>

      <section class="col col-4">

         <label class="input">

         <?= $form->field($model, 'principal_address_zip')->textInput(['placeholder' => 'Enter Zip Code']) ?>

         </label>

      </section>

      </div>

  </fieldset>

 <fieldset>

   <div class="row">

      <section class="col col-4">

         <label class="input">

         <?= $form->field($model, 'principal_email')->textInput(['placeholder' => 'Email'])?>

         </label>

      </section>

      <section class="col col-4">

         <label class="input">

         <?= $form->field($model, 'principal_phone')->textInput(['placeholder' => 'Phone','max' => 10])?>

         </label>

      </section>

      <section class="col col-4">

         <label class="input">

         

          <?= $form->field($model, 'principal_marital_status')->dropDownList([ 'Single' => 'Single', 'Married' => 'Married', 'Widower' => 'Widower','Widow' => 'Widow','Divorced' => 'Divorced', 'Separated' => 'Separated' ], ['prompt' => 'Choose Marital']) ?>

         </label>

      </section>

   </div>

</fieldset>

<footer>

   <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>

</footer>



    <?php ActiveForm::end(); ?>



</div><!-- principal-_form -->

