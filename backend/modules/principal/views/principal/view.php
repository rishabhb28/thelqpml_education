<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\principal\models\ErpMasterPrincipal */

$this->title = $model->principal_id;
$this->params['breadcrumbs'][] = ['label' => 'Erp Master Principals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="erp-master-principal-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->principal_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->principal_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'principal_id',
            //'principal_uuid',
            //'principal_school_uuid',
            'principal_first_name',
            'principal_last_name',
            'principal_email:email',
            'principal_phone',
            //'principal_employee_number',
            'principal_employemdnt_date',
            'principal_dob',
            'principal_marital_status',
            'principal_address1',
			'principal_address2',
			'principal_address_city',
			'principal_address_state',
			'principal_address_zip',
            'principal_is_deleted',
            'principal_is_status',
            'principal_created_date',
        ],
    ]) ?>

</div>
