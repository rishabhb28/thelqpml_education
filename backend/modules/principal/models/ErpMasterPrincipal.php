<?php

namespace app\modules\principal\models;

use Yii;
use app\modules\schoolinfo\models\ErpSchoolInformation;
/**
 * This is the model class for table "erp_master_principal".
 *
 * @property int $principal_id
 * @property string $principal_uuid
 * @property string $principal_school_uuid
 * @property string $principal_first_name
 * @property string $principal_last_name
 * @property string $principal_email
 * @property string $principal_phone
 * @property string $principal_employee_number
 * @property string $principal_employemdnt_date
 * @property string $principal_dob
 * @property string $principal_marital_status
 * @property string $principal_address1
 * @property string $principal_address2
 * @property string $principal_address_city
 * @property string $principal_address_state
 * @property string $principal_address_zip
 * @property string $principal_is_deleted
 * @property string $principal_is_status
 * @property string $principal_created_date
 *
 * @property ErpSchoolInformation $principalSchoolUu
 */
class ErpMasterPrincipal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'erp_master_principal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['principal_uuid', 'principal_school_uuid', 'principal_first_name', 'principal_last_name', 'principal_email', 'principal_phone', 'principal_employemdnt_date', 'principal_dob', 'principal_address1', 'principal_address2', 'principal_address_city', 'principal_address_state', 'principal_address_zip', 'principal_created_date'], 'required'],
            [['principal_employemdnt_date', 'principal_dob', 'principal_created_date'], 'safe'],
            [['principal_marital_status', 'principal_is_deleted', 'principal_is_status'], 'string'],
            [['principal_uuid', 'principal_school_uuid'], 'string', 'max' => 128],
            [['principal_first_name', 'principal_last_name', 'principal_email'], 'string', 'max' => 50],
            [['principal_phone', 'principal_employee_number'], 'string', 'max' => 10],
            [['principal_address1', 'principal_address2'], 'string', 'max' => 255],
            [['principal_address_city', 'principal_address_state'], 'string', 'max' => 20],
            [['principal_address_zip'], 'string', 'max' => 6],
            [['principal_uuid'], 'unique'],
            [['principal_school_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpSchoolInformation::className(), 'targetAttribute' => ['principal_school_uuid' => 'school_uuid']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'principal_id' => 'Principal ID',
            'principal_uuid' => 'Principal Uuid',
            'principal_school_uuid' => 'Principal School Uuid',
            'principal_first_name' => 'First Name',
            'principal_last_name' => 'Last Name',
            'principal_email' => 'Email',
            'principal_phone' => 'Phone',
            'principal_employee_number' => 'Employee Number',
            'principal_employemdnt_date' => 'Employement Date',
            'principal_dob' => 'Dob',
            'principal_marital_status' => 'Marital Status',
            'principal_address1' => 'Address1',
            'principal_address2' => 'Address2',
            'principal_address_city' => 'Address City',
            'principal_address_state' => 'Address State',
            'principal_address_zip' => 'Zip Code',
            'principal_is_deleted' => 'Is Deleted',
            'principal_is_status' => 'Is Status',
            'principal_created_date' => 'Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrincipalSchoolUu()
    {
        return $this->hasOne(ErpSchoolInformation::className(), ['school_uuid' => 'principal_school_uuid']);
    }

    /**
     * @inheritdoc
     * @return ErpMasterPrincipalQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ErpMasterPrincipalQuery(get_called_class());
    }
}
