<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\student\models\ErpStudentInformationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="erp-student-information-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'student_id') ?>

    <?= $form->field($model, 'student_uuid') ?>

    <?= $form->field($model, 'student_school_uuid') ?>

    <?= $form->field($model, 'student_class_uuid') ?>

    <?= $form->field($model, 'student_session_uuid') ?>

    <?php // echo $form->field($model, 'student_name') ?>

    <?php // echo $form->field($model, 'student_dob') ?>

    <?php // echo $form->field($model, 'student_gender') ?>

    <?php // echo $form->field($model, 'student_nationality') ?>

    <?php // echo $form->field($model, 'student_address') ?>

    <?php // echo $form->field($model, 'student_phone') ?>

    <?php // echo $form->field($model, 'student_email') ?>

    <?php // echo $form->field($model, 'student_is_status') ?>

    <?php // echo $form->field($model, 'student_is_delete') ?>

    <?php // echo $form->field($model, 'student_created_date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
