<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\student\models\ErpStudentInformationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Erp Student Informations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="erp-student-information-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Erp Student Information', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'student_id',
            'student_first_name',
			'student_last_name',
            'student_dob',
            'student_gender',
            'student_nationality',
            //'student_address:ntext',
            'student_phone',
            'student_email:email',
            'student_is_status',
            'student_is_delete',
            //'student_created_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
