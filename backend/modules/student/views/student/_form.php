<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\student\models\ErpStudentInformation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="erp-student-information-form">

    <?php $form = ActiveForm::begin(array('options'=>array("class"=>'smart-form'))); ?>
    
    <fieldset>
   <div class="row">
      <section class="col col-4">
         <label class="input">
         <?= $form->field($model, 'student_first_name')->textInput(['placeholder' => 'Student First Name']) ?>
         </label>
      </section>
      <section class="col col-4">
         <label class="input">
         <?= $form->field($model, 'student_last_name')->textInput(['placeholder' => 'Student Last Email'])?>
         </label>
      </section>
      <section class="col col-4">
         <label class="input">
         <?= $form->field($model, 'student_roll_number')->textInput(['placeholder' => 'Student Roll Number'])?>
         </label>
      </section>
      <section class="col col-4">
         <label class="input">
         <?= $form->field($model, 'student_email')->textInput(['placeholder' => 'Student Email'])?>
         </label>
      </section>
      <section class="col col-4">
         <label class="input">
         <?= $form->field($model, 'student_phone')->textInput(['placeholder' => 'Student Phone'])?>
         </label>
      </section>
      <section class="col col-4">
         <label class="input">Date of Birth
          
         <input type="date" id="erpstudentinformation-student_dob" class="form-control" name="ErpStudentInformation[student_dob]" value="<?=$model->student_dob?>" placeholder="Date of Birth" aria-required="true" aria-invalid="true">
         </label>
      </section>
   </div>
</fieldset>
<fieldset>
   <div class="row">
      <section class="col col-3">
         <label class="input">
         <?= $form->field($model, 'student_class_uuid')->dropDownList([$classdetail], ['prompt' => 'Choose One']) ?>
         </label>
      </section>
      <section class="col col-3">
         <label class="input">
         <?= $form->field($model, 'section_uuid')->dropDownList([$sectiondetail], ['prompt' => 'Choose One']) ?>
         </label>
      </section>
      <section class="col col-3">
         <label class="input">
         <?= $form->field($model, 'student_gender')->dropDownList([ 'MALE' => 'MALE', 'FEMALE' => 'FEMALE', ], ['prompt' => 'Choose Gender']) ?>
         </label>
      </section>
      <section class="col col-3">
         <label class="input">
         <?= $form->field($model, 'student_nationality')->dropDownList([ 'INDIAN' => 'INDIAN', 'OTHER' => 'OTHER', ], ['prompt' => 'Choose Nationality']) ?>
         </label>
      </section>
         </div>
</fieldset>
<fieldset>
   <div class="row">
      <section class="col col-6">
         <label class="input">
         <?= $form->field($model, 'student_address1')->textInput(['placeholder' => 'Enter Address']) ?>
         </label>
      </section>
      <section class="col col-6">
         <label class="input">
         <?= $form->field($model, 'student_address2')->textInput(['placeholder' => 'Enter Address']) ?>
         </label>
      </section>
      <section class="col col-4">
         <label class="input">
         <?= $form->field($model, 'student_state')->textInput(['placeholder' => 'Enter City']) ?>
         </label>
      </section>
      <section class="col col-4">
         <label class="input">
         <?= $form->field($model, 'student_city')->textInput(['placeholder' => 'Enter State']) ?>
         </label>
      </section>
      <section class="col col-4">
         <label class="input">
         <?= $form->field($model, 'student_zip')->textInput(['placeholder' => 'Enter Zip Code']) ?>
         </label>
      </section>
      </div>
  </fieldset>
  <fieldset>
   <div class="row">
      <section class="col col-3">
         <label class="input">
         <?= $form->field($model, 'student_father_name')->textInput(['placeholder' => 'Father Name']) ?>
         </label>
      </section>
      <section class="col col-3">
         <label class="input">
         <?= $form->field($model, 'father_contact_number')->textInput(['placeholder' => 'Father Contact Number']) ?>
         </label>
      </section>
      <section class="col col-3">
         <label class="input">
         <?= $form->field($model, 'student_mother_name')->textInput(['placeholder' => 'Mother Name']) ?>
         </label>
      </section>
      <section class="col col-3">
         <label class="input">
         <?= $form->field($model, 'mother_contact_number')->textInput(['placeholder' => 'Mother Contact Number']) ?>
         </label>
      </section>
      
      </div>
  </fieldset>
<footer>
   <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
</footer>

    <?php ActiveForm::end(); ?>

</div>
