<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\student\models\ErpStudentInformation */

$this->title = $model->student_id;
$this->params['breadcrumbs'][] = ['label' => 'Erp Student Informations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="erp-student-information-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->student_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->student_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'student_id',
            'student_uuid',
            'student_school_uuid',
            'student_class_uuid',
            //'student_session_uuid',
            'student_first_name',
			'student_last_name',
            'student_dob',
            'student_gender',
            'student_nationality',
            'student_address1',
			'student_address2',
			'student_city',
			'student_state',
			'student_zip',
            'student_phone',
            'student_email:email',
            'student_is_status',
            'student_is_delete',
            'student_created_date',
        ],
    ]) ?>

</div>
