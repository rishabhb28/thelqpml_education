<?php

namespace app\modules\student\controllers;

use Yii;
use app\modules\student\models\ErpStudentInformation;
use app\modules\student\models\ErpStudentInformationSearch;
use app\modules\classwise\models\ErpMasterClass;
use app\modules\classwise\models\ErpMasterClassSection;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
/**
 * StudentController implements the CRUD actions for ErpStudentInformation model.
 */
class StudentController extends Controller
{
    /**
     * @inheritdoc
     */
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
	public function behaviors()
		{
			return [
				'verbs' => [
					'class' => VerbFilter::className(),
					'actions' => [
						//'delete' => ['post'],
					],
				],
				'access' => [
							'class' => \yii\filters\AccessControl::className(),
							'only' => ['index','create','update','view','delete'],
							'rules' => [
								// allow authenticated users
								[
									'allow' => true,
									'roles' => ['@'],
								],
								// everything else is denied
							],
						],            
			];
		}

    /**
     * Lists all ErpStudentInformation models.
     * @return mixed
     */
    public function actionIndex()
    {
		$student_school_uuid = Yii::$app->utility->getLoginschooluuid();
		
        $searchModel = new ErpStudentInformationSearch();
		
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		$dataProvider->query->where(['student_school_uuid' => $student_school_uuid]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ErpStudentInformation model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ErpStudentInformation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		$classdetail = ArrayHelper::map(ErpMasterClass::find()
				  ->where(['school_uuid' => Yii::$app->utility->getLoginschooluuid()])
				  ->all(),'class_uuid','class_title');
				  
	    $sectiondetail = ArrayHelper::map(ErpMasterClassSection::find()
				  ->where(['school_uuid' => Yii::$app->utility->getLoginschooluuid()])
				  ->all(),'section_uuid','section_title');
				  
        $model = new ErpStudentInformation();
		
		$moduleName = "ErpStudentInformation"; //Module Name        

		$tableName = "ErpStudentInformation"; //Table Class Nam

		$student_uuid = Yii::$app->utility->generateGuuid($moduleName, $tableName); //Generate UUID

        if ($model->load(Yii::$app->request->post())) { 
		
		$model->student_uuid = $student_uuid;
		
		$model->student_school_uuid = Yii::$app->utility->getLoginschooluuid();
		
		$model->student_dob = date("Y-m-d",strtotime($model->student_dob));
		
		$model->student_created_date = date("Y-m-d h:i:s");
		
		    if($model->save()) {
               return $this->redirect(['index']);
			} else {
			   print_r($model->getErrors());
			   die;	
			}
        }
        return $this->render('create', [
            'model' => $model,
			'classdetail' => $classdetail,
			'sectiondetail' => $sectiondetail
        ]);
    }

    /**
     * Updates an existing ErpStudentInformation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
		$classdetail = ArrayHelper::map(ErpMasterClass::find()
				  ->where(['school_uuid' => Yii::$app->utility->getLoginschooluuid()])
				  ->all(),'class_uuid','class_title');
				  
	    $sectiondetail = ArrayHelper::map(ErpMasterClassSection::find()
				  ->where(['school_uuid' => Yii::$app->utility->getLoginschooluuid()])
				  ->all(),'section_uuid','section_title');
				  
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->student_id]);
        }

        return $this->render('update', [
            'model' => $model,
			'classdetail' => $classdetail,
			'sectiondetail' => $sectiondetail
        ]);
    }

    /**
     * Deletes an existing ErpStudentInformation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = ErpStudentInformation::find()->where(['student_id' => $id])->one();
		
		$model->student_is_delete = 'YES';
        
		$model->save();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ErpStudentInformation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ErpStudentInformation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ErpStudentInformation::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
