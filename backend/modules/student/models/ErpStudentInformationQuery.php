<?php

namespace app\modules\student\models;

/**
 * This is the ActiveQuery class for [[ErpStudentInformation]].
 *
 * @see ErpStudentInformation
 */
class ErpStudentInformationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ErpStudentInformation[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ErpStudentInformation|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
