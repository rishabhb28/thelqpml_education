<?php

namespace app\modules\student\models;

use Yii;
use app\modules\schoolinfo\models\ErpSchoolInformation;
use app\modules\classwise\models\ErpMasterClass;
use app\modules\classwise\models\ErpMasterClassSection;
/**
 * This is the model class for table "erp_student_information".
 *
 * @property int $student_id
 * @property string $student_uuid
 * @property string $student_school_uuid
 * @property string $student_class_uuid
 * @property string $section_uuid
 * @property int $student_roll_number
 * @property string $student_session_uuid
 * @property string $student_first_name
 * @property string $student_last_name
 * @property string $student_dob
 * @property string $student_gender
 * @property string $student_nationality
 * @property string $student_email
 * @property string $student_father_name
 * @property string $father_contact_number
 * @property string $student_mother_name
 * @property string $mother_contact_number
 * @property string $student_phone
 * @property string $student_address1
 * @property string $student_address2
 * @property string $student_state
 * @property string $student_city
 * @property string $student_zip
 * @property string $student_is_delete
 * @property string $student_is_status
 * @property string $student_created_date
 *
 * @property ErpScholAttendance[] $erpScholAttendances
 * @property ErpScholFeedback[] $erpScholFeedbacks
 * @property ErpSchoolInformation $studentSchoolUu
 * @property ErpMasterClassSection $sectionUu
 * @property ErpMasterClass $studentClassUu
 * @property ErpStudentLeaveStatus[] $erpStudentLeaveStatuses
 */
class ErpStudentInformation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'erp_student_information';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student_uuid', 'student_school_uuid', 'student_class_uuid', 'section_uuid', 'student_roll_number', 'student_first_name', 'student_last_name', 'student_dob', 'student_father_name', 'father_contact_number', 'student_mother_name', 'mother_contact_number', 'student_address1', 'student_address2', 'student_state', 'student_city', 'student_zip', 'student_created_date'], 'required'],
            [['student_roll_number'], 'integer'],
            [['student_dob', 'student_created_date'], 'safe'],
            [['student_gender', 'student_nationality', 'student_is_delete', 'student_is_status'], 'string'],
            [['student_uuid', 'student_school_uuid', 'student_class_uuid', 'section_uuid', 'student_session_uuid'], 'string', 'max' => 128],
            [['student_first_name', 'student_last_name', 'student_email', 'student_father_name', 'student_mother_name'], 'string', 'max' => 50],
            [['father_contact_number', 'mother_contact_number', 'student_phone'], 'string', 'max' => 10],
            [['student_address1', 'student_address2'], 'string', 'max' => 255],
            [['student_state', 'student_city'], 'string', 'max' => 20],
            [['student_zip'], 'string', 'max' => 6],
            [['student_uuid'], 'unique'],
            [['student_school_uuid', 'student_class_uuid', 'section_uuid', 'student_roll_number'], 'unique', 'targetAttribute' => ['student_school_uuid', 'student_class_uuid', 'section_uuid', 'student_roll_number']],
            [['student_school_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpSchoolInformation::className(), 'targetAttribute' => ['student_school_uuid' => 'school_uuid']],
            [['section_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpMasterClassSection::className(), 'targetAttribute' => ['section_uuid' => 'section_uuid']],
            [['student_class_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpMasterClass::className(), 'targetAttribute' => ['student_class_uuid' => 'class_uuid']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'student_id' => 'Student ID',
            'student_uuid' => 'Student Uuid',
            'student_school_uuid' => 'Student School Uuid',
            'student_class_uuid' => 'Student Class Uuid',
            'section_uuid' => 'Section Uuid',
            'student_roll_number' => 'Student Roll Number',
            'student_session_uuid' => 'Student Session Uuid',
            'student_first_name' => 'Student First Name',
            'student_last_name' => 'Student Last Name',
            'student_dob' => 'Student Dob',
            'student_gender' => 'Student Gender',
            'student_nationality' => 'Student Nationality',
            'student_email' => 'Student Email',
            'student_father_name' => 'Student Father Name',
            'father_contact_number' => 'Father Contact Number',
            'student_mother_name' => 'Student Mother Name',
            'mother_contact_number' => 'Mother Contact Number',
            'student_phone' => 'Student Phone',
            'student_address1' => 'Student Address1',
            'student_address2' => 'Student Address2',
            'student_state' => 'Student State',
            'student_city' => 'Student City',
            'student_zip' => 'Student Zip',
            'student_is_delete' => 'Student Is Delete',
            'student_is_status' => 'Student Is Status',
            'student_created_date' => 'Student Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getErpScholAttendances()
    {
        return $this->hasMany(ErpScholAttendance::className(), ['school_attendance_student_uuid' => 'student_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getErpScholFeedbacks()
    {
        return $this->hasMany(ErpScholFeedback::className(), ['school_feedback_student_uuid' => 'student_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentSchoolUu()
    {
        return $this->hasOne(ErpSchoolInformation::className(), ['school_uuid' => 'student_school_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSectionUu()
    {
        return $this->hasOne(ErpMasterClassSection::className(), ['section_uuid' => 'section_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentClassUu()
    {
        return $this->hasOne(ErpMasterClass::className(), ['class_uuid' => 'student_class_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getErpStudentLeaveStatuses()
    {
        return $this->hasMany(ErpStudentLeaveStatus::className(), ['student_uuid' => 'student_uuid']);
    }

    /**
     * @inheritdoc
     * @return ErpStudentInformationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ErpStudentInformationQuery(get_called_class());
    }
}
