<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\examination\models\ErpMasterExaminationType */

$this->title = $model->examination_type_id;
$this->params['breadcrumbs'][] = ['label' => 'Erp Master Examination Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="erp-master-examination-type-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->examination_type_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->examination_type_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'examination_type_id',
            //'examination_type_uuid',
            //'school_uuid',
            'examination_type_name',
            'examination_start_date',
            'examination_end_date',
            'examination_type_status',
            'examination_is_deleted',
            'examination_created_date',
        ],
    ]) ?>

</div>
