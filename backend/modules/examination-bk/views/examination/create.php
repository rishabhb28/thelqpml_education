<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\examination\models\ErpMasterExaminationType */

$this->title = 'Create Erp Master Examination Type';
$this->params['breadcrumbs'][] = ['label' => 'Erp Master Examination Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="erp-master-examination-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
