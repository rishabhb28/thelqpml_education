<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\examination\models\ErpMasterExaminationType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="erp-master-examination-type-form">

    <?php $form = ActiveForm::begin(array('options'=>array("class"=>'smart-form'))); ?>
    
    <fieldset>
   <div class="row">
      <section class="col col-6">
         <label class="input">
         <?= $form->field($model, 'examination_type_name')->textInput(['maxlength' => true]) ?>
         </label>
      </section>
      <section class="col col-6">
         <label class="input">
          <?= $form->field($model, 'examination_type_status')->dropDownList([ 'ACTIVE' => 'ACTIVE', 'INACTIVE' => 'INACTIVE', ], ['prompt' => 'Choose One']) ?>
         </label>
      </section>
     <section class="col col-6">
         <label class="input">Examination Start Date
          <input type="date" class="form-control" name="ErpMasterExaminationType[examination_start_date]" value="<?=$model->examination_start_date?>" id="erpmasterexaminationtype-examination_start_date" aria-required="true" aria-invalid="true" />
          <div class="help-block">Examination Start Date cannot be blank.</div>
         </label>
      </section>
      <section class="col col-6">
         <label class="input">Examination Start Date
          <input type="date" class="form-control" name="ErpMasterExaminationType[examination_end_date]" value="<?=$model->examination_end_date?>" id="erpmasterexaminationtype-examination_end_date" aria-required="true" aria-invalid="true" />
          <div class="help-block">Examination Start Date cannot be blank.</div>
         </label>
      </section>
      
      
   </div>
</fieldset>
<footer>
   <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
</footer>

    <?php ActiveForm::end(); ?>

</div>
