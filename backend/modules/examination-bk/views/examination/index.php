<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Erp Master Examination Types';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="erp-master-examination-type-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Erp Master Examination Type', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'examination_type_id',
            //'examination_type_uuid',
            //'school_uuid',
            'examination_type_name',
            'examination_start_date',
            'examination_end_date',
            'examination_type_status',
            'examination_is_deleted',
            //'examination_created_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
