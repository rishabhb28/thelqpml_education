<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Examinations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="erp-master-examination-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Examination', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'examination_id',
            //'examination_uuid',
            //'school_uuid',
            'examTypeUu.examination_type_name',
            'examClassUu.classwise_class_name',
            'examSubjectUu.subject_name',
            'exam_date',
            'examination_start_time',
            'examination_end_time',
            'exam_status',
            'exam_is_deleted',
            //'exam_created_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
