<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\examination\models\ErpMasterExamination */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="erp-master-examination-form">

     <?php $form = ActiveForm::begin(array('options'=>array("class"=>'smart-form'))); ?>
    
        <fieldset>
   <div class="row">
      <section class="col col-4">
         <label class="input">
         <?= $form->field($model, 'exam_type_uuid')->dropDownList([$examtype], ['prompt' => 'Choose One']) ?>
         </label>
      </section>
      <section class="col col-4">
         <label class="input">
         <?= $form->field($model, 'exam_class_uuid')->dropDownList([$classdetail], ['prompt' => 'Choose One']) ?>
         </label>
      </section>
      <section class="col col-4">
         <label class="input">
         <?= $form->field($model, 'exam_subject_uuid')->dropDownList([$subjectdetail], ['prompt' => 'Choose One']) ?>
         </label>
      </section>
      <section class="col col-3">
        <label class="input">Exam Date
         <input type="date" id="erpmasterexamination-exam_date" class="form-control" name="ErpMasterExamination[exam_date]" value="<?=$model->exam_date?>" aria-required="true" aria-invalid="true">
         </label>
      </section>
      <section class="col col-3">
        <label class="input">Start Time
         <input type="time" id="erpmasterexamination-examination_start_time" class="form-control" name="ErpMasterExamination[examination_start_time]" value="<?=$model->examination_start_time?>" aria-required="true" aria-invalid="true">
         </label>
      </section>
      <section class="col col-3">
        <label class="input">End Time
         <input type="time" id="erpmasterexamination-examination_end_time" class="form-control" name="ErpMasterExamination[examination_end_time]" value="<?=$model->examination_end_time?>" aria-required="true" aria-invalid="true">
         </label>
      </section>
      <section class="col col-3">
         <label class="input">
         <?= $form->field($model, 'exam_status')->dropDownList([ 'ACTIVE' => 'ACTIVE', 'INACTIVE' => 'INACTIVE', ], ['prompt' => 'Choose One']) ?>
         </label>
      </section>
      
   </div>
</fieldset>
<footer>
   <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
</footer>

    <?php ActiveForm::end(); ?>

</div>
