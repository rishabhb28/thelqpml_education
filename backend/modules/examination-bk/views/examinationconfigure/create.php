<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\examination\models\ErpMasterExamination */

$this->title = 'Create Erp Master Examination';
$this->params['breadcrumbs'][] = ['label' => 'Erp Master Examinations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="erp-master-examination-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'classdetail' => $classdetail,
		'subjectdetail' => $subjectdetail,
		'examtype' => $examtype
    ]) ?>

</div>
