<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\examination\models\ErpMasterExamination */

$this->title = 'Update Erp Master Examination: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Erp Master Examinations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->examination_id, 'url' => ['view', 'id' => $model->examination_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="erp-master-examination-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'classdetail' => $classdetail,
		'subjectdetail' => $subjectdetail,
		'examtype' => $examtype
    ]) ?>

</div>
