<?php

namespace app\modules\schoolmanagement\models;

use Yii;
use app\modules\schoolinfo\models\ErpSchoolInformation;
/**
 * This is the model class for table "erp_master_academic_calendar".
 *
 * @property int $academic_id
 * @property string $academic_uuid
 * @property string $academic_school_uuid
 * @property string $academic_title
 * @property string $academic_description
 * @property string $academic_date
 * @property string $academic_is_status
 * @property string $academic_is_deleted
 * @property string $academic_created_date
 *
 * @property ErpSchoolInformation $academicSchoolUu
 */
class ErpMasterAcademicCalendar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'erp_master_academic_calendar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['academic_uuid', 'academic_school_uuid', 'academic_title', 'academic_description', 'academic_date','academic_created_date'], 'required'],
            [['academic_description', 'academic_is_status', 'academic_is_deleted'], 'string'],
            [['academic_date', 'academic_created_date'], 'safe'],
            [['academic_uuid', 'academic_school_uuid'], 'string', 'max' => 128],
            [['academic_title'], 'string', 'max' => 255],
            [['academic_uuid'], 'unique'],
            [['academic_school_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpSchoolInformation::className(), 'targetAttribute' => ['academic_school_uuid' => 'school_uuid']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'academic_id' => 'Academic ID',
            'academic_uuid' => 'Academic Uuid',
            'academic_school_uuid' => 'Academic School Uuid',
            'academic_title' => 'Academic Title',
            'academic_description' => 'Academic Description',
            'academic_date' => 'Academic Date',
            'academic_is_status' => 'Academic Is Status',
            'academic_is_deleted' => 'Academic Is Deleted',
            'academic_created_date' => 'Academic Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicSchoolUu()
    {
        return $this->hasOne(ErpSchoolInformation::className(), ['school_uuid' => 'academic_school_uuid']);
    }

    /**
     * @inheritdoc
     * @return ErpMasterAcademicCalendarQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ErpMasterAcademicCalendarQuery(get_called_class());
    }
}
