<?php

namespace app\modules\schoolmanagement\models;

/**
 * This is the ActiveQuery class for [[ErpMasterAcademicCalendar]].
 *
 * @see ErpMasterAcademicCalendar
 */
class ErpMasterAcademicCalendarQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ErpMasterAcademicCalendar[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ErpMasterAcademicCalendar|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
