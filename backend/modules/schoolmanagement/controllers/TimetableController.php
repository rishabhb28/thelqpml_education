<?php

namespace app\modules\schoolmanagement\controllers;

use Yii;
use app\modules\schoolmanagement\models\ErpTimeTable;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\modules\schoolinfo\models\ErpSchoolInformation;
use app\modules\classwise\models\ErpMasterClass;
use app\modules\classwise\models\ErpMasterClassSection;
use app\modules\subject\models\ErpMasterSubject;
/**
 * TimetableController implements the CRUD actions for ErpTimeTable model.
 */
class TimetableController extends Controller
{
    /**
     * @inheritdoc
     */
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
	public function behaviors()
		{
			return [
				'verbs' => [
					'class' => VerbFilter::className(),
					'actions' => [
						//'delete' => ['post'],
					],
				],
				'access' => [
							'class' => \yii\filters\AccessControl::className(),
							'only' => ['index','create','update','view','delete'],
							'rules' => [
								// allow authenticated users
								[
									'allow' => true,
									'roles' => ['@'],
								],
								// everything else is denied
							],
						],            
			];
		}

    /**
     * Lists all ErpTimeTable models.
     * @return mixed
     */
    public function actionIndex()
    {
		$timetable_school_uuid = Yii::$app->utility->getLoginschooluuid();
		
        $dataProvider = new ActiveDataProvider([
            'query' => ErpTimeTable::find()->with('timetableSchoolClassUu')->with('timetableSubjectUu')
        ]);
		
		$dataProvider->query->where(['timetable_school_uuid' => $timetable_school_uuid]);


        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ErpTimeTable model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
       $model = ErpTimeTable::find()->with('timetableSubjectUu')->with('timetableSchoolClassUu')->with('sectionUu')->where(['timetable_id' => $id])->one();
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new ErpTimeTable model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		$action = 'create';
		
		$classdetail = ArrayHelper::map(ErpMasterClass::find()
				  ->where(['school_uuid' => Yii::$app->utility->getLoginschooluuid()])
				  ->all(),'class_uuid','class_title');
		  
		$subjectdetail = ArrayHelper::map(ErpMasterSubject::find()
		  ->where(['subject_school_uuid' => Yii::$app->utility->getLoginschooluuid()])
		  ->all(),'subject_uuid','subject_name');

	    $sectiondetail = ArrayHelper::map(ErpMasterClassSection::find()
				  ->where(['school_uuid' => Yii::$app->utility->getLoginschooluuid()])
				  ->all(),'section_uuid','section_title');
		  
        $model = new ErpTimeTable();

		if ($model->load(Yii::$app->request->post())) { 
		
		$i = 0;
		
	   $len =  count($_POST['ErpTimeTable']['timetable_subject_uuid']);
	   

		foreach($_POST['ErpTimeTable']['timetable_subject_uuid'] as $key=>$value) { 
		
		$data = new ErpTimeTable();
		
		$moduleName = "ErpTimeTable"; //Module Name        

		$tableName = "ErpTimeTable"; //Table Class Nam
		
		$timetable_uuid = Yii::$app->utility->generateGuuid($moduleName, $tableName); //Generate UUID
		  
		  $data->timetable_uuid = $timetable_uuid;
		  
		  $data->timetable_school_uuid = Yii::$app->utility->getLoginschooluuid();
		  
		  $data->timetable_school_class_uuid = $_POST['ErpTimeTable']['timetable_school_class_uuid'];
		  
		  $data->section_uuid = $_POST['ErpTimeTable']['section_uuid'];
		  
		  $data->timetable_subject_uuid = $value;

          $data->timetable_title= $_POST['ErpTimeTable']['timetable_title'][$key];
		  
		  $data->timetable_start_time = $_POST['ErpTimeTable']['timetable_start_time'][$key];
		  
		  $data->timetable_end_date = $_POST['ErpTimeTable']['timetable_end_date'][$key];
		  
		  $data->timetable_created_date = date("Y-m-d h:i:s");
		  
		  $data->save();
		  
		   if ($i == $len - 1) {
			   
			   return $this->redirect(['view', 'id' => $data->timetable_id]);
				// last
		   }
		   
		   $i++;
		  
		}
		  
		}

        return $this->render('create', [
            'model' => $model,
			'classdetail' => $classdetail,
			'subjectdetail' => $subjectdetail,
            'sectiondetail' => $sectiondetail,
			'action' => $action
        ]);
    }

    /**
     * Updates an existing ErpTimeTable model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
		
		$action = 'update';
		
        $model = $this->findModel($id);

		$classdetail = ArrayHelper::map(ErpMasterClass::find()
				  ->where(['school_uuid' => Yii::$app->utility->getLoginschooluuid()])
				  ->all(),'class_uuid','class_title');
		  
		$subjectdetail = ArrayHelper::map(ErpMasterSubject::find()
		  ->where(['subject_school_uuid' => Yii::$app->utility->getLoginschooluuid()])
		  ->all(),'subject_uuid','subject_name');

	    $sectiondetail = ArrayHelper::map(ErpMasterClassSection::find()
				  ->where(['school_uuid' => Yii::$app->utility->getLoginschooluuid()])
				  ->all(),'section_uuid','section_title');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->timetable_id]);
        }

        return $this->render('update', [
            'model' => $model,
			'classdetail' => $classdetail,
			'subjectdetail' => $subjectdetail,
            'sectiondetail' => $sectiondetail,
			'action' => $action
        ]);
    }

    /**
     * Deletes an existing ErpTimeTable model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ErpTimeTable model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ErpTimeTable the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ErpTimeTable::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
