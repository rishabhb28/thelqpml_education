<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\examination\models\ErpTimeTable */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="erp-master-examination-form">

     <?php $form = ActiveForm::begin(array('options'=>array("class"=>'smart-form'))); ?>
     
    
    <fieldset>
   <div class="row">
   <div id="clonedInput1" class="clonedInput">
      <section class="col col-2">
         <label class="input">
         <?= $form->field($model, 'timetable_school_class_uuid')->dropDownList([$classdetail], ['prompt' => 'Choose One']) ?>
         </label>
      </section>
      <section class="col col-2">
         <label class="input">
         <?= $form->field($model, 'section_uuid')->dropDownList([$sectiondetail], ['prompt' => 'Choose One']) ?>
         </label>
      </section>
      <?php if($action == 'update') { ?>
      <section class="col col-2">
         <label class="input">
         <?= $form->field($model, 'timetable_subject_uuid')->dropDownList([$subjectdetail], ['prompt' => 'Choose One']) ?>
         </label>
      </section>
      <section class="col col-2">
         <label class="input">
         <?= $form->field($model, 'timetable_title')->dropDownList(['Monday' => 'Monday','Tuesday' => 'Tuesday', 'Wednesday' => 'Wednesday', 'Thursday' => 'Thursday', 'Friday' => 'Friday', 'Saturday' => 'Saturday'], ['prompt' => 'Choose One']); ?>
         </label>
      </section>
      <section class="col col-2">
         <label class="input">
         <?= $form->field($model, 'timetable_start_time')->textInput(['type' => 'time']) ?>
         </label>
      </section>
      <section class="col col-2">
         <label class="input">
         <?= $form->field($model, 'timetable_end_date')->textInput(['type' => 'time']) ?>
         </label>
      </section>
      <?php } ?>
<?php if($action == 'create') { ?>
            <section class="col col-2">
         <label class="input">
         <?= $form->field($model, 'timetable_subject_uuid[]')->dropDownList([$subjectdetail], ['prompt' => 'Choose One']) ?>
         </label>
      </section>
      <section class="col col-2">
         <label class="input">
         <?= $form->field($model, 'timetable_title[]')->dropDownList(['Monday' => 'Monday','Tuesday' => 'Tuesday', 'Wednesday' => 'Wednesday', 'Thursday' => 'Thursday', 'Friday' => 'Friday', 'Saturday' => 'Saturday'], ['prompt' => 'Choose One']); ?>
         </label>
      </section>
      <section class="col col-2">
         <label class="input">
         <?= $form->field($model, 'timetable_start_time[]')->textInput(['type' => 'time']) ?>
         </label>
      </section>
      <section class="col col-2">
         <label class="input">
         <?= $form->field($model, 'timetable_end_date[]')->textInput(['type' => 'time']) ?>
         </label>
      </section>
       <?php } ?>
       <?php if($action == 'create') { ?>
            <div class="actions">
               <a class="clone btn btn-primary">Clone</a> 
               <a class="remove btn btn-danger">Remove</a>
            </div>
             <?php } ?>
         </div>
         <!----clone start ------>
         <div id="newHtml" class="newHtml1">
         </div>
         <!----clone end --------->
         </div>
</fieldset>
</div>

<footer>

   <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
   

</footer>

    <?php ActiveForm::end(); ?>
