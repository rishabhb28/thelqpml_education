<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolmanagement\models\ErpTimeTable */

$this->title = 'Update Erp Time Table: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Erp Time Tables', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->timetable_id, 'url' => ['view', 'id' => $model->timetable_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="erp-time-table-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'classdetail' => $classdetail,
		'subjectdetail' => $subjectdetail,
        'sectiondetail' => $sectiondetail,
		'action' => $action
    ]) ?>

</div>
