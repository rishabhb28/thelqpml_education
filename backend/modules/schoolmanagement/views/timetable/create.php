<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\schoolmanagement\models\ErpTimeTable */

$this->title = 'Create Erp Time Table';
$this->params['breadcrumbs'][] = ['label' => 'Erp Time Tables', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="erp-time-table-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'classdetail' => $classdetail,
		'subjectdetail' => $subjectdetail,
        'sectiondetail' => $sectiondetail,
		'action' => $action
    ]) ?>

</div>
  <script type="text/javascript">
var regex = /^(.+?)(\d+)$/i;
var cloneIndex = $(".clonedInput").length;

function clone(){

    $(this).parents(".clonedInput").clone()
        .appendTo(".newHtml1")
        .attr("id", "clonedInput" +  cloneIndex)
        .find("*")
        .each(function() {
            var id = this.id || "";
            var match = id.match(regex) || [];
            if (match.length == 3) {
                this.id = match[1] + (cloneIndex);
            }
        })
        .on('click', '.clone', clone)
        .on('click', '.remove', remove);
    cloneIndex++;
}
function remove(){
    $(this).parents(".clonedInput").remove();
}
$(".clone").on("click", clone);

$(".remove").on("click", remove);
    </script>