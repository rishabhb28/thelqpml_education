<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolmanagement\models\ErpMasterAcademicCalendar */

$this->title = $model->academic_id;
$this->params['breadcrumbs'][] = ['label' => 'Erp Master Academic Calendars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="erp-master-academic-calendar-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->academic_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->academic_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'academic_id',
           //'academic_uuid',
            //'academic_school_uuid',
            'academic_title',
			'academic_date',
            'academic_description:ntext',
            'academic_is_status',
            'academic_is_deleted',
            'academic_created_date',
        ],
    ]) ?>

</div>
