<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolmanagement\models\ErpSchoolNotification */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="erp-school-notification-form">

    <?php $form = ActiveForm::begin(array('options'=>array("class"=>'smart-form'))); ?>
    
    <fieldset>
   <div class="row">
      <section class="col col-4">
         <label class="input">
         <?= $form->field($model, 'notification_title')->textInput(['maxlength' => true]) ?>
         </label>
      </section>
      <section class="col col-4">
         <label class="input">
           <?= $form->field($model, 'notification_is_status')->dropDownList([ 'ACTIVE' => 'ACTIVE', 'INACTIVE' => 'INACTIVE', ], ['prompt' => 'Choose One']) ?>
         </label>
      </section>
      <section class="col col-4">
         <label class="input">Notification Date
           <input type="date" id="erpschoolnotification-notification_date" class="form-control" name="ErpSchoolNotification[notification_date]" value="<?=$model->notification_date?>" placeholder="Notification Date" aria-required="true" aria-invalid="true">
         </label>
      </section>
      <section class="col col-md-12">
         <label class="input">
           <?= $form->field($model, 'notification_description')->textarea(['rows' => 6]) ?>
         </label>
      </section>
      
      
   </div>
</fieldset>
<footer>
   <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
</footer>

    <?php ActiveForm::end(); ?>

</div><!-- teacher-_form -->


