<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolmanagement\models\ErpSchoolNotification */

$this->title = $model->notification_id;
$this->params['breadcrumbs'][] = ['label' => 'Erp School Notifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="erp-school-notification-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->notification_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->notification_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'notification_id',
            //'notification_uuid',
            //'notification_school_uuid',
            'notification_title',
			'notification_date',
            'notification_description:ntext',
            'notification_is_status',
            'notification_is_deleted',
            'notification_created_date',
        ],
    ]) ?>

</div>
