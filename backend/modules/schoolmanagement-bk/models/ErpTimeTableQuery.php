<?php

namespace app\modules\schoolmanagement\models;

/**
 * This is the ActiveQuery class for [[ErpTimeTable]].
 *
 * @see ErpTimeTable
 */
class ErpTimeTableQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ErpTimeTable[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ErpTimeTable|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
