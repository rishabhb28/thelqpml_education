
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolmanagement\models\ErpMasterAcademicCalendar */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="erp-master-academic-calendar-form">

    <?php $form = ActiveForm::begin(array('options'=>array("class"=>'smart-form'))); ?>
    
    <fieldset>
   <div class="row">
      <section class="col col-4">
         <label class="input">
         <?= $form->field($model, 'academic_title')->textInput(['maxlength' => true]) ?>
         </label>
      </section>
      <section class="col col-4">
         <label class="input">
           <?= $form->field($model, 'academic_is_status')->dropDownList([ 'ACTIVE' => 'ACTIVE', 'INACTIVE' => 'INACTIVE', ], ['prompt' => 'Choose One']) ?>
         </label>
      </section>
      <section class="col col-4">
         <label class="input">Academic Date
          <input type="date" id="erpmasteracademiccalendar-academic_date" class="form-control" name="ErpMasterAcademicCalendar[academic_date]" value="<?=$model->academic_date?>" placeholder="Academic Date" aria-required="true" aria-invalid="true">
         </label>
      </section>
      <section class="col col-md-12">
         <label class="input">
           <?= $form->field($model, 'academic_description')->textarea(['rows' => 6]) ?>
         </label>
      </section>
      
      
   </div>
</fieldset>
<footer>
   <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
</footer>

    <?php ActiveForm::end(); ?>

</div><!-- teacher-_form -->
