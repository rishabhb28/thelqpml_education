<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Academic Calendars';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="erp-master-academic-calendar-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Academic Calendar', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'academic_id',
            //'academic_uuid',
            //'academic_school_uuid',
            'academic_title',
			'academic_date',
            'academic_description:ntext',
            'academic_is_status',
            'academic_is_deleted',
            'academic_created_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
