<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'School Notifications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="erp-school-notification-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create School Notification', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'notification_id',
            //'notification_uuid',
            //'notification_school_uuid',
            'notification_title',
			'notification_date',
            'notification_description:ntext',
            'notification_is_status',
            'notification_is_deleted',
            'notification_created_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
