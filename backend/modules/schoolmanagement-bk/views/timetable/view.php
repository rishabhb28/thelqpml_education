<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolmanagement\models\ErpTimeTable */

$this->title = $model->timetable_id;
$this->params['breadcrumbs'][] = ['label' => 'Erp Time Tables', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="erp-time-table-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->timetable_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->timetable_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'timetable_id:datetime',
            //'timetable_uuid',
            //'timetable_school_uuid',
            'timetableSchoolClassUu.classwise_class_name',
            'sectionUu.section_title',
            'timetableSubjectUu.subject_name',
            //'timetable_title',
            'timetable_start_time',
            'timetable_end_date',
            'timetable_is_status',
            'timetable_is_deleted',
            'timetable_created_date',
        ],
    ]) ?>

</div>
