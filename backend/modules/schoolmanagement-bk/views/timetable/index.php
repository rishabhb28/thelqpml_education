<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Erp Time Tables';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="erp-time-table-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Erp Time Table', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'timetable_id',
            //'timetable_uuid',
            //'timetable_school_uuid',
            'timetableSchoolClassUu.classwise_class_name',
            'timetableSubjectUu.subject_name',
            //'timetable_title',
            'timetable_start_time',
            'timetable_end_date',
            //'timetable_is_status',
            //'timetable_is_deleted',
            //'timetable_created_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
