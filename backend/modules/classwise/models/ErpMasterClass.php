<?php

namespace app\modules\classwise\models;

use Yii;
use app\modules\schoolinfo\models\ErpSchoolInformation;
/**
 * This is the model class for table "erp_master_class".
 *
 * @property int $class_id
 * @property string $class_uuid
 * @property string $school_uuid
 * @property string $class_title
 * @property string $class_created_date
 * @property string $class_status
 * @property string $class_is_deleted
 *
 * @property ErpSchoolInformation $schoolUu
 * @property ErpMasterExamination[] $erpMasterExaminations
 * @property ErpSchoolClasswise[] $erpSchoolClasswises
 * @property ErpStudentInformation[] $erpStudentInformations
 * @property ErpTimeTable[] $erpTimeTables
 */
class ErpMasterClass extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'erp_master_class';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['class_uuid', 'school_uuid', 'class_title', 'class_created_date'], 'required'],
            [['class_created_date'], 'safe'],
            [['class_status', 'class_is_deleted'], 'string'],
            [['class_uuid', 'school_uuid'], 'string', 'max' => 128],
            [['class_title'], 'string', 'max' => 10],
            [['class_uuid'], 'unique'],
            [['class_title', 'school_uuid'], 'unique', 'targetAttribute' => ['class_title', 'school_uuid']],
            [['school_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpSchoolInformation::className(), 'targetAttribute' => ['school_uuid' => 'school_uuid']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'class_id' => 'Class ID',
            'class_uuid' => 'Class Uuid',
            'school_uuid' => 'School Uuid',
            'class_title' => 'Class Title',
            'class_created_date' => 'Class Created Date',
            'class_status' => 'Class Status',
            'class_is_deleted' => 'Class Is Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolUu()
    {
        return $this->hasOne(ErpSchoolInformation::className(), ['school_uuid' => 'school_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getErpMasterExaminations()
    {
        return $this->hasMany(ErpMasterExamination::className(), ['exam_class_uuid' => 'class_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getErpSchoolClasswises()
    {
        return $this->hasMany(ErpSchoolClasswise::className(), ['classwise_class_name' => 'class_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getErpStudentInformations()
    {
        return $this->hasMany(ErpStudentInformation::className(), ['student_class_uuid' => 'class_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getErpTimeTables()
    {
        return $this->hasMany(ErpTimeTable::className(), ['timetable_school_class_uuid' => 'class_uuid']);
    }

    /**
     * @inheritdoc
     * @return ErpMasterClassQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ErpMasterClassQuery(get_called_class());
    }
}
