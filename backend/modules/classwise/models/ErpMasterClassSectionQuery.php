<?php

namespace app\modules\classwise\models;

/**
 * This is the ActiveQuery class for [[ErpMasterClassSection]].
 *
 * @see ErpMasterClassSection
 */
class ErpMasterClassSectionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ErpMasterClassSection[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ErpMasterClassSection|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
