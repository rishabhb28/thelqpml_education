<?php

namespace app\modules\classwise\models;

use Yii;
use app\modules\schoolinfo\models\ErpSchoolInformation;
use app\modules\subject\models\ErpMasterSubject;
use app\modules\teacher\models\ErpMasterTeacher;

/**
 * This is the model class for table "teacher_subject_mapping".
 *
 * @property int $teacher_sub_id
 * @property string $teacher_sub_uuid
 * @property string $school_uuid
 * @property string $teacher_uuid
 * @property string $subject_uuid
 * @property string $class_name_uuid
 * @property string $class_uuid
 * @property string $section_uuid
 * @property string $teacher_sub_is_status
 * @property string $teacher_sub_is_deleted
 * @property string $teacher_sub_created_date
 *
 * @property ErpMasterSubject $subjectUu
 * @property ErpMasterTeacher $teacherUu
 * @property ErpSchoolInformation $schoolUu
 * @property ErpSchoolClasswise $classUu
 * @property ErpMasterClass $classNameUu
 * @property ErpMasterClassSection $sectionUu
 */
class TeacherSubjectMapping extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'teacher_subject_mapping';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teacher_sub_uuid', 'school_uuid', 'teacher_uuid', 'subject_uuid', 'class_name_uuid', 'class_uuid', 'section_uuid', 'teacher_sub_created_date'], 'required'],
            [['teacher_sub_is_status', 'teacher_sub_is_deleted'], 'string'],
            [['teacher_sub_created_date'], 'safe'],
            [['teacher_sub_uuid', 'school_uuid', 'teacher_uuid', 'subject_uuid', 'class_name_uuid', 'class_uuid', 'section_uuid'], 'string', 'max' => 128],
            [['teacher_sub_uuid'], 'unique'],
            [['subject_uuid', 'class_name_uuid', 'section_uuid'], 'unique', 'targetAttribute' => ['subject_uuid', 'class_name_uuid', 'section_uuid']],
            [['subject_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpMasterSubject::className(), 'targetAttribute' => ['subject_uuid' => 'subject_uuid']],
            [['teacher_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpMasterTeacher::className(), 'targetAttribute' => ['teacher_uuid' => 'teacher_uuid']],
            [['school_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpSchoolInformation::className(), 'targetAttribute' => ['school_uuid' => 'school_uuid']],
            [['class_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpSchoolClasswise::className(), 'targetAttribute' => ['class_uuid' => 'classwise_uuid']],
            [['class_name_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpMasterClass::className(), 'targetAttribute' => ['class_name_uuid' => 'class_uuid']],
            [['section_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpMasterClassSection::className(), 'targetAttribute' => ['section_uuid' => 'section_uuid']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'teacher_sub_id' => 'Teacher Sub ID',
            'teacher_sub_uuid' => 'Teacher Sub Uuid',
            'school_uuid' => 'School Uuid',
            'teacher_uuid' => 'Teacher Uuid',
            'subject_uuid' => 'Subject Uuid',
            'class_name_uuid' => 'Class Name Uuid',
            'class_uuid' => 'Class Uuid',
            'section_uuid' => 'Section Uuid',
            'teacher_sub_is_status' => 'Teacher Sub Is Status',
            'teacher_sub_is_deleted' => 'Teacher Sub Is Deleted',
            'teacher_sub_created_date' => 'Teacher Sub Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjectUu()
    {
        return $this->hasOne(ErpMasterSubject::className(), ['subject_uuid' => 'subject_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacherUu()
    {
        return $this->hasOne(ErpMasterTeacher::className(), ['teacher_uuid' => 'teacher_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolUu()
    {
        return $this->hasOne(ErpSchoolInformation::className(), ['school_uuid' => 'school_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassUu()
    {
        return $this->hasOne(ErpSchoolClasswise::className(), ['classwise_uuid' => 'class_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassNameUu()
    {
        return $this->hasOne(ErpMasterClass::className(), ['class_uuid' => 'class_name_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSectionUu()
    {
        return $this->hasOne(ErpMasterClassSection::className(), ['section_uuid' => 'section_uuid']);
    }

    /**
     * @inheritdoc
     * @return TeacherSubjectMappingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TeacherSubjectMappingQuery(get_called_class());
    }
}
