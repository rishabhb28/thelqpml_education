<?php

namespace app\modules\classwise\models;

use Yii;
use app\modules\schoolinfo\models\ErpSchoolInformation;
/**
 * This is the model class for table "erp_school_classwise".
 *
 * @property int $classwise_id
 * @property string $classwise_uuid
 * @property string $classwise_school_uuid
 * @property string $section_uuid
 * @property string $classwise_class_name
 * @property string $classwise_is_regular
 * @property string $classwise_is_status
 * @property string $classwise_is_delete
 * @property string $classwise_created_date
 *
 * @property ErpScholAttendance[] $erpScholAttendances
 * @property ErpScholFeedback[] $erpScholFeedbacks
 * @property ErpSchoolInformation $classwiseSchoolUu
 * @property ErpMasterClassSection $sectionUu
 * @property ErpMasterClass $classwiseClassName
 * @property ErpStudentHomeWork[] $erpStudentHomeWorks
 * @property ErpStudentLeaveStatus[] $erpStudentLeaveStatuses
 * @property TeacherSubjectMapping[] $teacherSubjectMappings
 * @property ErpMasterSubject[] $subjectUus
 */
class ErpSchoolClasswise extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'erp_school_classwise';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['classwise_uuid', 'classwise_school_uuid', 'section_uuid', 'classwise_class_name', 'classwise_created_date'], 'required'],
            [['classwise_is_regular', 'classwise_is_status', 'classwise_is_delete'], 'string'],
            [['classwise_created_date'], 'safe'],
            [['classwise_uuid', 'classwise_school_uuid', 'section_uuid'], 'string', 'max' => 128],
            [['classwise_class_name'], 'string', 'max' => 100],
            [['classwise_uuid'], 'unique'],
            [['classwise_school_uuid', 'classwise_class_name', 'section_uuid'], 'unique', 'targetAttribute' => ['classwise_school_uuid', 'classwise_class_name', 'section_uuid']],
            [['classwise_school_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpSchoolInformation::className(), 'targetAttribute' => ['classwise_school_uuid' => 'school_uuid']],
            [['section_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpMasterClassSection::className(), 'targetAttribute' => ['section_uuid' => 'section_uuid']],
            [['classwise_class_name'], 'exist', 'skipOnError' => true, 'targetClass' => ErpMasterClass::className(), 'targetAttribute' => ['classwise_class_name' => 'class_uuid']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'classwise_id' => 'Classwise ID',
            'classwise_uuid' => 'Classwise Uuid',
            'classwise_school_uuid' => 'Classwise School Uuid',
            'section_uuid' => 'Section Uuid',
            'classwise_class_name' => 'Classwise Class Name',
            'classwise_is_regular' => 'Classwise Is Regular',
            'classwise_is_status' => 'Classwise Is Status',
            'classwise_is_delete' => 'Classwise Is Delete',
            'classwise_created_date' => 'Classwise Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getErpScholAttendances()
    {
        return $this->hasMany(ErpScholAttendance::className(), ['school_attendance_class_uuid' => 'classwise_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getErpScholFeedbacks()
    {
        return $this->hasMany(ErpScholFeedback::className(), ['school_feedback_class_uuid' => 'classwise_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClasswiseSchoolUu()
    {
        return $this->hasOne(ErpSchoolInformation::className(), ['school_uuid' => 'classwise_school_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSectionUu()
    {
        return $this->hasOne(ErpMasterClassSection::className(), ['section_uuid' => 'section_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClasswiseClassName()
    {
        return $this->hasOne(ErpMasterClass::className(), ['class_uuid' => 'classwise_class_name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getErpStudentHomeWorks()
    {
        return $this->hasMany(ErpStudentHomeWork::className(), ['class_uuid' => 'classwise_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getErpStudentLeaveStatuses()
    {
        return $this->hasMany(ErpStudentLeaveStatus::className(), ['student_class_uuid' => 'classwise_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacherSubjectMappings()
    {
        return $this->hasMany(TeacherSubjectMapping::className(), ['class_uuid' => 'classwise_uuid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjectUus()
    {
        return $this->hasMany(ErpMasterSubject::className(), ['subject_uuid' => 'subject_uuid'])->viaTable('teacher_subject_mapping', ['class_uuid' => 'classwise_uuid']);
    }

    /**
     * @inheritdoc
     * @return ErpSchoolClasswiseQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ErpSchoolClasswiseQuery(get_called_class());
    }
}
