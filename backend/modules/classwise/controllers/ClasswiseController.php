<?php



namespace app\modules\classwise\controllers;



use Yii;

use app\modules\classwise\models\ErpSchoolClasswise;

use app\modules\classwise\models\ErpMasterClassSection;

use app\modules\classwise\models\TeacherSubjectMapping;

use app\modules\teacher\models\ErpMasterTeacher;

use app\modules\subject\models\ErpMasterSubject;

use app\modules\classwise\models\ErpMasterClass;

use yii\data\ActiveDataProvider;

use yii\web\Controller;

use yii\web\NotFoundHttpException;

use yii\filters\VerbFilter;

use yii\helpers\ArrayHelper;





/**

 * ClasswiseController implements the CRUD actions for ErpSchoolClasswise model.

 */

class ClasswiseController extends Controller

{

    /**

     * @inheritdoc

     */

    /**

     * Specifies the access control rules.

     * This method is used by the 'accessControl' filter.

     * @return array access control rules

     */

	public function behaviors()

		{

			return [

				'verbs' => [

					'class' => VerbFilter::className(),

					'actions' => [

						//'delete' => ['post'],

					],

				],

				'access' => [

							'class' => \yii\filters\AccessControl::className(),

							'only' => ['index','create','update','view','delete'],

							'rules' => [

								// allow authenticated users

								[

									'allow' => true,

									'roles' => ['@'],

								],

								// everything else is denied

							],

						],            

			];

		}



    /**

     * Lists all ErpSchoolClasswise models.

     * @return mixed

     */


    public function actionIndex()
    {
		$classwise_school_uuid = Yii::$app->utility->getLoginschooluuid();
		
        $dataProvider = new ActiveDataProvider([
            'query' => ErpSchoolClasswise::find()->with('sectionUu')->with('classwiseClassName'),
        ]);
		
		$dataProvider->query->where(['classwise_school_uuid' => $classwise_school_uuid]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    


    /**

     * Displays a single ErpSchoolClasswise model.

     * @param integer $id

     * @return mixed

     * @throws NotFoundHttpException if the model cannot be found

     */

    public function actionView($id)

    {
		
		
		$model = ErpSchoolClasswise::find()->with('sectionUu')->with('classwiseClassName')->where(['classwise_id' => $id])->one();
        
        return $this->render('view', [

            'model' => $model,

        ]);

    }



    /**

     * Creates a new ErpSchoolClasswise model.

     * If creation is successful, the browser will be redirected to the 'view' page.

     * @return mixed

     */

    public function actionCreate()

    {

		$classdetail = ArrayHelper::map(ErpMasterClass::find()

				  ->where(['school_uuid' => Yii::$app->utility->getLoginschooluuid()])

				  ->all(),'class_uuid','class_title');

		$sectiondetail = ArrayHelper::map(ErpMasterClassSection::find()

				  ->where(['school_uuid' => Yii::$app->utility->getLoginschooluuid()])

				  ->all(),'section_uuid','section_title');

				  

	    $teacherdetail = ArrayHelper::map(ErpMasterTeacher::find()

				  ->where(['teacher_school_uuid' => Yii::$app->utility->getLoginschooluuid()])

				  ->all(),'teacher_uuid','teacher_first_name');

	    

	    $subjectdetail = ArrayHelper::map(ErpMasterSubject::find()

				  ->where(['subject_school_uuid' => Yii::$app->utility->getLoginschooluuid()])

				  ->all(),'subject_uuid','subject_name');

				  

        $model = new ErpSchoolClasswise();

		

		$teacher = new TeacherSubjectMapping();

		

        $moduleName = "ErpSchoolClasswise"; //Module Name        



		$tableName = "ErpSchoolClasswise"; //Table Class Nam



		$classwise_uuid = Yii::$app->utility->generateGuuid($moduleName, $tableName); //Generate UUID



        if ($model->load(Yii::$app->request->post())) { 

		

		$model->classwise_uuid = $classwise_uuid;

		

		$model->classwise_school_uuid = Yii::$app->utility->getLoginschooluuid();

		

		$model->classwise_created_date = date("Y-m-d h:i:s");

		

		if($model->save()) {

				

			   foreach($_POST['TeacherSubjectMapping']['teacher_uuid'] as $key=>$mapping) {

				   

				  $teacher = new TeacherSubjectMapping();

				  

				  $moduleName = "TeacherSubjectMapping"; //Module Name        



				  $tableName = "TeacherSubjectMapping"; //Table Class Nam

		  

				  $teacher_sub_uuid = Yii::$app->utility->generateGuuid($moduleName, $tableName); //Generate UUID

				  

				  $teacher->teacher_sub_uuid =  $teacher_sub_uuid;

				  

				  $teacher->school_uuid =  Yii::$app->utility->getLoginschooluuid();

				  

				  $teacher->teacher_uuid =  $mapping;

				  

				  $teacher->subject_uuid =  $_POST['TeacherSubjectMapping']['subject_uuid'][$key];

				  $teacher->class_name_uuid =  $model->classwise_class_name;

				  $teacher->class_uuid =  $classwise_uuid;

                                  $teacher->section_uuid =  $model->section_uuid;
 

				  $teacher->teacher_sub_created_date =  date("Y-m-d h:i:s");

				  

				  $teacher->save();


				   

			   }

               return $this->redirect(['index']);

			}

        }



        return $this->render('create', [

            'model' => $model,

			'sectiondetail' => $sectiondetail,

			'teacherdetail' => $teacherdetail,

			'subjectdetail' => $subjectdetail,

			'teacher' => $teacher,
                        
                        'classdetail' => $classdetail

        ]);

    }



    /**

     * Updates an existing ErpSchoolClasswise model.

     * If update is successful, the browser will be redirected to the 'view' page.

     * @param integer $id

     * @return mixed

     * @throws NotFoundHttpException if the model cannot be found

     */

    public function actionUpdate($id)

    {


		 $action = "update";

		$classdetail = ArrayHelper::map(ErpMasterClass::find()

				  ->where(['school_uuid' => Yii::$app->utility->getLoginschooluuid()])

				  ->all(),'class_uuid','class_title');		 

	     $sectiondetail = ArrayHelper::map(ErpMasterClassSection::find()

				  ->where(['school_uuid' => Yii::$app->utility->getLoginschooluuid()])

				  ->all(),'section_uuid','section_title');

				  

	     $teacherdetail = ArrayHelper::map(ErpMasterTeacher::find()

				  ->where(['teacher_school_uuid' => Yii::$app->utility->getLoginschooluuid()])

				  ->all(),'teacher_uuid','teacher_last_name');

	    

	     $subjectdetail = ArrayHelper::map(ErpMasterSubject::find()

				  ->where(['subject_school_uuid' => Yii::$app->utility->getLoginschooluuid()])

				  ->all(),'subject_uuid','subject_name');



		$model = ErpSchoolClasswise::find()

				  ->where(['classwise_id' => $id])

				  ->one();

		

		$teacher = TeacherSubjectMapping::find()

				  ->where(['class_uuid' => $model->classwise_uuid])

				  ->all();

				  

	   if ($model->load(Yii::$app->request->post())) { 

	     

		   if($model->save()) {

			

				  return $this->redirect(['view', 'id' => $model->classwise_id]);  

			   

		   }

	   

	   }



        return $this->render('update', [

            'model' => $model,

			'sectiondetail' => $sectiondetail,

			'teacherdetail' => $teacherdetail,

			'subjectdetail' => $subjectdetail,

			'teacher' => $teacher,

			'action' => $action,

                        'classdetail' => $classdetail

        ]);

    }



    /**

     * Deletes an existing ErpSchoolClasswise model.

     * If deletion is successful, the browser will be redirected to the 'index' page.

     * @param integer $id

     * @return mixed

     * @throws NotFoundHttpException if the model cannot be found

     */

    public function actionDelete($id)

    {

        $model = ErpSchoolClasswise::find()->where(['classwise_id' => $id])->one();

		

		$model->classwise_is_delete = 'YES';

        

		$model->save();



        return $this->redirect(['index']);

    }



    /**

     * Finds the ErpSchoolClasswise model based on its primary key value.

     * If the model is not found, a 404 HTTP exception will be thrown.

     * @param integer $id

     * @return ErpSchoolClasswise the loaded model

     * @throws NotFoundHttpException if the model cannot be found

     */

    protected function findModel($id)

    {

        if (($model = ErpSchoolClasswise::findOne($id)) !== null) {

            return $model;

        }



        throw new NotFoundHttpException('The requested page does not exist.');

    }

}

