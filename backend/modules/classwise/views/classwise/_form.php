<?php



use yii\helpers\Html;

use yii\widgets\ActiveForm;



/* @var $this yii\web\View */

/* @var $model app\modules\teacher\models\ErpMasterTeacher */

/* @var $form ActiveForm */

?>

<div class="classwise_form">



    <?php $form = ActiveForm::begin(array('options'=>array("class"=>'smart-form'))); ?>

    

    <fieldset>

   <div class="row">

      <section class="col col-6">

         <label class="input">

         
        <?= $form->field($model, 'classwise_class_name')->dropDownList([$classdetail], ['prompt' => 'Choose One']) ?>

         </label>

      </section>

     <section class="col col-6">

         <label class="input">

         <?= $form->field($model, 'section_uuid')->dropDownList([$sectiondetail], ['prompt' => 'Choose One']) ?>

         </label>

      </section>

      <section class="col col-6">

         <label class="input">

          <?= $form->field($model, 'classwise_is_regular')->dropDownList([ 'YES' => 'YES', 'NO' => 'NO', ], ['prompt' => 'Choose One']) ?>

         </label>

      </section>

      <section class="col col-6">

         <label class="input">

          <?= $form->field($model, 'classwise_is_status')->dropDownList([ 'ACTIVE' => 'ACTIVE', 'INACTIVE' => 'INACTIVE', ], ['prompt' => 'Choose One']) ?>

         </label>

      </section>

   </div>

</fieldset>

<hr/>

<fieldset>

   <div class="row">

     <div id="clonedInput1" class="clonedInput">

     <?php if(isset($action)) { 

	     

		foreach($teacher as $value) { ?>

	   	  

       <section class="col col-6">

         <label class="input">

         <?= $form->field($value, 'teacher_uuid')->dropDownList([$teacherdetail], ['prompt' => 'Choose One','readonly' => true]) ?>

         </label>

      </section>

      <section class="col col-6">

         <label class="input">

          <?= $form->field($value, 'subject_uuid')->dropDownList([$subjectdetail], ['prompt' => 'Choose One','readonly' => true]) ?>

         </label>

      </section>

		  	

		 	

	 <?php	}

		 

		 

	 } else { ?>

      <section class="col col-6">

         <label class="input">

         <?= $form->field($teacher, 'teacher_uuid[]')->dropDownList([$teacherdetail], ['prompt' => 'Choose One']) ?>

         </label>

      </section>

      <section class="col col-6">

         <label class="input">

          <?= $form->field($teacher, 'subject_uuid[]')->dropDownList([$subjectdetail], ['prompt' => 'Choose One']) ?>

         </label>

      </section>

      

      <div class="actions">

        <a class="clone btn btn-primary">Clone</a> 

        <a class="remove btn btn-danger">Remove</a>

      </div>

      <?php } ?>

      </div>

        <!----clone start ------>

      <div id="newHtml" class="newHtml1">

      </div>

        <!----clone end --------->

      </div>

</fieldset>

<footer>

   <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>

</footer>

   <?php ActiveForm::end(); ?>

</div><!-- teacher-_form -->

