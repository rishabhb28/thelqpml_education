<?php



use yii\helpers\Html;

use yii\grid\GridView;



/* @var $this yii\web\View */

/* @var $dataProvider yii\data\ActiveDataProvider */



$this->title = 'Erp School Classwises';

$this->params['breadcrumbs'][] = $this->title;

?>

<div class="erp-school-classwise-index">



    <h1><?= Html::encode($this->title) ?></h1>



    <p>

        <?= Html::a('Create Erp School Classwise', ['create'], ['class' => 'btn btn-success']) ?>

    </p>



    <?= GridView::widget([

         'dataProvider' => $dataProvider,

        'columns' => [

            ['class' => 'yii\grid\SerialColumn'],



            'classwise_id',

            //'classwise_uuid',

            //'classwise_school_uuid',

            'classwiseClassName.class_title',

			'sectionUu.section_title',

            'classwise_is_regular',

            'classwise_is_status',

            'classwise_is_delete',

            //'classwise_created_date',



            ['class' => 'yii\grid\ActionColumn'],

        ],

    ]); ?>

</div>

