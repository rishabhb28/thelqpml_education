<?php



use yii\helpers\Html;

use yii\widgets\DetailView;



/* @var $this yii\web\View */

/* @var $model app\modules\classwise\models\ErpSchoolClasswise */



$this->title = $model->classwise_id;

$this->params['breadcrumbs'][] = ['label' => 'Erp School Classwises', 'url' => ['index']];

$this->params['breadcrumbs'][] = $this->title;

?>

<div class="erp-school-classwise-view">



    <h1><?= Html::encode($this->title) ?></h1>



    <p>

        <?= Html::a('Update', ['update', 'id' => $model->classwise_id], ['class' => 'btn btn-primary']) ?>

        <?= Html::a('Delete', ['delete', 'id' => $model->classwise_id], [

            'class' => 'btn btn-danger',

            'data' => [

                'confirm' => 'Are you sure you want to delete this item?',

                'method' => 'post',

            ],

        ]) ?>

    </p>



    <?= DetailView::widget([

        'model' => $model,

        'attributes' => [

            'classwise_id',

            //'classwise_uuid',

            //'classwise_school_uuid',

            'classwiseClassName.class_title',

			'sectionUu.section_title',

            'classwise_is_regular',

            'classwise_is_status',

            'classwise_is_delete',

            'classwise_created_date',

        ],

    ]) ?>



</div>

