<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\classwise\models\ErpSchoolClasswise */

$this->title = 'Create School Class';
$this->params['breadcrumbs'][] = ['label' => 'School Class', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
		<h1 class="page-title txt-color-blueDark">
			
			<!-- PAGE HEADER -->
			<i class="fa-fw fa fa-pencil-square-o"></i> 
				Dashboard
			<span>>  
				Create Class
			</span>
		</h1>
	</div>
</div>
<!-- widget grid -->
<section id="widget-grid" class="">


	<!-- START ROW -->

	<div class="row">

		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">
			
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">

				<header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Class Information Form </h2>				
					
				</header>

				<!-- widget div-->
				<div>
					
					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						
					</div>
					<!-- end widget edit box -->
					
					<!-- widget content -->
					<div class="widget-body no-padding">
						
						<!-- end widget grid -->
						<?= $this->render('_form', [
                            'model' => $model,
							'sectiondetail' => $sectiondetail,
							'teacherdetail' => $teacherdetail,
							'subjectdetail' => $subjectdetail,
							'teacher' => $teacher,
                                                        'classdetail' => $classdetail
                        ]) ?> 

					</div>
					<!-- end widget content -->
					
				</div>
				<!-- end widget div -->
				
			</div>


		</article>
	

	</div>

	<!-- END ROW -->
</section>
  <script type="text/javascript">
var regex = /^(.+?)(\d+)$/i;
var cloneIndex = $(".clonedInput").length;

function clone(){

    $(this).parents(".clonedInput").clone()
        .appendTo(".newHtml1")
        .attr("id", "clonedInput" +  cloneIndex)
        .find("*")
        .each(function() {
            var id = this.id || "";
            var match = id.match(regex) || [];
            if (match.length == 3) {
                this.id = match[1] + (cloneIndex);
            }
        })
        .on('click', '.clone', clone)
        .on('click', '.remove', remove);
    cloneIndex++;
}
function remove(){
    $(this).parents(".clonedInput").remove();
}
$(".clone").on("click", clone);

$(".remove").on("click", remove);
    </script>
