<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\classwise\models\ErpMasterClass */

$this->title = 'Update Erp Master Class: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Erp Master Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->class_id, 'url' => ['view', 'id' => $model->class_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="erp-master-class-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
