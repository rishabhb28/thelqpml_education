<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\classwise\models\ErpMasterClass */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="classwise_form">

    <?php $form = ActiveForm::begin(array('options'=>array("class"=>'smart-form'))); ?>

<fieldset>
   <div class="row">
      <section class="col col-6">
         <label class="input">
        
        <?= $form->field($model, 'class_title')->textInput(['placeholder' => 'Enter Title']) ?>
         </label>
      </section>
      <section class="col col-6">
         <label class="input">
         <?= $form->field($model, 'class_status')->dropDownList([ 'ACTIVE' => 'ACTIVE', 'INACTIVE' => 'INACTIVE', ], ['prompt' => 'Choose One']) ?>
         </label>
      </section>

   </div>
</fieldset>
<footer>
   <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
</footer>

    <?php ActiveForm::end(); ?>

</div>


