<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\classwise\models\ErpMasterClass */

$this->title = 'Create Erp Master Class';
$this->params['breadcrumbs'][] = ['label' => 'Erp Master Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="erp-master-class-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
