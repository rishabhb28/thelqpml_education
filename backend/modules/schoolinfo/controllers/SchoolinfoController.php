<?php

namespace app\modules\schoolinfo\controllers;

use Yii;
use app\modules\schoolinfo\models\ErpSchoolInformation;
use app\modules\schoolinfo\models\ErpSchoolInformationSearch;
use app\models\User;
use app\models\States;
use app\models\Cities;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\rbac\DbManager;
use yii\helpers\ArrayHelper;

/**
 * SchoolinfoController implements the CRUD actions for ErpSchoolInformation model.
 */ 
class SchoolinfoController extends Controller
{
    /**
     * @inheritdoc
     */
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
	public function behaviors()
		{
			return [
				'verbs' => [
					'class' => VerbFilter::className(),
					'actions' => [
						//'delete' => ['post'],
					],
				],
				'access' => [
							'class' => \yii\filters\AccessControl::className(),
							'only' => ['index','create','update','view','delete'],
							'rules' => [
								// allow authenticated users
								[
									'allow' => true,
									'roles' => ['@'],
								],
								// everything else is denied
							],
						],            
			];
		}

    /**
     * Lists all ErpSchoolInformation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ErpSchoolInformationSearch();
		
		$school_main_uuid = Yii::$app->utility->getLoginschooluuid();
		
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		$dataProvider->query->where(['school_main_uuid' => $school_main_uuid]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ErpSchoolInformation model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ErpSchoolInformation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		Yii::$app->user->id;
		
		$model = new ErpSchoolInformation();
		
		$school_main_uuid = User::find()->where(array('id' => Yii::$app->user->id))->one();
	
		$getAllstates = ArrayHelper::map(States::find()->where(array('country_id' => '101','id' => 13))->all(),'id', 'name');
		
		$getAllcities = ArrayHelper::map(Cities::find()->where(array('state_id' => '13'))->all(),'id', 'name');
		
		$moduleName = "ErpSchoolInformation"; //Module Name        

		$tableName = "ErpSchoolInformation"; //Table Class Nam

		$school_uuid = Yii::$app->utility->generateGuuid($moduleName, $tableName); //Generate UUID
		
		$model->school_uuid = $school_uuid;
		
		$model->school_main_uuid = $school_main_uuid['user_uuid'];
		
		$model->school_created_date = date("Y-m-d h:i:s");

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			
		   $user = new User();
		   
		   $usermoduleName = "ErpSchoolInformation"; //Module Name        

		   $usertableName = "ErpSchoolInformation"; //Table Class Nam
		   
		   $user_uuid = Yii::$app->utility->generateGuuid($usermoduleName, $usertableName); //Generate UUID
		    
		   $user->user_uuid = $user_uuid;
		   $user->username = $model->school_email;
		   $user->auth_key = Yii::$app->security->generateRandomString();
		   $user->email = $model->school_email;
		   $user->phone = $model->school_phone;
		   $user->role = "2";
		   $user->created_at = date("Y-m-d h:i:s");
		   if($user->save()) {
           // return $this->redirect(['view', 'id' => $model->school_id]);
		   return $this->redirect(['index']);
		   } 
        } 

        return $this->render('create', [
            'model' => $model,
			'getAllstates' => $getAllstates,
			'getAllcities' => $getAllcities
        ]);
    }
	

    /**
     * Creates a new ErpSchoolInformation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreaterole()
    {
		$r=new DbManager;
		$r->init();
		$test = $r->createRole('teacher');
		$r->add($test);
	}
    /**
     * Updates an existing ErpSchoolInformation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
	    $getAllstates = ArrayHelper::map(States::find()->where(array('country_id' => '101','id' => 13))->all(),'id', 'name');
		
		$getAllcities = ArrayHelper::map(Cities::find()->where(array('state_id' => '13'))->all(),'id', 'name');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->school_id]);
        }

        return $this->render('update', [
            'model' => $model,
			'getAllstates' => $getAllstates,
			'getAllcities' => $getAllcities
        ]);
    }

    /**
     * Deletes an existing ErpSchoolInformation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = ErpSchoolInformation::find()->where(['school_id' => $id])->one();
		
		$model->school_is_deleted = 'YES';
        
		$model->save();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ErpSchoolInformation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ErpSchoolInformation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ErpSchoolInformation::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
