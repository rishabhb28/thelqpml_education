<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolinfo\models\ErpSchoolInformation */

$this->title = 'Update Erp School Information: '.$model->school_name;
$this->params['breadcrumbs'][] = ['label' => 'Erp School Informations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->school_id, 'url' => ['view', 'id' => $model->school_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="erp-school-information-update">

    <h1><?= Html::encode($this->title) ?></h1>

	  <?= $this->render('_form', [
         'model' => $model,
          'getAllstates' => $getAllstates,
          'getAllcities' => $getAllcities
         ]) ?>

</div>
