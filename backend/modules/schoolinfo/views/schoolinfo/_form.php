<?php
   use yii\helpers\Html;
   
   use yii\widgets\ActiveForm;

   /* @var $this yii\web\View */
   
   /* @var $model app\modules\schoolinfo\models\ErpSchoolInformation */
   
   /* @var $form yii\widgets\ActiveForm */
   
   ?>
<?php $form = ActiveForm::begin(array('options'=>array("class"=>'smart-form'))); ?>
<fieldset>
   <div class="row">
      <section class="col col-6">
         <label class="input">
         <?= $form->field($model, 'school_reg_number')->textInput(['placeholder' => 'Registeration Number']) ?>
         </label>
      </section>
      <section class="col col-6">
         <label class="input">
         <?= $form->field($model, 'school_name')->textInput(['placeholder' => 'School Name'])?>
         </label>
      </section>
   </div>
</fieldset>
<fieldset>
   <div class="row">
      <section class="col col-5">
         <label class="select">
          <?= $form->field($model, 'school_state')->dropDownList([$getAllstates]) ?>
         </label>
      </section>
      <section class="col col-4">
         <label class="input">
         <?= $form->field($model, 'school_dist')->dropDownList([$getAllcities],['prompt' => 'Choose City','id'=>'state']) ?>
         </label>
      </section>
      <section class="col col-3">
         <label class="select">
         <?= $form->field($model, 'school_post_code')->textInput(['placeholder' => 'School Post Code']) ?></label>
      </section>
   </div>
   <div class="row">
      <section class="col col-4">
         <label class="input">
         <?= $form->field($model, 'school_email')->textInput(['placeholder' => 'School Email']) ?>
         </label>
      </section>
      <section class="col col-4">
         <label class="input">
         <?= $form->field($model, 'school_phone')->textInput(['placeholder' => 'School Phone Number']) ?>
         </label>
      </section>
      <section class="col col-4">
         <label class="input">
         <?= $form->field($model, 'school_board')->textInput(['placeholder' => 'School Board']) ?>
         </label>
      </section>
   </div>
</fieldset>
<footer>
   <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
</footer>
<?php ActiveForm::end(); ?>