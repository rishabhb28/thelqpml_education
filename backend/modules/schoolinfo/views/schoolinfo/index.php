<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolinfo\models\ErpSchoolInformationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Erp School Informations';
$this->params['breadcrumbs'][] = $this->title;
?>

  
  
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create School', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'school_id',
            'school_reg_number',
            'school_name',
            //'school_country',
            'school_state',
            'school_dist',
            'school_board',
            'school_is_deleted',
            'school_status',
            //'school_created_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

