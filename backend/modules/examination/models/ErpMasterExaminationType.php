<?php

namespace app\modules\examination\models;

use Yii;
use app\modules\schoolinfo\models\ErpSchoolInformation;
/**
 * This is the model class for table "erp_master_examination_type".
 *
 * @property int $examination_type_id
 * @property string $examination_type_uuid
 * @property string $school_uuid
 * @property string $examination_type_name
 * @property string $examination_start_date
 * @property string $examination_end_date
 * @property string $examination_type_status
 * @property string $examination_is_deleted
 * @property string $examination_created_date
 *
 * @property ErpSchoolInformation $schoolUu
 */
class ErpMasterExaminationType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'erp_master_examination_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['examination_type_uuid', 'school_uuid', 'examination_type_name', 'examination_start_date', 'examination_end_date', 'examination_created_date'], 'required'],
            [['examination_start_date', 'examination_end_date', 'examination_created_date'], 'safe'],
            [['examination_type_status', 'examination_is_deleted'], 'string'],
            [['examination_type_uuid', 'school_uuid'], 'string', 'max' => 128],
            [['examination_type_name'], 'string', 'max' => 20],
            [['school_uuid'], 'exist', 'skipOnError' => true, 'targetClass' => ErpSchoolInformation::className(), 'targetAttribute' => ['school_uuid' => 'school_uuid']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'examination_type_id' => 'Examination Type ID',
            'examination_type_uuid' => 'Examination Type Uuid',
            'school_uuid' => 'School Uuid',
            'examination_type_name' => 'Examination Type Name',
            'examination_start_date' => 'Examination Start Date',
            'examination_end_date' => 'Examination End Date',
            'examination_type_status' => 'Examination Type Status',
            'examination_is_deleted' => 'Examination Is Deleted',
            'examination_created_date' => 'Examination Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolUu()
    {
        return $this->hasOne(ErpSchoolInformation::className(), ['school_uuid' => 'school_uuid']);
    }

    /**
     * @inheritdoc
     * @return ErpMasterExaminationTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ErpMasterExaminationTypeQuery(get_called_class());
    }
}
