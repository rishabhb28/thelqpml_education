<?php



use yii\helpers\Html;





/* @var $this yii\web\View */

/* @var $model app\modules\examination\models\ErpMasterExamination */



$this->title = 'Create Erp Master Examination';

$this->params['breadcrumbs'][] = ['label' => 'Erp Master Examinations', 'url' => ['index']];

$this->params['breadcrumbs'][] = $this->title;

?>

<div class="erp-master-examination-create">



    <h1><?= Html::encode($this->title) ?></h1>



    <?= $this->render('_form', [

        'model' => $model,

		'classdetail' => $classdetail,

		'subjectdetail' => $subjectdetail,

		'examtype' => $examtype,
		
		'action' => $action

    ]) ?>



</div>

  <script type="text/javascript">

var regex = /^(.+?)(\d+)$/i;

var cloneIndex = $(".clonedInput").length;



function clone(){



    $(this).parents(".clonedInput").clone()

        .appendTo(".newHtml1")

        .attr("id", "clonedInput" +  cloneIndex)

        .find("*")

        .each(function() {

            var id = this.id || "";

            var match = id.match(regex) || [];

            if (match.length == 3) {

                this.id = match[1] + (cloneIndex);

            }

        })

        .on('click', '.clone', clone)

        .on('click', '.remove', remove);

    cloneIndex++;

}

function remove(){

    $(this).parents(".clonedInput").remove();

}

$(".clone").on("click", clone);



$(".remove").on("click", remove);

    </script>