<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\examination\models\ErpMasterExamination */

$this->title = $model->examination_id;
$this->params['breadcrumbs'][] = ['label' => 'Erp Master Examinations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="erp-master-examination-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->examination_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->examination_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'examination_id',
            //'examination_uuid',
            //'school_uuid',
            'examTypeUu.examination_type_name',
            'examClassUu.class_title',
            'examSubjectUu.subject_name',
            'exam_date',
            'examination_start_time',
            'examination_end_time',
            'exam_status',
            'exam_is_deleted',
            'exam_created_date',
        ],
    ]) ?>

</div>
