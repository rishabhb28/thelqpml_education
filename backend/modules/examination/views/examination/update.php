<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\examination\models\ErpMasterExaminationType */

$this->title = 'Update Erp Master Examination Type: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Erp Master Examination Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->examination_type_id, 'url' => ['view', 'id' => $model->examination_type_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="erp-master-examination-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
