<?php

namespace app\modules\examination\controllers;

use Yii;
use app\modules\examination\models\ErpMasterExamination;
use app\modules\examination\models\ErpMasterExaminationType;
use app\modules\classwise\models\ErpMasterClass;
use app\modules\subject\models\ErpMasterSubject;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
/**
 * ExaminationconfigureController implements the CRUD actions for ErpMasterExamination model.
 */
class ExaminationconfigureController extends Controller
{
    /**
     * @inheritdoc
     */
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
	public function behaviors()
		{
			return [
				'verbs' => [
					'class' => VerbFilter::className(),
					'actions' => [
						//'delete' => ['post'],
					],
				],
				'access' => [
							'class' => \yii\filters\AccessControl::className(),
							'only' => ['index','create','update','view','delete'],
							'rules' => [
								// allow authenticated users
								[
									'allow' => true,
									'roles' => ['@'],
								],
								// everything else is denied
							],
						],            
			];
		}

    /**
     * Lists all ErpMasterExamination models.
     * @return mixed
     */
    public function actionIndex()
    {
		$school_uuid = Yii::$app->utility->getLoginschooluuid();
		
        $dataProvider = new ActiveDataProvider([
            'query' => ErpMasterExamination::find()->with('examTypeUu')->with('examClassUu')->with('examSubjectUu'),
        ]);
		
		$dataProvider->query->where(['school_uuid' => $school_uuid]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ErpMasterExamination model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
	$model = ErpMasterExamination::find()->where(['examination_id' => $id])->with('examTypeUu')->with('examClassUu')->with('examSubjectUu')->one();
		
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new ErpMasterExamination model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ErpMasterExamination();
		
	    $examtype = ArrayHelper::map(ErpMasterExaminationType::find()
				  ->where(['school_uuid' => Yii::$app->utility->getLoginschooluuid()])
				  ->all(),'examination_type_uuid','examination_type_name');
		
		$classdetail = ArrayHelper::map(ErpMasterClass::find()
				  ->where(['school_uuid' => Yii::$app->utility->getLoginschooluuid()])
				  ->all(),'class_uuid','class_title');
				  
	    $subjectdetail = ArrayHelper::map(ErpMasterSubject::find()
				  ->where(['subject_school_uuid' => Yii::$app->utility->getLoginschooluuid()])
				  ->all(),'subject_uuid','subject_name');
				  
        $action = "create";

		if ($model->load(Yii::$app->request->post())) { 
		
		  $i = 0;
		
		  $len =  count($_POST['ErpMasterExamination']['exam_subject_uuid']);
		
		  foreach($_POST['ErpMasterExamination']['exam_subject_uuid'] as $key=>$value) { 
		  
		  $data = new ErpMasterExamination();
		  
		  $moduleName = "ErpMasterExamination"; //Module Name        

		  $tableName = "ErpMasterExamination"; //Table Class Nam
		
		   $examination_uuid = Yii::$app->utility->generateGuuid($moduleName, $tableName); //Generate UUID
		  
		   $data->examination_uuid = $examination_uuid;
		  
		   $data->school_uuid = Yii::$app->utility->getLoginschooluuid();
		   
		   $data->exam_type_uuid = $_POST['ErpMasterExamination']['exam_type_uuid'];
		   
		   $data->exam_class_uuid = $_POST['ErpMasterExamination']['exam_class_uuid'];
		   
		   $data->exam_subject_uuid = $value;
		   
		   $data->exam_date = $_POST['ErpMasterExamination']['exam_date'][$key];
		   
		   $data->examination_start_time = $_POST['ErpMasterExamination']['examination_start_time'][$key];
		   
		   $data->examination_end_time = $_POST['ErpMasterExamination']['examination_end_time'][$key];
		  
		   $data->exam_created_date = date("Y-m-d h:i:s");
		   
		   $data->save();
		   
		   if ($i == $len - 1) {
			   
			   return $this->redirect(['view', 'id' => $data->examination_id]);
				// last
		   }
		  $i++;
		  }

		}

        return $this->render('create', [
            'model' => $model,
			'classdetail' => $classdetail,
			'subjectdetail' => $subjectdetail,
			'examtype' => $examtype,
			'action' => $action
        ]);
    }

    /**
     * Updates an existing ErpMasterExamination model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {


$examtype = ArrayHelper::map(ErpMasterExaminationType::find()
				  ->where(['school_uuid' => Yii::$app->utility->getLoginschooluuid()])
				  ->all(),'examination_type_uuid','examination_type_name');
		
		$classdetail = ArrayHelper::map(ErpMasterClass::find()
				  ->where(['school_uuid' => Yii::$app->utility->getLoginschooluuid()])
				  ->all(),'class_uuid','class_title');
				  
	    $subjectdetail = ArrayHelper::map(ErpMasterSubject::find()
				  ->where(['subject_school_uuid' => Yii::$app->utility->getLoginschooluuid()])
				  ->all(),'subject_uuid','subject_name');

        $model = $this->findModel($id);
		
		$action = "update";
if ($model->load(Yii::$app->request->post())) {

$model->exam_date = $_POST['ErpMasterExamination']['exam_date'][0];
$model->examination_start_time = $_POST['ErpMasterExamination']['examination_start_time'][0];
$model->examination_end_time = $_POST['ErpMasterExamination']['examination_end_time'][0];
if($model->save()) {
 return $this->redirect(['view', 'id' => $model->examination_id]);
}
}
        
        return $this->render('update', [
            'model' => $model,
			'classdetail' => $classdetail,
			'subjectdetail' => $subjectdetail,
			'examtype' => $examtype,
			'action' => $action
        ]);
    }

    /**
     * Deletes an existing ErpMasterExamination model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = ErpMasterExamination::find()->where(['examination_id' => $id])->one();
		
		$model->exam_is_deleted = 'YES';
        
		$model->save();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ErpMasterExamination model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ErpMasterExamination the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ErpMasterExamination::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
