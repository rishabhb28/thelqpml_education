<?php

namespace app\modules\examination\controllers;

use Yii;
use app\modules\examination\models\ErpMasterExaminationType;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ExaminationController implements the CRUD actions for ErpMasterExaminationType model.
 */
class ExaminationController extends Controller
{
    /**
     * @inheritdoc
     */
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
	public function behaviors()
		{
			return [
				'verbs' => [
					'class' => VerbFilter::className(),
					'actions' => [
						//'delete' => ['post'],
					],
				],
				'access' => [
							'class' => \yii\filters\AccessControl::className(),
							'only' => ['index','create','update','view','delete'],
							'rules' => [
								// allow authenticated users
								[
									'allow' => true,
									'roles' => ['@'],
								],
								// everything else is denied
							],
						],            
			];
		}

    /**
     * Lists all ErpMasterExaminationType models.
     * @return mixed
     */
    public function actionIndex()
    {
		$school_uuid = Yii::$app->utility->getLoginschooluuid();
		
        $dataProvider = new ActiveDataProvider([
            'query' => ErpMasterExaminationType::find(),
        ]);
		
		$dataProvider->query->where(['school_uuid' => $school_uuid]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ErpMasterExaminationType model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ErpMasterExaminationType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ErpMasterExaminationType();
		
		if ($model->load(Yii::$app->request->post())) { 
		
		  $moduleName = "ErpMasterExaminationType"; //Module Name        

		  $tableName = "ErpMasterExaminationType"; //Table Class Nam

		  $examination_type_uuid = Yii::$app->utility->generateGuuid($moduleName, $tableName); //Generate UUID
		  
		  $model->examination_type_uuid = $examination_type_uuid;
		  
		  $model->school_uuid = Yii::$app->utility->getLoginschooluuid();
		  
		  $model->examination_created_date = date("Y-m-d h:i:s");
		  
		  if($model->save()) {
			  
			  return $this->redirect(['view', 'id' => $model->examination_type_id]);
			  
		  } 
		
		}


        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ErpMasterExaminationType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->examination_type_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ErpMasterExaminationType model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = ErpMasterExaminationType::find()->where(['examination_type_id' => $id])->one();
		
		$model->examination_is_deleted = 'YES';
        
		$model->save();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ErpMasterExaminationType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ErpMasterExaminationType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ErpMasterExaminationType::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
