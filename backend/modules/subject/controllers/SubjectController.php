<?php

namespace app\modules\subject\controllers;

use Yii;
use app\modules\subject\models\ErpMasterSubject;
//use app\modules\classwise\models\ErpSchoolClasswise;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
/**
 * SubjectController implements the CRUD actions for ErpMasterSubject model.
 */
class SubjectController extends Controller
{
    /**
     * @inheritdoc
     */
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
	public function behaviors()
		{
			return [
				'verbs' => [
					'class' => VerbFilter::className(),
					'actions' => [
						//'delete' => ['post'],
					],
				],
				'access' => [
							'class' => \yii\filters\AccessControl::className(),
							'only' => ['index','create','update','view','delete'],
							'rules' => [
								// allow authenticated users
								[
									'allow' => true,
									'roles' => ['@'],
								],
								// everything else is denied
							],
						],            
			];
		}

    /**
     * Lists all ErpMasterSubject models.
     * @return mixed
     */
    public function actionIndex()
    {
		$subject_school_uuid = Yii::$app->utility->getLoginschooluuid();
		
        $dataProvider = new ActiveDataProvider([
            'query' => ErpMasterSubject::find(),
        ]);
		
		$dataProvider->query->where(['subject_school_uuid' => $subject_school_uuid]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ErpMasterSubject model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ErpMasterSubject model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
//		$classdetail = ArrayHelper::map(ErpSchoolClasswise::find()
//				  ->where(['classwise_school_uuid' => Yii::$app->utility->getLoginschooluuid()])
//				  ->all(),'classwise_uuid','classwise_class_name');
				  
        $model = new ErpMasterSubject();
		
		$moduleName = "ErpMasterSubject"; //Module Name        

		$tableName = "ErpMasterSubject"; //Table Class Nam

		$subject_uuid = Yii::$app->utility->generateGuuid($moduleName, $tableName); //Generate UUID


        if ($model->load(Yii::$app->request->post())) { 
		
		$model->subject_uuid = $subject_uuid;
		
		$model->subject_school_uuid = Yii::$app->utility->getLoginschooluuid();
		
		$model->subject_created_date = date("Y-m-d h:i:s");
		
		    if($model->save()) {
               return $this->redirect(['index']);
			} 
        }

        return $this->render('create', [
            'model' => $model,
			//'classdetail' => $classdetail
        ]);
    }

    /**
     * Updates an existing ErpMasterSubject model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
	    $classdetail = ArrayHelper::map(ErpSchoolClasswise::find()
				  ->where(['classwise_school_uuid' => Yii::$app->utility->getLoginschooluuid()])
				  ->all(),'classwise_uuid','classwise_class_name');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->subject_id]);
        }

        return $this->render('update', [
            'model' => $model,
			'classdetail' => $classdetail
        ]);
    }

    /**
     * Deletes an existing ErpMasterSubject model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = ErpMasterSubject::find()->where(['subject_id' => $id])->one();
		
		$model->subject_is_deleted = 'YES';
        
		$model->save();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ErpMasterSubject model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ErpMasterSubject the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ErpMasterSubject::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
