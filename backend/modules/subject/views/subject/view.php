<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\subject\models\ErpMasterSubject */

$this->title = $model->subject_id;
$this->params['breadcrumbs'][] = ['label' => 'Erp Master Subjects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="erp-master-subject-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->subject_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->subject_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'subject_id',
            //'subject_uuid',
            //'subject_school_uuid',
            //'subject_class_uuid',
            'subject_name',
            'subject_status',
            'subject_is_deleted',
            'subject_created_date',
        ],
    ]) ?>

</div>
