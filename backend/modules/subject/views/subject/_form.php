<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\subject\models\ErpMasterSubject */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="erp-master-subject-form">

    <?php $form = ActiveForm::begin(array('options'=>array("class"=>'smart-form'))); ?>
    
    <fieldset>
   <div class="row">
      <?php /*?><section class="col col-4">
         <label class="input">
         <?= $form->field($model, 'subject_class_uuid')->dropDownList([$classdetail], ['prompt' => 'Choose One']) ?>
         </label>
      </section><?php */?>
      <section class="col col-4">
         <label class="input">
         <?= $form->field($model, 'subject_name')->textInput(['maxlength' => true]) ?>
         </label>
      </section>
      <section class="col col-4">
         <label class="input">
           <?= $form->field($model, 'subject_status')->dropDownList([ 'ACTIVE' => 'ACTIVE', 'INACTIVE' => 'INACTIVE', ], ['prompt' => 'Choose One']) ?>
         </label>
      </section>
   </div>
</fieldset>
<footer>
   <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
</footer>

    <?php ActiveForm::end(); ?>

</div><!-- teacher-_form -->
