<?php

namespace app\modules\teacher\controllers;

use Yii;
use app\modules\teacher\models\ErpMasterTeacher;
use app\modules\teacher\models\ErpMasterTeacherSearch;
use common\models\User;
use backend\models\SignupForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * TeacherController implements the CRUD actions for ErpMasterTeacher model.
 */
class TeacherController extends Controller
{
    /**
     * @inheritdoc
     */
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
	public function behaviors()
		{
			return [
				'verbs' => [
					'class' => VerbFilter::className(),
					'actions' => [
						//'delete' => ['post'],
					],
				],
				'access' => [
							'class' => \yii\filters\AccessControl::className(),
							'only' => ['index','create','update','view','delete'],
							'rules' => [
								// allow authenticated users
								[
									'allow' => true,
									'roles' => ['@'],
								],
								// everything else is denied
							],
						],            
			];
		}


    /**
     * Lists all ErpMasterTeacher models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ErpMasterTeacherSearch();
		
		$teacher_school_uuid = Yii::$app->utility->getLoginschooluuid();
		
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		$dataProvider->query->where(['teacher_school_uuid' => $teacher_school_uuid]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ErpMasterTeacher model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ErpMasterTeacher model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		
        $model = new ErpMasterTeacher();
		
		if ($model->load(Yii::$app->request->post())) {

				$moduleName = "ErpMasterTeacher"; //Module Name        

		        $tableName = "ErpMasterTeacher"; //Table Class Nam

		        $teacher_uuid = Yii::$app->utility->generateGuuid($moduleName, $tableName); //Generate UUID
				
				$model->teacher_uuid = $teacher_uuid;
				
				$model->teacher_school_uuid = Yii::$app->utility->getLoginschooluuid();
				
				//$model->teacher_password = md5(Yii::$app->request->post('teacher_phone'));

				$model->teacher_employemdnt_date = date("Y-m-d",strtotime($model->teacher_employemdnt_date));
				
				$model->teacher_dob = date("Y-m-d",strtotime($model->teacher_dob));
				
				$model->teacher_created_date = date("Y-m-d h:i:s");
				
				$model->attributes = $_POST['ErpMasterTeacher'];
				
				if($model->save()) {
					
					
					 Yii::$app->getSession()->setFlash('success', 'Teacher Infromation Added');
					  
					 return $this->redirect(['teacher/index']);
					  
				  } else {
					
					Yii::$app->getSession()->setFlash('danger', 'Internal Server Error');
                    
				  }
				
		
			
		} 
        return $this->render('create', [
            'model' => $model,
		
        ]);
    }

    /**
     * Updates an existing ErpMasterTeacher model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->teacher_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ErpMasterTeacher model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
		
		$model = ErpMasterTeacher::find()->where(['teacher_id' => $id])->one();
		
		$model->teacher_is_deleted = 'YES';
        
		$model->save();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ErpMasterTeacher model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ErpMasterTeacher the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ErpMasterTeacher::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
