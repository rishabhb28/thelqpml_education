<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'teachers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="erp-master-teacher-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create teacher', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?php
	$flashMessages = Yii::$app->session->getAllFlashes();
	if ($flashMessages) {
    foreach($flashMessages as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
     }
	}
   ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'teacher_id',
            'teacher_first_name',
            'teacher_last_name',
			'teacher_email',
			'teacher_phone',
			'teacher_marital_status',
			'teacher_dob',
            'teacher_is_deleted',
            'teacher_is_status',
            //'teacher_created_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
