<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\teacher\models\ErpMasterTeacher */
/* @var $form ActiveForm */
?>
<div class="teacher-_form">

    <?php $form = ActiveForm::begin(array('options'=>array("class"=>'smart-form'))); ?>
    
    <fieldset>
   <div class="row">
      <section class="col col-6">
         <label class="input">
         <?= $form->field($model, 'teacher_first_name')->textInput(['placeholder' => 'First Name']) ?>
         </label>
      </section>
      <section class="col col-6">
         <label class="input">
         <?= $form->field($model, 'teacher_last_name')->textInput(['placeholder' => 'Last Name'])?>
         </label>
      </section>
      <section class="col col-6">
         <label class="input">Employemdnt Date
         <input type="date" id="erpmasterteacher-teacher_employemdnt_date" class="form-control" name="ErpMasterTeacher[teacher_employemdnt_date]" placeholder="Employemdnt Date" value="<?=$model->teacher_employemdnt_date?>" aria-required="true" aria-invalid="true">
         </label>
      </section>
      <section class="col col-6">
         <label class="input">Date of Birth
         <input type="date" id="erpmasterteacher-teacher_dob" class="form-control" name="ErpMasterTeacher[teacher_dob]" value="<?=$model->teacher_dob?>" placeholder="Date of Birth" aria-required="true">
         </label>
      </section>
   </div>
</fieldset>
<fieldset>
   <div class="row">
      
      <section class="col col-4">
         <label class="input">
         <?= $form->field($model, 'teacher_email')->textInput(['autofocus' => true,'type' => 'email']) ?>
         </label>
      </section>
      
      <section class="col col-4">
         <label class="input">
         <?= $form->field($model, 'teacher_phone')->textInput(['placeholder' => 'Phone'])?>
         </label>
      </section>
      <section class="col col-4">
         <label class="input">
         
          <?= $form->field($model, 'teacher_marital_status')->dropDownList([ 'Single' => 'Single', 'Married' => 'Married', 'Widower' => 'Widower','Widow' => 'Widow','Divorced' => 'Divorced', 'Separated' => 'Separated' ], ['prompt' => 'Choose Marital']) ?>
         </label>
      </section>
   </div>
</fieldset>
<fieldset>
   <div class="row">
      <section class="col col-6">
         <label class="input">
         <?= $form->field($model, 'teacher_address1')->textInput(['placeholder' => 'Enter Address']) ?>
         </label>
      </section>
      <section class="col col-6">
         <label class="input">
         <?= $form->field($model, 'teacher_address2')->textInput(['placeholder' => 'Enter Address']) ?>
         </label>
      </section>
      <section class="col col-4">
         <label class="input">
         <?= $form->field($model, 'teacher_state')->textInput(['placeholder' => 'Enter City']) ?>
         </label>
      </section>
      <section class="col col-4">
         <label class="input">
         <?= $form->field($model, 'teacher_city')->textInput(['placeholder' => 'Enter State']) ?>
         </label>
      </section>
      <section class="col col-4">
         <label class="input">
         <?= $form->field($model, 'teacher_zip')->textInput(['placeholder' => 'Enter Zip Code']) ?>
         </label>
      </section>
      </div>
  </fieldset>
<footer>
   <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
</footer>

    <?php ActiveForm::end(); ?>

</div><!-- teacher-_form -->
