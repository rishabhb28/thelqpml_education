<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\teacher\models\ErpMasterTeacherSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="erp-master-teacher-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'teacher_id') ?>

    <?= $form->field($model, 'teacher_uuid') ?>

    <?= $form->field($model, 'teacher_school_uuid') ?>

    <?= $form->field($model, 'teacher_first_name') ?>

    <?= $form->field($model, 'teacher_last_name') ?>

    <?php // echo $form->field($model, 'teacher_is_deleted') ?>

    <?php // echo $form->field($model, 'teacher_is_status') ?>

    <?php // echo $form->field($model, 'teacher_created_date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
