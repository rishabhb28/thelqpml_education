<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\teacher\models\ErpMasterTeacher */

$this->title = 'Create Erp Master Teacher';
$this->params['breadcrumbs'][] = ['label' => 'Erp Master Teachers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
		<h1 class="page-title txt-color-blueDark">
			
			<!-- PAGE HEADER -->
			<i class="fa-fw fa fa-pencil-square-o"></i> 
				Dashboard
			<span>>  
				Create Teacher
			</span>
		</h1>
	</div>
</div>
<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- START ROW -->

	<div class="row">

		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">
        
		  <?php
          $flashMessages = Yii::$app->session->getAllFlashes();
          if ($flashMessages) {
          foreach($flashMessages as $key => $message) {
              echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
           }
          }
         ?>
			
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false">

				<header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Teacher Information Form </h2>				
					
				</header>

				<!-- widget div-->
				<div>
					
					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						
					</div>
					<!-- end widget edit box -->
					
					<!-- widget content -->
					<div class="widget-body no-padding">
						
						<!-- end widget grid -->
						<?= $this->render('_form', [
                            'model' => $model,
							
                        ]) ?>

					</div>
					<!-- end widget content -->
					
				</div>
				<!-- end widget div -->
				
			</div>


		</article>
	

	</div>

	<!-- END ROW -->
</section>


