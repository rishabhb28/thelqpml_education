<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\teacher\models\ErpMasterTeacher */

$this->title = 'Update Erp Master Teacher: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Erp Master Teachers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->teacher_id, 'url' => ['view', 'id' => $model->teacher_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="erp-master-teacher-update">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?php
	$flashMessages = Yii::$app->session->getAllFlashes();
	if ($flashMessages) {
    foreach($flashMessages as $key => $message) {
        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
     }
	}
   ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
