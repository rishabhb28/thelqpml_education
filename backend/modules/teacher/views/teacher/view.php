<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\teacher\models\ErpMasterTeacher */

$this->title = $model->teacher_id;
$this->params['breadcrumbs'][] = ['label' => 'Erp Master Teachers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="erp-master-teacher-view">

    <h1>Teacher : <?= Html::encode($model->teacher_first_name.' '.$model->teacher_last_name) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->teacher_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->teacher_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'teacher_id',
            //'teacher_uuid',
            //'teacher_school_uuid',
            'teacher_first_name',
            'teacher_last_name',
            'teacher_email:email',
            'teacher_phone',
            //'teacher_employee_number',
            'teacher_employemdnt_date',
            'teacher_dob',
            'teacher_marital_status',
            'teacher_address1',
			'teacher_address2',
			'teacher_city',
			'teacher_state',
			'teacher_zip',
            'teacher_is_deleted',
            'teacher_is_status',
            'teacher_created_date',
        ],
    ]) ?>
</div>
