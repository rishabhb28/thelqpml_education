<?php

namespace app\modules\teacher\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\teacher\models\ErpMasterTeacher;

/**
 * ErpMasterTeacherSearch represents the model behind the search form of `app\modules\teacher\models\ErpMasterTeacher`.
 */
class ErpMasterTeacherSearch extends ErpMasterTeacher
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teacher_id'], 'integer'],
            [['teacher_uuid', 'teacher_school_uuid', 'teacher_first_name', 'teacher_last_name', 'teacher_is_deleted', 'teacher_is_status', 'teacher_created_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ErpMasterTeacher::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'teacher_id' => $this->teacher_id,
            'teacher_created_date' => $this->teacher_created_date,
        ]);

        $query->andFilterWhere(['like', 'teacher_uuid', $this->teacher_uuid])
            ->andFilterWhere(['like', 'teacher_school_uuid', $this->teacher_school_uuid])
            ->andFilterWhere(['like', 'teacher_first_name', $this->teacher_first_name])
            ->andFilterWhere(['like', 'teacher_last_name', $this->teacher_last_name])
            ->andFilterWhere(['like', 'teacher_is_deleted', $this->teacher_is_deleted])
            ->andFilterWhere(['like', 'teacher_is_status', $this->teacher_is_status]);

        return $dataProvider;
    }
}
