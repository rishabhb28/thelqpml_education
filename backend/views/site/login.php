<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="content" class="container">
   <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8 hidden-xs hidden-sm">
         <h1 class="txt-color-red login-header-big">Securing Next Generation</h1>
         <div class="hero">
            <div class="pull-left login-desc-box-l">
               <h4 class="paragraph-header">It's Okay to be Smart. Experience the simplicity of SmartAdmin, everywhere you go!</h4>
               <div class="login-app-icons">
                  <a href="javascript:void(0);" class="btn btn-danger btn-sm">View More</a>
                  <a href="javascript:void(0);" class="btn btn-danger btn-sm">Find out more</a>
               </div>
            </div>
            <img src="img/demo/iphoneview.png" class="pull-right display-image" alt="" style="width:210px">
         </div>
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
               <h5 class="about-heading">About Securing Next Generation - Are you up to date?</h5>
               <p>
                  Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa.
               </p>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
               <h5 class="about-heading">Not just your average template!</h5>
               <p>
                  Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi voluptatem accusantium!
               </p>
            </div>
         </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
         <div class="well no-padding">
            <?php
               $form = ActiveForm::begin(
               [
               'id' => 'login-form',
               'options' => [
               'class' => 'smart-form client-form'
               ]
               ]
               );
               ?>
            <header>
               Sign In
            </header>
            <fieldset>
               <section>
                  <label class="label">Username</label>
                  <label class="input"> <i class="icon-append fa fa-user"></i>
                  <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label(false) ?>
                  </label>
               </section>
               <section>
                  <label class="label">Password</label>
                  <label class="input"> <i class="icon-append fa fa-lock"></i>
                  <?= $form->field($model, 'password')->passwordInput()->label(false) ?>
                  </label>
                  <div class="note">
                     <a href="forgotpassword.html">Forgot password?</a>
                  </div>
               </section>
               <section>
                  <label class="checkbox">
                  <?= $form->field($model, 'rememberMe')->checkbox()->label(false) ?>
                  </label>
               </section>
            </fieldset>
            <footer>
               <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </footer>
            <?php ActiveForm::end(); ?>
         </div>
         <h5 class="text-center"> - Or sign in using -</h5>
         <ul class="list-inline text-center">
            <li>
               <a href="javascript:void(0);" class="btn btn-primary btn-circle"><i class="fa fa-facebook"></i></a>
            </li>
            <li>
               <a href="javascript:void(0);" class="btn btn-info btn-circle"><i class="fa fa-twitter"></i></a>
            </li>
            <li>
               <a href="javascript:void(0);" class="btn btn-warning btn-circle"><i class="fa fa-linkedin"></i></a>
            </li>
         </ul>
      </div>
   </div>
</div>
