<?php
   /* @var $this \yii\web\View */
   /* @var $content string */
   
   use backend\assets\AppAsset;
   use yii\helpers\Html;
   use yii\bootstrap\Nav;
   use yii\bootstrap\NavBar;
   use yii\widgets\Breadcrumbs;
   use common\widgets\Alert;
   
   AppAsset::register($this);
   ?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html id="extr-page" lang="<?= Yii::$app->language ?>">
   <head>
      <meta charset="<?= Yii::$app->charset ?>">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <?= Html::csrfMetaTags() ?>
      <title>
         <?= Html::encode($this->title) ?>
      </title>
      <?php $this->head() ?>
      <!-- #FAVICONS -->
      <link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
      <link rel="icon" href="img/favicon/favicon.ico" type="image/x-icon">
      <!-- #GOOGLE FONT -->
      <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
      <!-- #APP SCREEN / ICONS -->
      <!-- Specifying a Webpage Icon for Web Clip 
         Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
      <link rel="apple-touch-icon" href="img/splash/sptouch-icon-iphone.png">
      <link rel="apple-touch-icon" sizes="76x76" href="img/splash/touch-icon-ipad.png">
      <link rel="apple-touch-icon" sizes="120x120" href="img/splash/touch-icon-iphone-retina.png">
      <link rel="apple-touch-icon" sizes="152x152" href="img/splash/touch-icon-ipad-retina.png">
      <!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="apple-mobile-web-app-status-bar-style" content="black">
      <!-- Startup image for web apps -->
      <link rel="apple-touch-startup-image" href="img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
      <link rel="apple-touch-startup-image" href="img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
      <link rel="apple-touch-startup-image" href="img/splash/iphone.png" media="screen and (max-device-width: 320px)">
   </head>
   <body>
      <?php $this->beginBody() ?>
      <header id="header">
         <div id="logo-group"> <span id="logo"> <?php echo Html::img('@web/img/logo-new.png',array('class'=>'img-responsive')) ?></span> </div>
      </header>
      <div id="main" role="main">
         <?=$content?>
      </div>
      <?php $this->endBody() ?>
   </body>
</html>
<?php $this->endPage() ?>