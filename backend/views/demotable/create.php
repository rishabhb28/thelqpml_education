<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DemoTable */

$this->title = 'Create Demo Table';
$this->params['breadcrumbs'][] = ['label' => 'Demo Tables', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="demo-table-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
