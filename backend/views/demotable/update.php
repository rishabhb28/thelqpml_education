<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DemoTable */

$this->title = 'Update Demo Table: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Demo Tables', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="demo-table-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
