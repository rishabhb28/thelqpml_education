<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'New Demos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="new-demo-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create New Demo', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'newid',
            'student_number',
            'total_marks',
            'obtained_marks_',
            'student_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
