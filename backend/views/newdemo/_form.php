<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NewDemo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="new-demo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'newid')->textInput() ?>

    <?= $form->field($model, 'student_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total_marks')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'obtained_marks_')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'student_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
