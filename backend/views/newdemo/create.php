<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\NewDemo */

$this->title = 'Create New Demo';
$this->params['breadcrumbs'][] = ['label' => 'New Demos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="new-demo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
