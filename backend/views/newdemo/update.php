<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NewDemo */

$this->title = 'Update New Demo: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'New Demos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->newid, 'url' => ['view', 'id' => $model->newid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="new-demo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
