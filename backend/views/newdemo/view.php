<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\NewDemo */

$this->title = $model->newid;
$this->params['breadcrumbs'][] = ['label' => 'New Demos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="new-demo-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->newid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->newid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'newid',
            'student_number',
            'total_marks',
            'obtained_marks_',
            'student_id',
        ],
    ]) ?>

</div>
