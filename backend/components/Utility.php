<?php

namespace app\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use PDO;
use yii\web\Cookie;
use yii\web\Session;
use app\models\User;
use app\modules\schoolinfo\models\ErpSchoolInformation;

use yii\db\Query;

class Utility extends Component {

  /**
   * Function to redirect on changepassword page when user login first time
   * @return Boolean
   */
  public function forcefullyChangePassword() {
    return FALSE;

}

    /**
     * Function to generater unique token for SSO
     * @return String $uuid
     */
    static function generateGuuid($moduleName, $tableName) {
        $time = time();
        $randValue = rand(10000000, 99999999);
        $firstmd5 = md5($time . $randValue);

        $time = time();
        $randValue = rand(10000000, 99999999);
        $secondmd5 = md5($moduleName . $tableName . $randValue);

        $guuid = strtoupper($firstmd5 . $secondmd5);
        return $guuid;
    }
	
	public function getLoginuseruuid() {
	  
	  $id = Yii::$app->user->id;

	  
	  $model = User::find()
				  ->where(['id' => $id])
				  ->one();
	  
	  $userUUid = $model->user_uuid;
	  
	  return $userUUid;
		
	}

	public function getLoginschooluuid() {
	  
	  $id = $this->getLoginuseruuid();
	  
	  $model = ErpSchoolInformation::find()
				  ->where(['school_main_uuid' => $id])
				  ->one();
	  
	  $scholUUid = $model->school_uuid;
	  
	  return $scholUUid;
		
	}  

}
