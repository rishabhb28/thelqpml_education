<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/site.css',
		'css/bootstrap.min.css',
		'css/style.css',
		'css/responsive.css',
		'css/animate-min.css',
		'css/owl.carousel.css',
		'css/owl.theme.css',
		'css/fonts.css',
		'css/font-awesome.min.css',
    ];
    public $js = [
	    'js/jquery-1.9.1.min.js',
		'js/bootstrap.min.js',
		'js/owl.carousel.js',
		'js/header.js',
		'js/script.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
