jQuery(document).ready(function() {
	
	jQuery("#owl-demo").owlCarousel({
		autoPlay: 3000,
		items : 1,
		navigation: true,
		itemsDesktop : [1199,1],
		itemsDesktopSmall : [979,1]
	});
	
	jQuery("#owl-demo-2").owlCarousel({
		autoPlay: 3000,
		items : 4,
		navigation: true,
		itemsDesktop : [1199,4],
		itemsDesktopSmall : [979,4]
	});
	
	jQuery('.carousel').carousel({
        interval: 5000 //changes the speed
    });
	jQuery("#owl-demo-3").owlCarousel({
		autoPlay: 3000,
		items : 6,
		navigation: true,
		itemsDesktop : [1199,6],
		itemsDesktopSmall : [979,6]
	});
	
	jQuery("#owl-demo-5").owlCarousel({
		autoPlay: 3000,
		items : 4,
		navigation: true,
		itemsDesktop : [1199,4],
		itemsDesktopSmall : [979,4]
	});
	
	jQuery("#owl-demo-6").owlCarousel({
		autoPlay: 3000,
		items : 2,
		navigation: true,
		itemsDesktop : [1199,2],
		itemsDesktopSmall : [979,2]
	});
	
	

	
	
	
	
	
	
	jQuery('.carousel').carousel({
        interval: 5000 //changes the speed
    });
});


//Animation 
(function($) {
	
  $.fn.visible = function(partial) {
      var $t            = $(this),
          $w            = $(window),
          viewTop       = $w.scrollTop(),
          viewBottom    = viewTop + $w.height(),
          _top          = $t.offset().top,
          _bottom       = _top + $t.height(),
          compareTop    = partial === true ? _bottom : _top,
          compareBottom = partial === true ? _top : _bottom; 
    return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
  };
    
})(jQuery);

var win = $(window);
var allMods = $(".anim");
allMods.each(function(i, el) {
  var el = $(el);
  if (el.visible(true)) {
    el.addClass("already-animated"); 
  } 
});

win.scroll(function(event) {
  allMods.each(function(i, el) {
    var el = $(el);
    if (el.visible(true)) {
      el.addClass("animated"); 
    } 
  });
});

//Animationn Ends//

 //For Portfolio Bar
	 $(".filter-button").click(function(){
        var value = $(this).attr('data-filter');
        if(value == "all")
        {		 
            $('.filter').show('1000');
        }
        else
        {
            $(".filter").not('.'+value).hide('3000');
            $('.filter').filter('.'+value).show('3000');  
        }
    });
    
    $('.filter-button').click(function() {
      $(this).addClass('activ').siblings().removeClass('activ');
	});

	
	
