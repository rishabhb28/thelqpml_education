$(function() {
    /**For Portfolio Bar*****/
		$(".filter-button").click(function(){
		if ($(".filter-button").removeClass("activ")) {
		$(this).removeClass("activ");
		}
		$(this).addClass("activ");
		var value = $(this).attr('data-filter');
		if(value == "all")
		{		 
		$('.filter').show('1000');
		}
		else
		{
		$(".filter").not('.'+value).hide('3000');
		$('.filter').filter('.'+value).show('3000');  
		}
    });
});
		//Animation of why choose us home services
	//jQuery("#why-choose").mouseover(function(){
	//	jQuery(".anim").addClass("animated bounceInUp");
	//});
// Test
(function($) {

  /**
   * Copyright 2012, Digital Fusion
   * Licensed under the MIT license.
   * http://teamdf.com/jquery-plugins/license/
   *
   * @author Sam Sehnert
   * @desc A small plugin that checks whether elements are within
   *     the user visible viewport of a web browser.
   *     only accounts for vertical position, not horizontal.
   */

  $.fn.visible = function(partial) {
      var $t            = $(this),
          $w            = $(window),
          viewTop       = $w.scrollTop(),
          viewBottom    = viewTop + $w.height(),
          _top          = $t.offset().top,
          _bottom       = _top + $t.height(),
          compareTop    = partial === true ? _bottom : _top,
          compareBottom = partial === true ? _top : _bottom; 
    return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
  };
    
})(jQuery);

var win = $(window);
var allMods = $(".anim");
allMods.each(function(i, el) {
  var el = $(el);
  if (el.visible(true)) {
    el.addClass("already-animated"); 
  } 
});

win.scroll(function(event) {
  allMods.each(function(i, el) {
    var el = $(el);
    if (el.visible(true)) {
      el.addClass("animated"); 
    } 
  });
});
