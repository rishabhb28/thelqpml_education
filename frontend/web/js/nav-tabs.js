$(document).ready(function () { 
    $('#navi').children('li').first().children('a').addClass('active')
        .next().addClass('is-open').show();
        
    $('#navi').on('click', 'li > a', function() {
        
      if (!$(this).hasClass('active')) {

        $('#navi .is-open').removeClass('is-open').hide();
        $(this).next().toggleClass('is-open').toggle();
          
        $('#navi').find('.active').removeClass('active');
        $(this).addClass('active');
      } else {
        $('#navi .is-open').removeClass('is-open').hide();
        $(this).removeClass('active');
      }
   });
});
