<?php

namespace app\modules\schoolinfo\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\schoolinfo\models\ErpSchoolInformation;

/**
 * ErpSchoolInformationSearch represents the model behind the search form of `app\modules\schoolinfo\models\ErpSchoolInformation`.
 */
class ErpSchoolInformationSearch extends ErpSchoolInformation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_id', 'school_country', 'school_state', 'school_dist', 'school_post_code'], 'integer'],
            [['school_uuid', 'school_main_uuid', 'school_reg_number', 'school_name', 'school_email', 'school_phone', 'school_board', 'school_is_deleted', 'school_status', 'school_created_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ErpSchoolInformation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'school_id' => $this->school_id,
            'school_country' => $this->school_country,
            'school_state' => $this->school_state,
            'school_dist' => $this->school_dist,
            'school_post_code' => $this->school_post_code,
            'school_created_date' => $this->school_created_date,
        ]);

        $query->andFilterWhere(['like', 'school_uuid', $this->school_uuid])
            ->andFilterWhere(['like', 'school_main_uuid', $this->school_main_uuid])
            ->andFilterWhere(['like', 'school_reg_number', $this->school_reg_number])
            ->andFilterWhere(['like', 'school_name', $this->school_name])
            ->andFilterWhere(['like', 'school_email', $this->school_email])
            ->andFilterWhere(['like', 'school_phone', $this->school_phone])
            ->andFilterWhere(['like', 'school_board', $this->school_board])
            ->andFilterWhere(['like', 'school_is_deleted', $this->school_is_deleted])
            ->andFilterWhere(['like', 'school_status', $this->school_status]);

        return $dataProvider;
    }
}
