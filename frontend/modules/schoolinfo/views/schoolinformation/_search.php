<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolinfo\models\ErpSchoolInformationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="container">
  <div class="site-about">
    <div class="section-heading-2">
      <h2>
        <?= Html::encode($this->title) ?>
      </h2>
    </div>
    <div class="text-wrap">
      <div class="line">
		  <?php $form = ActiveForm::begin([
              'action' => ['index'],
              'method' => 'get',
          ]); ?>
      
          <?= $form->field($model, 'school_id') ?>
      
          <?= $form->field($model, 'school_uuid') ?>
      
          <?= $form->field($model, 'school_main_uuid') ?>
      
          <?= $form->field($model, 'school_reg_number') ?>
      
          <?= $form->field($model, 'school_name') ?>
      
          <?php // echo $form->field($model, 'school_email') ?>
      
          <?php // echo $form->field($model, 'school_phone') ?>
      
          <?php // echo $form->field($model, 'school_country') ?>
      
          <?php // echo $form->field($model, 'school_state') ?>
      
          <?php // echo $form->field($model, 'school_dist') ?>
      
          <?php // echo $form->field($model, 'school_post_code') ?>
      
          <?php // echo $form->field($model, 'school_board') ?>
      
          <?php // echo $form->field($model, 'school_is_deleted') ?>
      
          <?php // echo $form->field($model, 'school_status') ?>
      
          <?php // echo $form->field($model, 'school_created_date') ?>
      
          <div class="form-group">
              <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
              <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
          </div>
      
          <?php ActiveForm::end(); ?>
      </div>
    </div>
  </div>
</div>
