<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolinfo\models\ErpSchoolInformation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="erp-school-information-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'school_reg_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'school_name')->textInput(['maxlength' => true]) ?>
    
    <?php if($action != 'update') { ?>
 
    <?= $form->field($signup, 'username')->textInput(['autofocus' => true]) ?>
    
    <?= $form->field($signup, 'email')->textInput(['autofocus' => true,'type' => 'email']) ?>
    
     <?= $form->field($signup, 'password')->passwordInput() ?>
    
    <?php } else  { ?>
    
    <?= $form->field($signup, 'username')->textInput(['autofocus' => true,'readonly' => true]) ?>
    
    <?= $form->field($signup, 'email')->textInput(['autofocus' => true,'readonly' => true,'type' => 'email']) ?>
    
    <?php } ?>
   
    <?= $form->field($model, 'school_phone')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'school_state')->dropDownList([$getAllstates]) ?>
    
    <?= $form->field($model, 'school_dist')->dropDownList([$getAllcities],['prompt' => 'Choose City','id'=>'state']) ?>

    <?= $form->field($model, 'school_post_code')->textInput() ?>

    <?= $form->field($model, 'school_board')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
