<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolinfo\models\ErpSchoolInformation */

$this->title = $model->school_id;
$this->params['breadcrumbs'][] = ['label' => 'Erp School Informations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
  <div class="site-about">
    <div class="section-heading-2">
      <h2>
        <?= Html::encode($this->title) ?>
      </h2>
      <p>
        <?= Html::a('Update', ['update', 'id' => $model->school_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->school_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    </div>
    <div class="text-wrap">
      <div class="line">
		  <?= DetailView::widget([
              'model' => $model,
              'attributes' => [
                  'school_id',
                  //'school_uuid',
                  //'school_main_uuid',
                  'school_reg_number',
                  'school_name',
                  'school_email:email',
                  'school_phone',
                  'school_country',
                  'school_state',
                  'school_dist',
                  'school_post_code',
                  'school_board',
                  'school_is_deleted',
                  'school_status',
                  'school_created_date',
              ],
          ]) ?>
      </div>
    </div>
  </div>
</div>

