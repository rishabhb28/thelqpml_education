<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolinfo\models\ErpSchoolInformationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Erp School Informations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
  <div class="site-about">
    <div class="section-heading-2">
      <h2>
        <?= Html::encode($this->title) ?>
      </h2>
     <p>
        <?= Html::a('Create School', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    </div>
    <div class="text-wrap">
      <div class="line">
		  <?= GridView::widget([
              'dataProvider' => $dataProvider,
              'filterModel' => $searchModel,
              'columns' => [
                  ['class' => 'yii\grid\SerialColumn'],
      
                  //'school_id',
                  //'school_uuid',
                  //'school_main_uuid',
                  'school_reg_number',
                  'school_name',
                  'school_email:email',
                  'school_phone',
                  //'school_country',
                  //'school_state',
                  //'school_dist',
                  //'school_post_code',
                  //'school_board',
                  'school_is_deleted',
                  'school_status',
                  //'school_created_date',
      
                  ['class' => 'yii\grid\ActionColumn'],
              ],
          ]); ?>
      </div>
    </div>
  </div>
</div>

