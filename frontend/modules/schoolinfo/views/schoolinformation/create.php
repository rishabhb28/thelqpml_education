<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\schoolinfo\models\ErpSchoolInformation */

$this->title = 'Create Erp School Information';
$this->params['breadcrumbs'][] = ['label' => 'Erp School Informations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
  <div class="site-about">
    <div class="section-heading-2">
      <h2>
        <?= Html::encode($this->title) ?>
      </h2>
    </div>
    <div class="text-wrap">
      <div class="line">
		  <?= $this->render('_form', [
              'model' => $model,
			  'signup' => $signup,
			  'getAllcities' => $getAllcities,
			  'getAllstates' => $getAllstates,
'action' => $action,
          ]) ?>
      </div>
    </div>
  </div>
</div>
