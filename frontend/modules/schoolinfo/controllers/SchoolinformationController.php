<?php

namespace app\modules\schoolinfo\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\User;
use app\modules\schoolinfo\models\ErpSchoolInformation;
use app\modules\schoolinfo\models\ErpSchoolInformationSearch;
use frontend\models\SignupForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\models\States;
use app\models\Cities;
/**
 * SchoolinformationController implements the CRUD actions for ErpSchoolInformation model.
 */
class SchoolinformationController extends Controller
{
	 public $layout='maininner';
    /**
     * @inheritdoc
     */
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
	public function behaviors()
		{
			return [
				'verbs' => [
					'class' => VerbFilter::className(),
					'actions' => [
						//'delete' => ['post'],
					],
				],
				'access' => [
							'class' => \yii\filters\AccessControl::className(),
							'only' => ['index','create','update','view','delete'],
							'rules' => [
								// allow authenticated users
								[
									'allow' => true,
									'roles' => ['@'],
								],
								// everything else is denied
							],
						],            
			];
		}

    /**
     * Lists all ErpSchoolInformation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ErpSchoolInformationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ErpSchoolInformation model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ErpSchoolInformation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
	public function actionCreate()
    {
		
$action = 'create';
		$signup = new SignupForm();
		
		$model = new ErpSchoolInformation();
		
		$getAllstates = ArrayHelper::map(States::find()->where(array('country_id' => '101','id' => 13))->all(),'id', 'name');
		
		$getAllcities = ArrayHelper::map(Cities::find()->where(array('state_id' => '13'))->all(),'id', 'name');
		
	    $moduleName = "SignupForm"; //Module Name        

		$tableName = "SignupForm"; //Table Class Nam

		$user_uuid = Yii::$app->utility->generateGuuid($moduleName, $tableName); //Generate UUID
		
		$signup->user_uuid = $user_uuid;
		
		//$signup->role = "2";
		
	    $signup->created_at = date("Y-m-d h:i:s");

		$signup->updated_at = date("Y-m-d h:i:s");
		
		if ($signup->load(Yii::$app->request->post())) {

            if ($user = $signup->signup()) {
				
			    $moduleName = "ErpSchoolInformation"; //Module Name        

				$tableName = "ErpSchoolInformation"; //Table Class Nam
		
				$school_uuid = Yii::$app->utility->generateGuuid($moduleName, $tableName); //Generate UUID
				
				$model->school_uuid = $school_uuid;
				
				$model->school_main_uuid = $user->user_uuid;
				
				$model->school_email = $signup->email;
				
				$model->school_created_date = date("Y-m-d h:i:s");
				
				$model->attributes = $_POST['ErpSchoolInformation'];

                 if($model->save()) {
					 
					Yii::$app->getSession()->setFlash('success', 'Teacher Infromation Added');
					  
					 return $this->redirect(['schoolinformation/index']);
					  
				  } else {
					  
					  Yii::$app->getSession()->setFlash('danger', 'Internal Server Error');
					  
				  } 
            } else {
				
				    Yii::$app->getSession()->setFlash('danger', 'Internal Server Error');
				
			}

        }

        return $this->render('create', [
            'model' => $model,
			'getAllstates' => $getAllstates,
			'getAllcities' => $getAllcities,
			'signup' => $signup,
'action' => $action
        ]);
    }


    /**
     * Updates an existing ErpSchoolInformation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
		$getAllstates = ArrayHelper::map(States::find()->where(array('country_id' => '101','id' => 13))->all(),'id', 'name');
		
		$getAllcities = ArrayHelper::map(Cities::find()->where(array('state_id' => '13'))->all(),'id', 'name');
		
        $model = $this->findModel($id);
		
		$signup = User::find()->where(['user_uuid' => $model->school_main_uuid])->one(); 
		
		$action = 'update';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->school_id]);
        }

        return $this->render('update', [
            'model' => $model,
			'getAllstates' => $getAllstates,
			'getAllcities' => $getAllcities,
			'signup' => $signup,
			'action' => $action,
        ]);
    }

    /**
     * Deletes an existing ErpSchoolInformation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        
		$model = ErpSchoolInformation::find()->where(['school_id' => $id])->one();
		
		$model->school_is_deleted = 'YES';
        
		$model->save();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ErpSchoolInformation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ErpSchoolInformation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ErpSchoolInformation::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
