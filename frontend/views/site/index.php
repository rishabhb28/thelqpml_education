<?php

/* @var $this yii\web\View */

$this->title = 'Securing Next Generation';
?>
<section id="home-banner-2">
	<div class="container">
		<div class="text-wrap">
			<h1>we never dreamed about success we worked for it</h1>
			<p>We cover a range of industries as diverse as these...</p>
			<a href="" class="angle-btn"><span>read more</span></a>
		</div>
	</div>
</section>
<section id="home-spcial-2" class="section-2">
	<div class="container">
		<div class="section-heading-2">
			<h2><span class="light">The</span> Corporate Consulting Edge</h2>
			<p>we never dreamed about success we worked for it</p>
		</div>
		
		<div class="row special-list">
			<div class="col-md-4">
				<div class="media">
					<div class="media-left">
						<div class="img-wrap">
							<i class="fa fa-globe" aria-hidden="true"></i>
						</div>
					</div>
					<div class="media-body">
						<h4 class="media-heading">Energy</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sit amet lectus quis justo efficitur elementum.</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="media">
					<div class="media-left">
						<div class="img-wrap">
							<i class="fa fa-globe" aria-hidden="true"></i>
						</div>
					</div>
					<div class="media-body">
						<h4 class="media-heading">Energy</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sit amet lectus quis justo efficitur elementum.</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="media">
					<div class="media-left">
						<div class="img-wrap">
							<i class="fa fa-globe" aria-hidden="true"></i>
						</div>
					</div>
					<div class="media-body">
						<h4 class="media-heading">Energy</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sit amet lectus quis justo efficitur elementum.</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="media">
					<div class="media-left">
						<div class="img-wrap">
							<i class="fa fa-globe" aria-hidden="true"></i>
						</div>
					</div>
					<div class="media-body">
						<h4 class="media-heading">Energy</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sit amet lectus quis justo efficitur elementum.</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="media">
					<div class="media-left">
						<div class="img-wrap">
							<i class="fa fa-globe" aria-hidden="true"></i>
						</div>
					</div>
					<div class="media-body">
						<h4 class="media-heading">Energy</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sit amet lectus quis justo efficitur elementum.</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="media">
					<div class="media-left">
						<div class="img-wrap">
							<i class="fa fa-globe" aria-hidden="true"></i>
						</div>
					</div>
					<div class="media-body">
						<h4 class="media-heading">Energy</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sit amet lectus quis justo efficitur elementum.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="business" class="section-2 dark">
	<div class="container">
		<div class="section-heading-2">
			<h2><span class="light">The</span> Corporate Consulting Edge</h2>
		</div>
		<div class="buiz-text row">
			<div class="col-md-10">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sit amet lectus quis justo efficitur elementum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sit amet lectus quis justo efficitur elementum.</p>
			</div>
			<div class="col-md-2">
				<a href="" class="angle-btn"><span>Read More</span></a>
			</div>
		</div>
	</div>
</section>
<section id="empower" class="section-2">
	<div class="container">
		<div class="row">
			<div class="col-md-4 texts">
				<h2>Consultancy<span class="light"> that empowers you </span></h2>
				<div class="text-wrap">
					<div class="line">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sit amet lectus quis justo efficitur elementum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sit amet lectus quis justo efficitur elementum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sit amet lectus quis justo efficitur elementum.</p>
					<a href="" class="angle-btn"><span>Read More</span></a>
					</div>
				</div>
			</div>
			<div class="col-md-8 images">
				<ul class="row">
					<li class="col-md-6">
						<div class="img-wrap">
							<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/image1.jpg" class="img-responsive"/>
						</div>
					</li>
					<li class="col-md-6">
						<div class="img-wrap">
							<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/image1.jpg" class="img-responsive"/>
						</div>
					</li>
					<li class="col-md-6">
						<div class="img-wrap">
							<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/image1.jpg" class="img-responsive"/>
						</div>
					</li>
					<li class="col-md-6">
						<div class="img-wrap">
							<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/image1.jpg" class="img-responsive"/>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section id="team-2" class="section-2 dark">
	<div class="container">
		<div class="section-heading-2">
			<h2><span class="light">meet</span> our team</h2>
			<p>The best of best is here</p>
		</div>		
		<div id="owl-demo-5" class="owl-carousel">
			<div class="item">
				<div class="team-wrap">
					<div class="img-wrap">
						<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/team1.jpg" class="img-responsive"/>
					</div>
					<div class="text-wrap text-center">
						<h1>John Doe</h1>
						<span>CEO</span>
					</div>
				</div>
			</div>
			<div class="item">
				<div class="team-wrap">
					<div class="img-wrap">
						<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/team1.jpg" class="img-responsive"/>
					</div>
					<div class="text-wrap text-center">
						<h1>John Doe</h1>
						<span>CEO</span>
					</div>
				</div>
			</div>
			<div class="item">
				<div class="team-wrap">
					<div class="img-wrap">
						<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/team1.jpg" class="img-responsive"/>
					</div>
					<div class="text-wrap text-center">
						<h1>John Doe</h1>
						<span>CEO</span>
					</div>
				</div>
			</div>
			<div class="item">
				<div class="team-wrap">
					<div class="img-wrap">
						<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/team1.jpg" class="img-responsive"/>
					</div>
					<div class="text-wrap text-center">
						<h1>John Doe</h1>
						<span>CEO</span>
					</div>
				</div>
			</div>
			<div class="item">
				<div class="team-wrap">
					<div class="img-wrap">
						<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/team1.jpg" class="img-responsive"/>
					</div>
					<div class="text-wrap text-center">
						<h1>John Doe</h1>
						<span>CEO</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="broad-range" class="section-2">
	<div class="container text-center">
		<div class="section-heading-2">
			<h2><span class="light">Our </span>Broad Range <span class="light">of Service</span></h2>
		</div>
		<div class="broad-content">
			<p class="main-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sit amet lectus quis justo efficitur elementum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sit amet lectus quis justo efficitur elementum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sit amet lectus quis justo efficitur elementum.</p>
			<p class="name">- John Doe -</p>
			<span>(CEO)</span>
		</div>
	</div>
	<div class="container-fluid broad-list">
		<div class="row no-mar">
			<div class="col-md-4 no-p">
				<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/broad1.jpg" class="img-responsive">
			</div>
			<div class="col-md-4 no-p">
				<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/broad2.jpg" class="img-responsive">
			</div>
			<div class="col-md-4 no-p">
				<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/broad1.jpg" class="img-responsive">
			</div>
			
		</div>
	</div>
</section>
<section id="client" class="section-2">
	<div class="container">
		<div class="section-heading-2">
			<h2><span class="light">Words</span> from our clients</h2>
			<p>The best of best is here</p>
		</div>		
		<div id="owl-demo-6" class="owl-carousel">
			<div class="item">
				<div class="testi">
					<div class="text-wrap">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis dignissim, orci non porta convallis, felis dui fringilla lectus, sit amet lacinia diam augue et ante. Suspendisse fermentum varius libero, sit amet hendrerit diam gravida sit amet. Quisque sed turpis in est molestie varius nec sed purus.</p>
						
					</div>
					<div class="img-wrap">
						<div class="media">
							<div class="media-left">
								<div class="img-wrap">
									<img class="img-responsive" src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/team1.jpg"/>
								</div>
								
							</div>
							<div class="media-body">
								<span class="title">Russel White</span>
								<span class="designation">- Wyeth Inc</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="item">
				<div class="testi">
					<div class="text-wrap">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis dignissim, orci non porta convallis, felis dui fringilla lectus, sit amet lacinia diam augue et ante. Suspendisse fermentum varius libero, sit amet hendrerit diam gravida sit amet. Quisque sed turpis in est molestie varius nec sed purus.</p>
						
					</div>
					<div class="img-wrap">
						<div class="media">
							<div class="media-left">
								<div class="img-wrap">
									<img class="img-responsive" src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/team1.jpg"/>
								</div>
								
							</div>
							<div class="media-body">
								<span class="title">Russel White</span>
								<span class="designation">- Wyeth Inc</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="item">
				<div class="testi">
					<div class="text-wrap">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis dignissim, orci non porta convallis, felis dui fringilla lectus, sit amet lacinia diam augue et ante. Suspendisse fermentum varius libero, sit amet hendrerit diam gravida sit amet. Quisque sed turpis in est molestie varius nec sed purus.</p>
						
					</div>
					<div class="img-wrap">
						<div class="media">
							<div class="media-left">
								<div class="img-wrap">
									<img class="img-responsive" src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/team1.jpg"/>
								</div>
								
							</div>
							<div class="media-body">
								<span class="title">Russel White</span>
								<span class="designation">- Wyeth Inc</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
