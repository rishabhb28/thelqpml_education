<?php

/* @var $this yii\web\View */

$this->title = 'Our Services';
?>
<section id="home-banner">
	<div class="container">
		<div class="text-wrap text-right">
			<div class="headinb-wrap"><h1>Best Education</h1></div>
			<div class="sub-heaindg"><h4>for your better future</h4></div>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sollicitudin sem orci, in ultrices leo vulputate malesuada.</p>
			<a href="" class="banner-btn">Apply Now</a>
		</div>
	</div>
</section>
<section id="why-choose" class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<div class="img-wrap">
					<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/why.jpg" class="img-responsive">
				</div>
			</div>
			<div class="col-md-7">
				<div class="text-wrap">
					<div class="section-heading">
						<h2>Why <span class="orange">Choose</span> Us?</h2>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="media why-list">
								<div class="media-left">
									<i class="fa fa-bar-chart" aria-hidden="true"></i>
								</div>
								<div class="media-body">
									<h4 class="media-heading">Popular Courses</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sollicitudin sem orci, in ultrices leo vulputate malesuada.</p>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="media why-list">
								<div class="media-left">
									<i class="fa fa-bar-chart" aria-hidden="true"></i>
								</div>
								<div class="media-body">
									<h4 class="media-heading">Popular Courses</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sollicitudin sem orci, in ultrices leo vulputate malesuada.</p>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="media why-list">
								<div class="media-left">
									<i class="fa fa-bar-chart" aria-hidden="true"></i>
								</div>
								<div class="media-body">
									<h4 class="media-heading">Popular Courses</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sollicitudin sem orci, in ultrices leo vulputate malesuada.</p>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="media why-list">
								<div class="media-left">
									<i class="fa fa-bar-chart" aria-hidden="true"></i>
								</div>
								<div class="media-body">
									<h4 class="media-heading">Popular Courses</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sollicitudin sem orci, in ultrices leo vulputate malesuada.</p>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="media why-list">
								<div class="media-left">
									<i class="fa fa-bar-chart" aria-hidden="true"></i>
								</div>
								<div class="media-body">
									<h4 class="media-heading">Popular Courses</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sollicitudin sem orci, in ultrices leo vulputate malesuada.</p>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="media why-list">
								<div class="media-left">
									<i class="fa fa-bar-chart" aria-hidden="true"></i>
								</div>
								<div class="media-body">
									<h4 class="media-heading">Popular Courses</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sollicitudin sem orci, in ultrices leo vulputate malesuada.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="testimonial" class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="testimonial-wrap">
					<div class="section-heading">
						<h2>What <span class="orange">People</span> Say</h2>
						<p>Lorem Ipsum Dolor et</p>
					</div>
					<div id="owl-demo" class="owl-carousel">
						<div class="item">
							<div class="media">
								<div class="media-left">
									<div class="img-wrap">
										<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/team1.jpg" class="media-object">
									</div>
								</div>
								<div class="media-body">
									<div class="text-wrap">
										<p class="main-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin quis ante nulla. Maecenas efficitur felis risus, nec dapibus ipsum placerat nec. Quisque dolor dui, dignissim quis odio eu, finibus posuere ligula.</p>
										<h6>John Doe</h6>
										<span>Deputy Minister of Defence</span>
									</div>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="media">
								<div class="media-left">
									<div class="img-wrap">
										<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/team1.jpg" class="media-object">
									</div>
								</div>
								<div class="media-body">
									<div class="text-wrap">
										<p class="main-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin quis ante nulla. Maecenas efficitur felis risus, nec dapibus ipsum placerat nec. Quisque dolor dui, dignissim quis odio eu, finibus posuere ligula.</p>
										<h6>John Doe</h6>
										<span>Deputy Minister of Defence</span>
									</div>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="media">
								<div class="media-left">
									<div class="img-wrap">
										<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/team1.jpg" class="media-object">
									</div>
								</div>
								<div class="media-body">
									<div class="text-wrap">
										<p class="main-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin quis ante nulla. Maecenas efficitur felis risus, nec dapibus ipsum placerat nec. Quisque dolor dui, dignissim quis odio eu, finibus posuere ligula.</p>
										<h6>John Doe</h6>
										<span>Deputy Minister of Defence</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-area">
					<form class="row">
						<div class="col-md-12 form-line">
							<div class="form-group">
								<input type="text" class="form-control" id="exampleInputEmail" placeholder="Enter Name">
							</div>	
						</div>
						<div class="col-md-6 form-line">
							<div class="form-group">
								<input type="email" class="form-control" id="exampleInputEmail" placeholder="Email">
							</div>	
						</div>
						<div class="col-md-6 form-line">
							<div class="form-group">
								<input type="text" class="form-control" id="exampleInputEmail" placeholder="Phone">
							</div>	
						</div>
						<div class="col-md-6 form-line">
							<div class="form-group">
								<select class="form-control" >
									<option value="volvo">Select Subject</option>
									<option value="saab">A</option>
								</select> 
							</div>	
						</div>
						<div class="col-md-6 form-line">
							<div class="form-group">
								<input type="text" class="form-control" id="exampleInputEmail" placeholder="Date Of Birth">
							</div>	
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<textarea  class="form-control" id="description" placeholder="Enter Your Message"></textarea>
							</div>
							<div>
								<button type="button" class="banner-btn">Send Message</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="section" id="upcoming">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="events">
					<div class="section-heading">
						<h2>Upcoming <span class="orange">Events</span></h2>
					</div>
					
					<ul class="event-list">
						<li class="event-individual">
							<div class="post-date">
								<span>12 Feb</span>
							</div>
							<div class="posts-wrap pull-right">
								<a href="" class="posts-title">Admission Open spring 2017</a>
								<ul class="post-mata list-inline">
									<li class="date"><i class="fa fa-clock-o" aria-hidden="true"></i><span> 07:00am - 09:00pm</span></li>
									<li class="date"><i class="fa fa-map-marker" aria-hidden="true"></i><span> 07:00am - 09:00pm</span></li>
								</ul>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sollicitudin</p>
							</div>
						</li>
						<li class="event-individual">
							<div class="post-date">
								<span>12 Feb</span>
							</div>
							<div class="posts-wrap pull-right">
								<a href="" class="posts-title">Admission Open spring 2017</a>
								<ul class="post-mata list-inline">
									<li class="date"><i class="fa fa-clock-o" aria-hidden="true"></i><span> 07:00am - 09:00pm</span></li>
									<li class="date"><i class="fa fa-map-marker" aria-hidden="true"></i><span> 07:00am - 09:00pm</span></li>
								</ul>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sollicitudin</p>
							</div>
						</li>
						<li class="event-individual">
							<div class="post-date">
								<span>12 Feb</span>
							</div>
							<div class="posts-wrap pull-right">
								<a href="" class="posts-title">Admission Open spring 2017</a>
								<ul class="post-mata list-inline">
									<li class="date"><i class="fa fa-clock-o" aria-hidden="true"></i><span> 07:00am - 09:00pm</span></li>
									<li class="date"><i class="fa fa-map-marker" aria-hidden="true"></i><span> 07:00am - 09:00pm</span></li>
								</ul>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sollicitudin </p>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-md-6">
				<div class="faq-wrap">	
					<div class="section-heading">
						<h2>Frequently <span class="orange">Asked </span>Question</h2>
					</div>
					
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">		
						<!-- panel 1 -->
						<div class="panel panel-default">
							<!--wrap panel heading in span to trigger image change as well as collapse -->
							<span class="side-tab" data-target="#tab1" data-toggle="tab" role="tab" aria-expanded="false">
								<div class="panel-heading" role="tab" id="headingOne"data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
									<h4 class="panel-title">Why we are best</h4>
								</div>
							</span>
							
							<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
								<!-- Tab content goes here -->
								That fall, as Nadia and Masha got shipped off to prison camps in Siberia, South Brooklyn tried to recover from the storm. My dad and I spent a lot of time in the same apartment engrossed in separate laptops, separate internet missives. He followed Russian news bloggers closely and would update me on troubling developments. A rise in protofascist nationalism
								</div>
							</div>
						</div> 
						<!-- / panel 1 -->
						
						<!-- panel 2 -->
						<div class="panel panel-default">
							<!--wrap panel heading in span to trigger image change as well as collapse -->
							<span class="side-tab" data-target="#tab2" data-toggle="tab" role="tab" aria-expanded="false">
								<div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
									<h4 class="panel-title collapsed">TAB 2</h4>
								</div>
							</span>

							<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
								<div class="panel-body">
								<!-- Tab content goes here -->
								tab 2 content
								</div>
							</div>
						</div>
						<!-- / panel 2 -->
						
						<!--  panel 3 -->
						<div class="panel panel-default">
							<!--wrap panel heading in span to trigger image change as well as collapse -->
							<span class="side-tab" data-target="#tab3" data-toggle="tab" role="tab" aria-expanded="false">
								<div class="panel-heading" role="tab" id="headingThree"  class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
									<h4 class="panel-title">TAB 3 </h4>
								</div>
							</span>
							<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
							  <div class="panel-body">
							  <!-- tab content goes here -->
							   tab 3 content
							  </div>
							</div>
						  </div>
					</div> <!-- / panel-group -->
					
					
				</div>
			</div>
		</div>
	</div>
</section>
<section id="team" class="section">
	<div class="container">
		<div class="section-heading">
			<h2>Qualified <span class="orange">teachers</span> </h2>
			<p>Lorem Ipsum Dolor et</p>
		</div>
		<div id="owl-demo-2" class="owl-carousel">
			<div class="item">
				<div class="team-wrap">
					<div class="img-wrap">
						<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/team1.jpg" class="img-responsive"/>
					</div>
					<div class="text-wrap text-center">
						<h1>John Doe</h1>
						<span>CEO</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sollicitudin auctor eros, at efficitur justo pharetra non. Phasellus tempus tincidunt justo, a fermentum mauris suscipit sed.</p>
					</div>
					<ul class="social-list">
						<li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
						<li><a href=""><i class="fa fa-skype" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="item">
				<div class="team-wrap">
					<div class="img-wrap">
						<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/team1.jpg" class="img-responsive"/>
					</div>
					<div class="text-wrap text-center">
						<h1>John Doe</h1>
						<span>CEO</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sollicitudin auctor eros, at efficitur justo pharetra non. Phasellus tempus tincidunt justo, a fermentum mauris suscipit sed.</p>
					</div>
					<ul class="social-list">
						<li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
						<li><a href=""><i class="fa fa-skype" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="item">
				<div class="team-wrap">
					<div class="img-wrap">
						<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/team1.jpg" class="img-responsive"/>
					</div>
					<div class="text-wrap text-center">
						<h1>John Doe</h1>
						<span>CEO</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sollicitudin auctor eros, at efficitur justo pharetra non. Phasellus tempus tincidunt justo, a fermentum mauris suscipit sed.</p>
					</div>
					<ul class="social-list">
						<li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
						<li><a href=""><i class="fa fa-skype" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="item">
				<div class="team-wrap">
					<div class="img-wrap">
						<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/team1.jpg" class="img-responsive"/>
					</div>
					<div class="text-wrap text-center">
						<h1>John Doe</h1>
						<span>CEO</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sollicitudin auctor eros, at efficitur justo pharetra non. Phasellus tempus tincidunt justo, a fermentum mauris suscipit sed.</p>
					</div>
					<ul class="social-list">
						<li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
						<li><a href=""><i class="fa fa-skype" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="item">
				<div class="team-wrap">
					<div class="img-wrap">
						<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/team1.jpg" class="img-responsive"/>
					</div>
					<div class="text-wrap text-center">
						<h1>John Doe</h1>
						<span>CEO</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sollicitudin auctor eros, at efficitur justo pharetra non. Phasellus tempus tincidunt justo, a fermentum mauris suscipit sed.</p>
					</div>
					<ul class="social-list">
						<li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
						<li><a href=""><i class="fa fa-skype" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</div>
			
		</div>
	</div>
</section>
<section id="blog" class="section">
	<div class="container">
		<div class="section-heading">
			<h2>Latest <span class="orange">News</span></h2>
			<p>Lorem Ipsum Dolor et</p>
		</div>
		<div class="row blog-list">
			<div class="col-md-4">
				<div class="blog-wrap">
					<div class="img-wrap">
						<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/broad2.jpg" class="img-responsive">
					</div>
					<div class="text-wrap">
						<a href="" class="post-title">Post Title Here lorem Ipsum</a>
						<ul class="post-mata list-inline">
							<li class="by">By Admin</li>	
							<li class="on">12 Dec 2017</li>	
						</ul>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sollicitudin auctor eros, at efficitur justo pharetra non. Phasellus tempus tincidunt justo, a fermentum mauris suscipit sed.</p>
						<a href="" class="banner-btn">Read More</a>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="blog-wrap">
					<div class="img-wrap">
						<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/broad1.jpg" class="img-responsive">
					</div>
					<div class="text-wrap">
						<a href="" class="post-title">Post Title Here lorem Ipsum</a>
						<ul class="post-mata list-inline">
							<li class="by">By Admin</li>	
							<li class="on">12 Dec 2017</li>	
						</ul>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sollicitudin auctor eros, at efficitur justo pharetra non. Phasellus tempus tincidunt justo, a fermentum mauris suscipit sed.</p>
						<a href="" class="banner-btn">Read More</a>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="blog-wrap">
					<div class="img-wrap">
						<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/broad1.jpg" class="img-responsive">
					</div>
					<div class="text-wrap">
						<a href="" class="post-title">Post Title Here lorem Ipsum</a>
						<ul class="post-mata list-inline">
							<li class="by">By Admin</li>	
							<li class="on">12 Dec 2017</li>	
						</ul>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sollicitudin auctor eros, at efficitur justo pharetra non. Phasellus tempus tincidunt justo, a fermentum mauris suscipit sed.</p>
						<a href="" class="banner-btn">Read More</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="section" id="gallery">
	<div class="container">
		
		
		<div class="section-heading">
			<h2>Campus <span class="orange">Gallery</span></h2>
			<p>Lorem Ipsum Dolor et</p>
		</div>
		
		<div class="cats row">
			<div class="nav-buttons col-md-12">
				<button class="btn btn-default filter-button activ" data-filter="all">All</button>
				<button class="btn btn-default filter-button" data-filter="hdpe">Campus</button>
				<button class="btn btn-default filter-button" data-filter="sprinkle">Students</button>
				<button class="btn btn-default filter-button" data-filter="spray">Teachers</button>
				
			</div>
			<div class="gallery_product col-lg-3 col-md-3 col-sm-3 col-xs-6 filter irrigation">
				 <div class="cat-wrap">
					<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/adventure.jpg" class="img-responsive">
				</div>
			</div>

			<div class="gallery_product col-lg-3 col-md-3 col-sm-3 col-xs-6 filter spray">
				 <div class="cat-wrap">
					<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/adventure.jpg" class="img-responsive">
				</div>
			</div>

			<div class="gallery_product col-lg-3 col-md-3 col-sm-3 col-xs-6 filter spray">
				 <div class="cat-wrap">
					<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/adventure.jpg" class="img-responsive">
				</div>
			</div>

			<div class="gallery_product col-lg-3 col-md-3 col-sm-3 col-xs-6 filter sprinkle">
				 <div class="cat-wrap">
					<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/adventure.jpg" class="img-responsive">
				</div>
			</div>

			<div class="gallery_product col-lg-3 col-md-3 col-sm-3 col-xs-6 filter spray">
				 <div class="cat-wrap">
					<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/adventure.jpg" class="img-responsive">
				</div>
			</div>

			<div class="gallery_product col-lg-3 col-md-3 col-sm-3 col-xs-6 filter spray">
				<div class="cat-wrap">
					<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/adventure.jpg" class="img-responsive">
				</div>
			</div>

			<div class="gallery_product col-lg-3 col-md-3 col-sm-3 col-xs-6 filter spray">
				 <div class="cat-wrap">
					<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/adventure.jpg" class="img-responsive">
				</div>
			</div>

			<div class="gallery_product col-lg-3 col-md-3 col-sm-3 col-xs-6 filter hdpe">
				 <div class="cat-wrap">
					<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/adventure.jpg" class="img-responsive">
				</div>
			</div>

			<div class="gallery_product col-lg-3 col-md-3 col-sm-3 col-xs-6 filter hdpe">
				<div class="cat-wrap">
					<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/adventure.jpg" class="img-responsive">
				</div>
			</div>

			<div class="gallery_product col-lg-3 col-md-3 col-sm-3 col-xs-6 filter hdpe">
				<div class="cat-wrap">
					<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/adventure.jpg" class="img-responsive">
				</div>
			</div>

			<div class="gallery_product col-lg-3 col-md-3 col-sm-3 col-xs-6 filter spray">
				<div class="cat-wrap">
					<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/adventure.jpg" class="img-responsive">
				</div>
			</div>

			<div class="gallery_product col-lg-3 col-md-3 col-sm-3 col-xs-6 filter sprinkle">
			   <div class="cat-wrap">
					<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/adventure.jpg" class="img-responsive">
				</div>
			</div>
		</div>
	</div>
</section>
<section id="partners" class="section">
	<div class="container">
		<div id="owl-demo-3" class="owl-carousel">
			<div class="item">
				<div class="img-wrap">
					<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/partner.png" class="img-responsive center-block">
				</div>
			</div>
			<div class="item">
				<div class="img-wrap ">
					<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/partner-2.png" class="img-responsive center-block">
				</div>
			</div>
			<div class="item">
				<div class="img-wrap">
					<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/partner.png" class="img-responsive center-block">
				</div>
			</div>
			<div class="item">
				<div class="img-wrap ">
					<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/partner-2.png" class="img-responsive center-block">
				</div>
			</div>
			<div class="item">
				<div class="img-wrap">
					<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/partner.png" class="img-responsive center-block">
				</div>
			</div>
			<div class="item">
				<div class="img-wrap ">
					<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/partner-2.png" class="img-responsive center-block">
				</div>
			</div>
			<div class="item">
				<div class="img-wrap">
					<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/partner.png" class="img-responsive center-block">
				</div>
			</div>
			<div class="item">
				<div class="img-wrap ">
					<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/partner-2.png" class="img-responsive center-block">
				</div>
			</div>
			<div class="item">
				<div class="img-wrap">
					<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/partner.png" class="img-responsive center-block">
				</div>
			</div>
			<div class="item">
				<div class="img-wrap ">
					<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/partner-2.png" class="img-responsive center-block">
				</div>
			</div>
			
			
			
		</div>
		
	</div>
</section>