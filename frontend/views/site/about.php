<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
  <div class="site-about">
    <div class="section-heading-2">
      <h2>
        <?= Html::encode($this->title) ?>
      </h2>
    </div>
    <div class="text-wrap">
      <div class="line">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sit amet lectus quis justo efficitur elementum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sit amet lectus quis justo efficitur elementum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sit amet lectus quis justo efficitur elementum.</p>
      </div>
    </div>
  </div>
</div>
