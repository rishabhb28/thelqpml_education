<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet" /> 
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<header id="header">
	<div class="top-head">
		<div class="container">
			<div class="col-md-6 menu-contact">
				<ul class="list-inline pull-left">
					<li><a href="">How It Works</a></li>
					<li><a href="">benefits</a></li>
				</ul>
			</div>
			<div class="col-md-6 menu-social">
				<div class="logins pull-right">
                                     <?php if(Yii::$app->user->isGuest){ ?>
					<a href="<?php echo Yii::$app->urlManager->createUrl("/site/login"); ?>" class="login"><i class="fa fa-user" aria-hidden="true"></i><span>Login</span></a>
				     <?php } else { ?>
                                        <a href="<?php echo Yii::$app->urlManager->createUrl("/site/logout"); ?>" class="login"><i class="fa fa-user" aria-hidden="true"></i><span>Logout</span></a>
                                     <?php } ?>
				</div>
				<ul class="list-inline">
					<li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href=""><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
					<li><a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
					<li><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
					<li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="main-head">
		<div class="container">
			<div class="row menu-contact">
				<div class="col-md-offset-3 col-md-9 pull-right">
					<ul class="contact-list">
						<li>
							<p>Call us for more details</p>
							<p>+(012) 345 6789</p>
						</li>
						<li>
							<p>Email us for more details</p>
							<p>email@dummymail.com</p>
						</li>
						<li>
							<p>Our Location</p>
							<p>1211 King Street, ABC City</p>
						</li>
					</ul>
				</div>
			</div>
			<nav class="navbar">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-main-menu">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#"><img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/logo.png" class="img-responsive"></a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div id="navbar-collapse-3" class="collapse navbar-collapse" role="navigation">
					<div class="menu-container">
						<div class="quote pull-right">
						<a href="" class="quote-btn">get a quote</a>
						</div>
						<ul id="menu-main-menu" class="nav navbar-nav navbar-left">
							<li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/products"); ?>">Home</a></li>
							<li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/about"); ?>">About</a></li>
							<li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/products"); ?>">Our Services</a></li>
							<li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/products"); ?>">Our Team</a></li>
							<li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/products"); ?>">Career</a></li>
							<li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/products"); ?>">Blog</a></li>
							<li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/products"); ?>">Get A Quote</a></li>
							<li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/contact"); ?>">Contact Us</a></li>
						</ul>
						
					</div>
					
				</div><!-- /.navbar-collapse -->
			</nav><!-- /.navbar -->
		</div>
	</div>
</header>
<section id="home-spcial-2" class="section-2">

<?= Breadcrumbs::widget([
  'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]) ?>
<?= Alert::widget() ?>
<?= $content ?>

</section>
<footer id="footer">
	<div class="foot-main">
		<div class="container">
			<div class="row">
				<div class="col-md-4 about">
					<div class="about-wrap">
						<img src="<?=Yii::$app->getUrlManager()->getBaseUrl()?>/images/logo-white.png" class="img-responsive"/>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,sed do eiusmod tempor incididunt ut l</p>
						<ul class="">						
							<li class="phone">123-345-22323</li>
							<li class="email">acvgh@admin.com</li>
							<li class="web">www.yourdomain.com</li>
						</ul>
						

					</div>
				</div>
				
				<div class="col-md-4 quick">
					<h4 class="foot-ttl">Quick Links</h4>
					<ul class="">
							<li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/products"); ?>">Home</a></li>
							<li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/about"); ?>">About</a></li>
							<li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/products"); ?>">Our Services</a></li>
							<li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/products"); ?>">Our Team</a></li>
							<li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/products"); ?>">Career</a></li>
							<li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/products"); ?>">Blog</a></li>
							<li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/products"); ?>">Get A Quote</a></li>
							<li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/contact"); ?>">Contact Us</a></li>
					</ul>
				</div>
				<div class="col-md-4 news">
					<h4 class="foot-ttl">Connect With US</h4>
					<form>
						<div class="form-group">
							<input type="text" class="form-control" id="exampleInputEmail" placeholder="Enter Your Mail">
						</div>
						<button type="button" class="banner-btn">Subscribe</button>
					</form>
					<ul class="list-inline social-menu">
						<li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href=""><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
						<li><a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
						<li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
					</ul>
				
				</div>
			</div>
		</div>
	</div>
	<div class="copy">
		<p>&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>
	</div>
</footer>



<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
