<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet" /> 
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<header id="header-2">
	<div class="top-head">
		<div class="container">
			<div class="col-md-6 menu-contact">
				<ul class="list-inline pull-left">
					<li><a href="">FAQs</a></li>
					<li><a href="">Find and Agent</a></li>
					<li><a href="">Contact Us</a></li>
				</ul>
			</div>
			<div class="col-md-6 menu-social">
				<ul class="list-inline pull-right">
					<li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href=""><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
					<li><a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
					<li><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
					<li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="main-head">
		<div class="container">
			<nav class="navbar">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-3">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#"><img src="images/logo.png" class="img-responsive"></a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div id="navbar-collapse-3" class="collapse navbar-collapse" role="navigation">
					<ul id="menu-main-menu" class="nav navbar-nav navbar-right">
                        
						<li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/index"); ?>">Who We Are</a></li>
						<li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/products"); ?>">Our Products</a></li>
						<li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/index"); ?>">Our Partners</a></li>
						<li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/index"); ?>">Our Team</a></li>
						<li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/index"); ?>">Upcoming Events</a></li>
						<li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/index"); ?>">Request For Demo</a></li>
						<li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/index"); ?>">FAQS</a></li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</nav><!-- /.navbar -->
		</div>
		
	</div>
</header>
<section id="home-spcial-2" class="section-2">

<?= Breadcrumbs::widget([
  'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]) ?>
<?= Alert::widget() ?>
<?= $content ?>

</section>
<footer id="footer-2">
	<div class="top-foot">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<div class="about">
						<div class="img-wrap">
							<img src="images/logo-white.png" class="img-responsive">
						</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis dignissim, orci non porta convallis, felis dui fringilla lectus, sit amet lacinia diam augue et ante. Suspendisse fermentum varius libero, sit amet hendrerit diam gravida sit amet.</p>
					</div>
					<div class="social-wrap">
						<ul class="list-inline">
							<li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href=""><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
							<li><a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
							<li><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
							<li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-3">
					<div class="menu-foot">
						<h3>Recent Posts</h3>
						<ul class="list-posts">
							<li class=""><a href=""><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span><b>23rd June 2017</b></a></li>
							<li class=""><a href=""><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span><b>23rd June 2017</b></a></li>
							<li class=""><a href=""><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span><b>23rd June 2017</b></a></li>
							<li class=""><a href=""><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span><b>23rd June 2017</b></a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-3">
					<div class="menu-foot">
						<h3>Quick Links</h3>
						<ul class="list-posts">
                            <li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/index"); ?>">Home</a></li>
                            <li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/about"); ?>">Who We Are</a></li>
                            <li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/products"); ?>">Our Products</a></li>
                            <li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/index"); ?>">Our Partners</a></li>
                            <li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/index"); ?>">Our Team</a></li>
                            <li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/index"); ?>">Upcoming Events</a></li>
                            <li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/contact"); ?>">Request For Demo</a></li>
                            <li><a href="<?php echo Yii::$app->urlManager->createUrl("/site/index"); ?>">FAQS</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-3">
					<div class="contacts">
						<h3>Contact Us</h3>
						<ul class="contact">
							<li class="add">71 Pilgrim Avenue, Cavery<br/>Chase, <br/>MD 20163</li>
							<li class="phone">(226) 9021 -219 <br/>(226) 9021 -219</li>
							<li class="time">Mon to Sat 09:00 am to 6:00 pm<br>Sun 10:00 am to 1:00 pm</li>
							<li class="email">dummmy@mail.com</li>
						</ul>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
	<div class="bot-foot">
		<div class="container text-center">
			<p>&copy; 2018 securing next generations. All rights reserved </p>
		</div>
	</div>
</footer>



<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
