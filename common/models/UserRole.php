<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user_role".
 *
 * @property int $role_id
 * @property string $role_uuid
 * @property string $role_name
 * @property string $role_is_status
 * @property string $role_is_deleted
 * @property string $role_created_date
 */
class UserRole extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role_uuid', 'role_name', 'role_created_date'], 'required'],
            [['role_is_status', 'role_is_deleted'], 'string'],
            [['role_created_date'], 'safe'],
            [['role_uuid'], 'string', 'max' => 128],
            [['role_name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'role_id' => 'Role ID',
            'role_uuid' => 'Role Uuid',
            'role_name' => 'Role Name',
            'role_is_status' => 'Role Is Status',
            'role_is_deleted' => 'Role Is Deleted',
            'role_created_date' => 'Role Created Date',
        ];
    }
}
